package com.adgooroo.qa.insights.login;

import static org.testng.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;

import com.innominds.itaf.frameworkengine.Constants;
import com.innominds.itaf.frameworkengine.PageActionUtils;

public class Login extends PageActionUtils{
	public static final int TIMEOUT = 300000;
	public static final int PULSE = 100;
	Logger logger = LogManager.getLogger(Login.class.getName());
	WebElement element = null;
	
	HashMap objectMap=null;
	/**Login into Insights Application*/
	public void validateLogin(LoginDP dp, WebDriver driver)
	{		
		try
		{
			System.out.println(System.currentTimeMillis());
			
			Reporter.log("Insights Login URL", true);
		
			Reporter.log("........Insights Login Application........", true);
			Reporter.log(driver.getTitle(), true);
			//driver.manage().window().maximize();
			enterText(getWebElement(dp.or, "username",driver), dp.td.get("UserName"));
			/*Reporter.log(driver.getTitle(), true);
			boolean flag = driver.getTitle().contains("Welcome to Insights");
			Assert.assertEquals(flag, true);*/
			enterText(getWebElement(dp.or, "password",driver), dp.td.get("Password"));		
			click(getWebElement(dp.or, "loginBtn",driver));
			waitForElementPresent(getWebElement(dp.or, "notifySuccess",driver),TIMEOUT, PULSE);
			//waitForElementPresent(getWebElement(dp.or, "welcomeNote",driver),TIMEOUT, PULSE);
			Thread.sleep(1500);
			for (int i=0;i<Constants.EXPLICIT_TIMEOUT;i++){
			if (!(driver.findElement(By.tagName("body")).getText().contains("loading profile, please wait...")))
				{System.out.println("Breaking loop at: "+i);
				break;}
			else
				//System.out.println(getWebElement(dp.or, "welcomeNote",driver).isDisplayed());
			Thread.sleep(1000);	
			}
			
			Thread.sleep(7000);	
			
			System.out.println(driver.getTitle());
			Reporter.log(driver.getTitle());
			
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}
	
	/**Validate the login UI of Insights Application*/
	public void validateLoginUI(LoginDP dp, WebDriver driver)
	{		
		try
		{
		
			Reporter.log("Insights Login UI", true);
			Reporter.log("........Insights Login UI check........", true);
			Reporter.log(driver.getTitle(), true);
			Thread.sleep(5000);
			Assert.assertEquals(driver.getTitle(), dp.td.get("PageTitle"));
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "logo",driver)));
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "username",driver)));
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "password",driver)));
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "forgotLnk",driver)));
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "linkedInBtn",driver)));
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "twitterBtn",driver)));
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "forgotLnk",driver)));
			click(getWebElement(dp.or, "forgotLnk",driver));
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "resetEmail",driver)));
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "cancelBtn",driver)));
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "sendBtn",driver)));
			String resetPwdPopupText = getWebElement(dp.or, "resetPwdPopup",driver).getText();
			//System.out.println(resetPwdPopupText);
			Assert.assertTrue( (resetPwdPopupText.contains(dp.td.get("ResetText1"))) && (resetPwdPopupText.contains(dp.td.get("ResetText2"))) && (resetPwdPopupText.contains(dp.td.get("ResetText3"))));
			click(getWebElement(dp.or, "cancelBtn",driver));
			System.out.println(resetPwdPopupText);
			Reporter.log(resetPwdPopupText);
			
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}
	
	
	
	/**Logout from Insights Application successfully*/
	public void validateLogout(LoginDP dp, WebDriver driver)
	{		
		try
		{
		
			Reporter.log("Insights Logout", true);
		
			Reporter.log("........Logout Insights Application successfully ........", true);
			waitForPageLoaded(driver);
			Thread.sleep(3000);
			waitForElementPresent(getWebElement(dp.or, "userDropDown",driver),TIMEOUT, PULSE);
			click(getWebElement(dp.or, "userDropDown",driver));
			click(getWebElement(dp.or, "signoutBtn",driver));
			Thread.sleep(1000);
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "logo",driver)));
					
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
		
	}	
		
		//div[contains(@class,'accountsList')]//li
	public void validateSwitchAccount(LoginDP dp, WebDriver driver)
	{
	
		try
		{
			waitForElementPresent(getWebElement(dp.or, "accountName",driver),TIMEOUT, PULSE);
			Assert.assertEquals(getWebElement(dp.or, "accountName",driver).getText().trim(), dp.td.get("DefaultAccount").trim());
			click(getWebElement(dp.or, "userDropDown",driver));
			waitForElementPresent(getWebElement(dp.or, "accountLookupInput",driver),TIMEOUT, PULSE);
			List<WebElement> accounts = driver.findElements(By.xpath("//div[contains(@class,'accountsList')]//li"));//div[contains(@class,'accountsList')]//li
			Random r = new Random();
			int n =r.nextInt(accounts.size()-1)+1;
			String targetName = accounts.get(n).getText();
			getWebElement(dp.or, "accountLookupInput",driver).sendKeys(targetName);
			driver.findElement(By.xpath("//div[contains(@class,'accountsList')]//li")).click();
			waitForElementPresent(getWebElement(dp.or, "popupAccountSwitchHeader",driver),TIMEOUT, PULSE);
			getWebElement(dp.or, "okBtn",driver).click();
			waitForElementPresent(getWebElement(dp.or, "toastNotification",driver),TIMEOUT, PULSE);
			Thread.sleep(1500);
			//System.out.print(driver.findElement(By.xpath("//div[@class='toastify toastify--bottom-right']")).getText());
			//System.out.print(getWebElement(dp.or, "toastNotification",driver).getText());
			Assert.assertEquals(getWebElement(dp.or, "toastNotification",driver).getText().trim(), "You have switched accounts");
			Assert.assertEquals(getWebElement(dp.or, "accountName",driver).getText().trim(), targetName);

			
		} catch (Exception e) {
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
		
	}
		/*try {
			Assert.assertFalse(isElementDisplayed(getWebElement(dp.or, "userDropDown",driver)));
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "logo",driver)));
			Reporter.log(driver.getTitle());
		} catch (Exception e) {
			System.out.println("Exception ignored");
		}*/
		

	
	
	
	
   
}
