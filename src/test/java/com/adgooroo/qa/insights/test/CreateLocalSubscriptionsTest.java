package com.adgooroo.qa.insights.test;

import java.io.FileNotFoundException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.eclipse.jetty.websocket.api.SuspendToken;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.innominds.itaf.driverinit.DriverManager;
import com.innominds.itaf.frameworkengine.Constants;
import com.innominds.itaf.utils.PropertyFileUtils;
import com.adgooroo.qa.insights.subscriptions.Subscriptions;
import com.adgooroo.qa.insights.subscriptions.SubscriptionsDP;
import com.adgooroo.qa.insights.alerts.AlertsDP;
import com.adgooroo.qa.insights.login.Login;
import com.adgooroo.qa.insights.login.LoginDP;
import com.innominds.itaf.frameworkengine.CommonUtils;

public class CreateLocalSubscriptionsTest extends DriverManager {
	
	Logger logger = LogManager.getLogger(CreateSubscriptionsTest.class.getName());

	private WebDriver driver=null;
	/**
	 * Inits the.
	 * @throws Exception 
	 * @throws FileNotFoundException 
	 */
	@Parameters({ "browser","os"})
	@BeforeClass(alwaysRun = true, groups = { "Regression","Sanity" })
	public void init(String browser,String osName) throws  Exception {

		System.out.println("===========================================================");
		System.out.println("==================== My Performance Alert script execution Started ==========================================");
		System.out.println("===========================================================");
		browserName=browser;
		os=osName;
		this.driver=getDriver(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "GridExecution"));
		loadURL(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "web.app.url"), driver,browserName);
	}


	/*************************************************************************
	 * 
	 * TEST SUITE TESTS
	 * 
	 *************************************************************************/

	/*
	 * ****************************************
	 * Objects Creation
	 * ****************************************
	 */

	Login login = new Login();
	Subscriptions subscription = new Subscriptions();
	
	/*
	 * ****************************************
	 * Data Providers
	 * ****************************************
	 */

	@DataProvider(name="LoginDP")
	public Object[][] LoginDP() {
		if("firefox".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("chrome".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("ie".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}
		return null;
	}
	@DataProvider(name="SubscriptionsDP")
	public Object[][] Subscriptions() {
		return SubscriptionsDP.createDP("DR1");
	}
	

	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void loginWithValidCredentials(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogin(dp, driver);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type MultiCityOverview in Multi City reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_MultiCityOverviewDP")
	public Object[][] Local_MultiCityOverview() {
		return SubscriptionsDP.createDP("Local_MultiCityOverview");
	}
	@Test( dataProvider = "Local_MultiCityOverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_MultiCityOverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setLocalTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type MultiCity Advertisers in Multi City reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_MultiCityAdvertisersDP")
	public Object[][] Local_MultiCityAdvertisers() {
		return SubscriptionsDP.createDP("Local_MultiCityAdvertisers");
	}
	@Test( dataProvider = "Local_MultiCityAdvertisersDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_MultiCityAdvertisers(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.setLocalTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Group Advertisers in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_GroupAdvertisersDP")
	public Object[][] Local_GroupAdvertisers() {
		return SubscriptionsDP.createDP("Local_GroupAdvertisers");
	}
	@Test( dataProvider = "Local_GroupAdvertisersDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_GroupAdvertisers(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Calendar Report in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_CalendarReportDP")
	public Object[][] Local_CalendarReport() {
		return SubscriptionsDP.createDP("Local_CalendarReport");
	}
	@Test( dataProvider = "Local_CalendarReportDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_CalendarReport(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Competitor Keywords in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_CompetitorKeywordsDP")
	public Object[][] Local_CompetitorKeywords() {
		return SubscriptionsDP.createDP("Local_CompetitorKeywords");
	}
	@Test( dataProvider = "Local_CompetitorKeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_CompetitorKeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Group Creative in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_GroupCreativeDP")
	public Object[][] Local_GroupCreative() {
		return SubscriptionsDP.createDP("Local_GroupCreative");
	}
	@Test( dataProvider = "Local_GroupCreativeDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_GroupCreative(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Group Keywords in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_GroupKeywordsDP")
	public Object[][] Local_GroupKeywords() {
		return SubscriptionsDP.createDP("Local_GroupKeywords");
	}
	@Test( dataProvider = "Local_GroupKeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_GroupKeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Group Advertiser Historical in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_GroupAdvertiserHistoricalDP")
	public Object[][] Local_GroupAdvertiserHistorical() {
		return SubscriptionsDP.createDP("Local_GroupAdvertiserHistorical");
	}
	@Test( dataProvider = "Local_GroupAdvertiserHistoricalDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_GroupAdvertiserHistorical(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Group Overview in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_GroupOverviewDP")
	public Object[][] Local_GroupOverview() {
		return SubscriptionsDP.createDP("Local_GroupOverview");
	}
	@Test( dataProvider = "Local_GroupOverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_GroupOverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Keyword Opportunities in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_KeywordOpportunitiesDP")
	public Object[][] Local_KeywordOpportunities() {
		return SubscriptionsDP.createDP("Local_KeywordOpportunities");
	}
	@Test( dataProvider = "Local_KeywordOpportunitiesDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_KeywordOpportunities(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Keyword Opportunities Historical in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_KeywordOpportunitiesHistoricalDP")
	public Object[][] Local_KeywordOpportunitiesHistorical() {
		return SubscriptionsDP.createDP("Local_KeywordOpportunitiesHistorical");
	}
	@Test( dataProvider = "Local_KeywordOpportunitiesHistoricalDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_KeywordOpportunitiesHistorical(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Local_Multi-GroupComparison in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_Multi-GroupComparisonDP")
	public Object[][] Local_MultiGroupComparison() {
		return SubscriptionsDP.createDP("Local_Multi-GroupComparison");
	}
	@Test( dataProvider = "Local_Multi-GroupComparisonDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_MultiGroupComparison(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.setLocalTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Advertiser Overview in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_AdvertiserOverviewDP")
	public Object[][] Local_AdvertiserOverview() {
		return SubscriptionsDP.createDP("Local_AdvertiserOverview");
	}
	@Test( dataProvider = "Local_AdvertiserOverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_AdvertiserOverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Advertiser Keywords in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_AdvertiserKeywordsDP")
	public Object[][] Local_AdvertiserKeywords() {
		return SubscriptionsDP.createDP("Local_AdvertiserKeywords");
	}
	@Test( dataProvider = "Local_AdvertiserKeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_AdvertiserKeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Missed Keywords in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_MissedKeywordsDP")
	public Object[][] Local_MissedKeywords() {
		return SubscriptionsDP.createDP("Local_MissedKeywords");
	}
	@Test( dataProvider = "Local_MissedKeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_MissedKeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.setLocalTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Advertiser Creative in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_AdvertiserCreativeDP")
	public Object[][] Local_AdvertiserCreative() {
		return SubscriptionsDP.createDP("Local_AdvertiserCreative");
	}
	@Test( dataProvider = "Local_AdvertiserCreativeDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_AdvertiserCreative(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Local_Multi-GroupComparison Historical in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_Multi-GroupComparisonHistoricalDP")
	public Object[][] Local_MultiGroupComparisonHistorical() {
		return SubscriptionsDP.createDP("Local_Multi-GroupComparisonHistorical");
	}
	@Test( dataProvider = "Local_Multi-GroupComparisonHistoricalDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_MultiGroupComparisonHistorical(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.setLocalTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Advertiser Comparison in Comparison reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_AdvertiserComparisonDP")
	public Object[][] Local_AdvertiserComparison() {
		return SubscriptionsDP.createDP("Local_AdvertiserComparison");
	}
	@Test( dataProvider = "Local_AdvertiserComparisonDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_AdvertiserComparison(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Keyword Comparison in Comparison reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_KeywordComparisonDP")
	public Object[][] Local_KeywordComparison() {
		return SubscriptionsDP.createDP("Local_KeywordComparison");
	}
	@Test( dataProvider = "Local_KeywordComparisonDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_KeywordComparison(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type MultiCity Keywords in Multicity reports reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_MultiCityKeywordsDP")
	public Object[][] Local_MultiCityKeywords() {
		return SubscriptionsDP.createDP("Local_MultiCityKeywords");
	}
	@Test( dataProvider = "Local_MultiCityKeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_MultiCityKeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.setLocalTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Keyword Creative in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_KeywordCreativeDP")
	public Object[][] Local_KeywordCreative() {
		return SubscriptionsDP.createDP("Local_KeywordCreative");
	}
	@Test( dataProvider = "Local_KeywordCreativeDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_KeywordCreative(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Keyword Advertisers in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_KeywordAdvertisersDP")
	public Object[][] Local_KeywordAdvertisers() {
		return SubscriptionsDP.createDP("Local_KeywordAdvertisers");
	}
	@Test( dataProvider = "Local_KeywordAdvertisersDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_KeywordAdvertisers(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type PositionShare in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_PositionShareDP")
	public Object[][] Local_PositionShare() {
		return SubscriptionsDP.createDP("Local_PositionShare");
	}
	@Test( dataProvider = "Local_PositionShareDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_PositionShare(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Local
	 * and report type Keyword Overview in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="Local_KeywordOverviewDP")
	public Object[][] Local_KeywordOverview() {
		return SubscriptionsDP.createDP("Local_KeywordOverview");
	}
	@Test( dataProvider = "Local_KeywordOverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Local_KeywordOverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void logoutValidations(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogout(dp, driver);
	}




	/**This section is used to print the execution end and quit the browser*/
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		System.out.println("===============================================================================================");
		System.out.println("====================  Script execution Completed ==============================");
		System.out.println("===============================================================================================");

		try
		{

			if(driver != null)
			{
				driver.quit();

			}
		}catch(Exception e)
		{
			throw new RuntimeException("teardown() method failed to execute " +e);
		}
	}

}

