/**
 * 
 */
package com.adgooroo.qa.insights.test;
import java.io.FileNotFoundException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.eclipse.jetty.websocket.api.SuspendToken;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.innominds.itaf.driverinit.DriverManager;
import com.innominds.itaf.frameworkengine.Constants;
import com.innominds.itaf.utils.PropertyFileUtils;
import com.adgooroo.qa.insights.subscriptions.Subscriptions;
import com.adgooroo.qa.insights.subscriptions.SubscriptionsDP;
import com.adgooroo.qa.insights.alerts.AlertsDP;
import com.adgooroo.qa.insights.login.Login;
import com.adgooroo.qa.insights.login.LoginDP;
import com.innominds.itaf.frameworkengine.CommonUtils;

public class CreatePLASubscriptionsTest extends DriverManager {
	
	Logger logger = LogManager.getLogger(CreateSubscriptionsTest.class.getName());

	private WebDriver driver=null;
	/**
	 * Inits the.
	 * @throws Exception 
	 * @throws FileNotFoundException 
	 */
	@Parameters({ "browser","os"})
	@BeforeClass(alwaysRun = true, groups = { "Regression","Sanity" })
	public void init(String browser,String osName) throws  Exception {

		System.out.println("===========================================================");
		System.out.println("==================== My Performance Alert script execution Started ==========================================");
		System.out.println("===========================================================");
		browserName=browser;
		os=osName;
		this.driver=getDriver(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "GridExecution"));
		loadURL(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "web.app.url"), driver,browserName);
	}


	/*************************************************************************
	 * 
	 * TEST SUITE TESTS
	 * 
	 *************************************************************************/

	/*
	 * ****************************************
	 * Objects Creation
	 * ****************************************
	 */

	Login login = new Login();
	Subscriptions subscription = new Subscriptions();
	
	/*
	 * ****************************************
	 * Data Providers
	 * ****************************************
	 */

	@DataProvider(name="LoginDP")
	public Object[][] LoginDP() {
		if("firefox".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("chrome".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("ie".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}
		return null;
	}
	@DataProvider(name="SubscriptionsDP")
	public Object[][] Subscriptions() {
		return SubscriptionsDP.createDP("DR1");
	}
	

	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void loginWithValidCredentials(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogin(dp, driver);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type PLA
	 * and report type Keyword Research in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="PLA_KeywordResearchDP")
	public Object[][] PLA_KeywordResearch() {
		return SubscriptionsDP.createDP("PLA_KeywordResearch");
	}
	@Test( dataProvider = "PLA_KeywordResearchDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_PLA_KeywordResearch(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),"");
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void logoutValidations(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogout(dp, driver);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type PLA
	 * and report type Group overview in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="PLA_GroupoverviewDP")
	public Object[][] PLA_Groupoverview() {
		return SubscriptionsDP.createDP("PLA_Groupoverview");
	}
	@Test( dataProvider = "PLA_GroupoverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_PLA_Groupoverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),"");
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type PLA
	 * and report type Group advertisers in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="PLA_GroupadvertisersDP")
	public Object[][] PLA_Groupadvertisers() {
		return SubscriptionsDP.createDP("PLA_Groupadvertisers");
	}
	@Test( dataProvider = "PLA_GroupadvertisersDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_PLA_Groupadvertisers(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),"");
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type PLA
	 * and report type Group keywords in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="PLA_GroupkeywordsDP")
	public Object[][] PLA_Groupkeywords() {
		return SubscriptionsDP.createDP("PLA_Groupkeywords");
	}
	@Test( dataProvider = "PLA_GroupkeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_PLA_Groupkeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),"");
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type PLA
	 * and report type Group creative in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="PLA_GroupcreativeDP")
	public Object[][] PLA_Groupcreative() {
		return SubscriptionsDP.createDP("PLA_Groupcreative");
	}
	@Test( dataProvider = "PLA_GroupcreativeDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_PLA_Groupcreative(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),"");
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}

	
	/*
	 * ****************************************
	 * This test is used to create subscription product type PLA
	 * and report type Keyword opportunities in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="PLA_KeywordopportunitiesDP")
	public Object[][] PLA_Keywordopportunities() {
		return SubscriptionsDP.createDP("PLA_Keywordopportunities");
	}
	@Test( dataProvider = "PLA_KeywordopportunitiesDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_PLA_Keywordopportunities(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),"");
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type PLA
	 * and report type MultiGroup comparison in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="PLA_MultiGroupcomparisonDP")
	public Object[][] PLA_MultiGroupcomparison() {
		return SubscriptionsDP.createDP("PLA_MultiGroupcomparison");
	}
	@Test( dataProvider = "PLA_MultiGroupcomparisonDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_PLA_MultiGroupcomparison(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),"");
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type PLA
	 * and report type Advertiser overview in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="PLA_AdvertiseroverviewDP")
	public Object[][] PLA_Advertiseroverview() {
		return SubscriptionsDP.createDP("PLA_Advertiseroverview");
	}
	@Test( dataProvider = "PLA_AdvertiseroverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_PLA_Advertiseroverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),"");
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type PLA
	 * and report type Advertiser keywords in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="PLA_AdvertiserkeywordsDP")
	public Object[][] PLA_Advertiserkeywords() {
		return SubscriptionsDP.createDP("PLA_Advertiserkeywords");
	}
	@Test( dataProvider = "PLA_AdvertiserkeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_PLA_Advertiserkeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),"");
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type PLA
	 * and report type Advertiser creative in Advertiser reports.
	 * ****************************************
	 */
	
	@DataProvider(name="PLA_AdvertisercreativeDP")
	public Object[][] PLA_Advertisercreative() {
		return SubscriptionsDP.createDP("PLA_Advertisercreative");
	}
	@Test( dataProvider = "PLA_AdvertisercreativeDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_PLA_Advertisercreative(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),"");
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type PLA
	 * and report type Advertiser comparison in comparison reports.
	 * ****************************************
	 */
	@DataProvider(name="PLA_AdvertisercomparisonDP")
	public Object[][] PLA_Advertisercomparison() {
		return SubscriptionsDP.createDP("PLA_Advertisercomparison");
	}
	@Test( dataProvider = "PLA_AdvertisercomparisonDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_PLA_Advertisercomparison(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),"");
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type PLA
	 * and report type Keyword comparison in comparison reports.
	 * ****************************************
	 */
	@DataProvider(name="PLA_KeywordcomparisonDP")
	public Object[][] PLA_Keywordcomparison() {
		return SubscriptionsDP.createDP("PLA_Keywordcomparison");
	}
	@Test( dataProvider = "PLA_KeywordcomparisonDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_PLA_Keywordcomparison(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),"");
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type PLA
	 * and report type Keyword overview in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="PLA_KeywordoverviewDP")
	public Object[][] PLA_Keywordoverview() {
		return SubscriptionsDP.createDP("PLA_Keywordoverview");
	}
	@Test( dataProvider = "PLA_KeywordoverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_PLA_Keywordoverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),"");
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type PLA
	 * and report type Keyword advertisers in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="PLA_KeywordadvertisersDP")
	public Object[][] PLA_Keywordadvertisers() {
		return SubscriptionsDP.createDP("PLA_Keywordadvertisers");
	}
	@Test( dataProvider = "PLA_KeywordadvertisersDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_PLA_Keywordadvertisers(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),"");
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type PLA
	 * and report type Keyword creative in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="PLA_KeywordcreativeDP")
	public Object[][] PLA_Keywordcreative() {
		return SubscriptionsDP.createDP("PLA_Keywordcreative");
	}
	@Test( dataProvider = "PLA_KeywordcreativeDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_PLA_Keywordcreative(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "No");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),"");
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/**This section is used to print the execution end and quit the browser*/
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		System.out.println("===============================================================================================");
		System.out.println("====================  Script execution Completed ==============================");
		System.out.println("===============================================================================================");

		try
		{

			if(driver != null)
			{
				driver.quit();

			}
		}catch(Exception e)
		{
			throw new RuntimeException("teardown() method failed to execute " +e);
		}
	}

}
