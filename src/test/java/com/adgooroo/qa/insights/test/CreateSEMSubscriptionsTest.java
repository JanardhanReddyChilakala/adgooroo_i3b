/**
 * 
 */
package com.adgooroo.qa.insights.test;

import java.io.FileNotFoundException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.eclipse.jetty.websocket.api.SuspendToken;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.innominds.itaf.driverinit.DriverManager;
import com.innominds.itaf.frameworkengine.Constants;
import com.innominds.itaf.utils.PropertyFileUtils;
import com.adgooroo.qa.insights.subscriptions.Subscriptions;
import com.adgooroo.qa.insights.subscriptions.SubscriptionsDP;
import com.adgooroo.qa.insights.alerts.AlertsDP;
import com.adgooroo.qa.insights.login.Login;
import com.adgooroo.qa.insights.login.LoginDP;
import com.innominds.itaf.frameworkengine.CommonUtils;

public class CreateSEMSubscriptionsTest extends DriverManager {

	Logger logger = LogManager.getLogger(CreateSubscriptionsTest.class.getName());

	private WebDriver driver=null;
	/**
	 * Inits the.
	 * @throws Exception 
	 * @throws FileNotFoundException 
	 */
	@Parameters({ "browser","os"})
	@BeforeClass(alwaysRun = true, groups = { "Regression","Sanity" })
	public void init(String browser,String osName) throws  Exception {

		System.out.println("===========================================================");
		System.out.println("==================== My Performance Alert script execution Started ==========================================");
		System.out.println("===========================================================");
		browserName=browser;
		os=osName;
		this.driver=getDriver(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "GridExecution"));
		loadURL(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "web.app.url"), driver,browserName);
	}


	/*************************************************************************
	 * 
	 * TEST SUITE TESTS
	 * 
	 *************************************************************************/

	/*
	 * ****************************************
	 * Objects Creation
	 * ****************************************
	 */

	Login login = new Login();
	Subscriptions subscription = new Subscriptions();
	
	/*
	 * ****************************************
	 * Data Providers
	 * ****************************************
	 */

	@DataProvider(name="LoginDP")
	public Object[][] LoginDP() {
		if("firefox".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("chrome".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("ie".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}
		return null;
	}
	@DataProvider(name="SubscriptionsDP")
	public Object[][] Subscriptions() {
		return SubscriptionsDP.createDP("DR1");
	}
	

	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void loginWithValidCredentials(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogin(dp, driver);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Competitor Keywords in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_CompetitorKeywordsDP")
	public Object[][] SEM_CompetitorKeywords() {
		return SubscriptionsDP.createDP("SEM_CompetitorKeywords");
	}
	@Test( dataProvider = "SEM_CompetitorKeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_CompetitorKeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Group Creative Group in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_GroupCreativeGroupDP")
	public Object[][] SEM_GroupCreativeGroup() {
		return SubscriptionsDP.createDP("SEM_GroupCreativeGroup");
	}
	@Test( dataProvider = "SEM_GroupCreativeGroupDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_GroupCreativeGroup(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Group Creative in Group reports.
	 * ****************************************
	 */
	
	@DataProvider(name="SEM_GroupCreativeDP")
	public Object[][] SEM_GroupCreative() {
		return SubscriptionsDP.createDP("SEM_GroupCreative");
	}
	@Test( dataProvider = "SEM_GroupCreativeDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_GroupCreative(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Group Advertisers Rollup in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_GroupAdvertisersRollupDP")
	public Object[][] SEM_GroupAdvertisersRollup() {
		return SubscriptionsDP.createDP("SEM_GroupAdvertisersRollup");
	}
	@Test( dataProvider = "SEM_GroupAdvertisersRollupDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_GroupAdvertisersRollup(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Group Advertisers Rollup Historical in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_GroupAdvertisersRollupHistoricalDP")
	public Object[][] SEM_GroupAdvertisersRollupHistorical() {
		return SubscriptionsDP.createDP("SEM_GroupAdvertisersRollupHistorical");
	}
	@Test( dataProvider = "SEM_GroupAdvertisersRollupHistoricalDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_GroupAdvertisersRollupHistorical(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Group Keywords in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_GroupKeywordsDP")
	public Object[][] SEM_GroupKeywords() {
		return SubscriptionsDP.createDP("SEM_GroupKeywords");
	}
	@Test( dataProvider = "SEM_GroupKeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_GroupKeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Group Overview in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_GroupOverviewDP")
	public Object[][] SEM_GroupOverview() {
		return SubscriptionsDP.createDP("SEM_GroupOverview");
	}
	@Test( dataProvider = "SEM_GroupOverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_GroupOverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectRollupBreakout(dp, driver, dp.td.get("RollupBreakout"), dp.td.get("BugID"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Group Advertisers in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_GroupAdvertisersDP")
	public Object[][] SEM_GroupAdvertisers() {
		return SubscriptionsDP.createDP("SEM_GroupAdvertisers");
	}
	@Test( dataProvider = "SEM_GroupAdvertisersDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_GroupAdvertisers(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Group Advertisers History in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_GroupAdvertisersHistoryDP")
	public Object[][] SEM_GroupAdvertisersHistory() {
		return SubscriptionsDP.createDP("SEM_GroupAdvertisersHistory");
	}
	@Test( dataProvider = "SEM_GroupAdvertisersHistoryDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_GroupAdvertisersHistory(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type CalendarReport in Group reports.
	 * ****************************************
	 */
	
	@DataProvider(name="SEM_CalendarReportDP")
	public Object[][] SEM_CalendarReport() {
		return SubscriptionsDP.createDP("SEM_CalendarReport");
	}
	@Test( dataProvider = "SEM_CalendarReportDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_CalendarReport(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Branded Unbranded in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_BrandedUnbrandedDP")
	public Object[][] SEM_BrandedUnbranded() {
		return SubscriptionsDP.createDP("SEM_BrandedUnbranded");
	}
	@Test( dataProvider = "SEM_BrandedUnbrandedDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_BrandedUnbranded(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Keyword Opportunities in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_KeywordOpportunitiesDP")
	public Object[][] SEM_KeywordOpportunities() {
		return SubscriptionsDP.createDP("SEM_KeywordOpportunities");
	}
	@Test( dataProvider = "SEM_KeywordOpportunitiesDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_KeywordOpportunities(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Advertiser Overview in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_AdvertiserOverviewDP")
	public Object[][] SEM_AdvertiserOverview() {
		return SubscriptionsDP.createDP("SEM_AdvertiserOverview");
	}
	@Test( dataProvider = "SEM_AdvertiserOverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_AdvertiserOverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Advertiser Keywords in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_AdvertiserKeywordsDP")
	public Object[][] SEM_AdvertiserKeywords() {
		return SubscriptionsDP.createDP("SEM_AdvertiserKeywords");
	}
	@Test( dataProvider = "SEM_AdvertiserKeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_AdvertiserKeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Advertiser Keywords History in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_AdvertiserKeywordsHistoryDP")
	public Object[][] SEM_AdvertiserKeywordsHistory() {
		return SubscriptionsDP.createDP("SEM_AdvertiserKeywordsHistory");
	}
	@Test( dataProvider = "SEM_AdvertiserKeywordsHistoryDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_AdvertiserKeywordsHistory(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type MultiGroup Comparison in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_MultiGroupComparisonDP")
	public Object[][] SEM_MultiGroupComparison() {
		return SubscriptionsDP.createDP("SEM_MultiGroupComparison");
	}
	@Test( dataProvider = "SEM_MultiGroupComparisonDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_MultiGroupComparison(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Advertiser Creative in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_AdvertiserCreativeDP")
	public Object[][] SEM_AdvertiserCreative() {
		return SubscriptionsDP.createDP("SEM_AdvertiserCreative");
	}
	@Test( dataProvider = "SEM_AdvertiserCreativeDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_AdvertiserCreative(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Missed Keywords in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_MissedKeywordsDP")
	public Object[][] SEM_MissedKeywords() {
		return SubscriptionsDP.createDP("SEM_MissedKeywords");
	}
	@Test( dataProvider = "SEM_MissedKeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_MissedKeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type MultiGroup Comparison History in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_MultiGroupComparisonHistoryDP")
	public Object[][] SEM_MultiGroupComparisonHistory() {
		return SubscriptionsDP.createDP("SEM_MultiGroupComparisonHistory");
	}
	@Test( dataProvider = "SEM_MultiGroupComparisonHistoryDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_MultiGroupComparisonHistory(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Keyword Comparison in Comparison reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_KeywordComparisonDP")
	public Object[][] SEM_KeywordComparison() {
		return SubscriptionsDP.createDP("SEM_KeywordComparison");
	}
	@Test( dataProvider = "SEM_KeywordComparisonDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_KeywordComparison(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Advertiser Comparison in Comparison reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_AdvertiserComparisonDP")
	public Object[][] SEM_AdvertiserComparison() {
		return SubscriptionsDP.createDP("SEM_AdvertiserComparison");
	}
	@Test( dataProvider = "SEM_AdvertiserComparisonDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_AdvertiserComparison(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type PPCvsNatural in Comparison reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_PPCvsNaturalDP")
	public Object[][] SEM_PPCvsNatural() {
		return SubscriptionsDP.createDP("SEM_PPCvsNatural");
	}
	@Test( dataProvider = "SEM_PPCvsNaturalDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_PPCvsNatural(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectComparisonAdvertisers(dp, driver, dp.td.get("Advertiser1"),dp.td.get("Advertiser2"),dp.td.get("BugID"),browserName);
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Keyword Overview in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_KeywordOverviewDP")
	public Object[][] SEM_KeywordOverview() {
		return SubscriptionsDP.createDP("SEM_KeywordOverview");
	}
	@Test( dataProvider = "SEM_KeywordOverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_KeywordOverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}

	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Keyword Advertisers in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_KeywordAdvertisersDP")
	public Object[][] SEM_KeywordAdvertisers() {
		return SubscriptionsDP.createDP("SEM_KeywordAdvertisers");
	}
	@Test( dataProvider = "SEM_KeywordAdvertisersDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_KeywordAdvertisers(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Keyword Creative in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_KeywordCreativeDP")
	public Object[][] SEM_KeywordCreative() {
		return SubscriptionsDP.createDP("SEM_KeywordCreative");
	}
	@Test( dataProvider = "SEM_KeywordCreativeDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_KeywordCreative(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type KeywordCreative_Breakout in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_KeywordCreative_BreakoutDP")
	public Object[][] SEM_KeywordCreative_Breakout() {
		return SubscriptionsDP.createDP("SEM_KeywordCreative_Breakout");
	}
	@Test( dataProvider = "SEM_KeywordCreative_BreakoutDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_KeywordCreative_Breakout(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type PositionShare in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="SEM_PositionShareDP")
	public Object[][] SEM_PositionShare() {
		return SubscriptionsDP.createDP("SEM_PositionShare");
	}
	@Test( dataProvider = "SEM_PositionShareDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_SEM_PositionShare(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	
	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void logoutValidations(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogout(dp, driver);
	}




	/**This section is used to print the execution end and quit the browser*/
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		System.out.println("===============================================================================================");
		System.out.println("====================  Script execution Completed ==============================");
		System.out.println("===============================================================================================");

		try
		{

			if(driver != null)
			{
				driver.quit();

			}
		}catch(Exception e)
		{
			throw new RuntimeException("teardown() method failed to execute " +e);
		}
	}
}
