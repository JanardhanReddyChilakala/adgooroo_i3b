package com.adgooroo.qa.insights.test;

import java.io.FileNotFoundException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.eclipse.jetty.websocket.api.SuspendToken;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.innominds.itaf.driverinit.DriverManager;
import com.innominds.itaf.frameworkengine.Constants;
import com.innominds.itaf.utils.PropertyFileUtils;
import com.adgooroo.qa.insights.subscriptions.Subscriptions;
import com.adgooroo.qa.insights.subscriptions.SubscriptionsDP;
import com.adgooroo.qa.insights.alerts.AlertsDP;
import com.adgooroo.qa.insights.login.Login;
import com.adgooroo.qa.insights.login.LoginDP;
import com.innominds.itaf.frameworkengine.CommonUtils;

public class CreateTMSubscriptionsTest extends DriverManager {
	
	Logger logger = LogManager.getLogger(CreateSubscriptionsTest.class.getName());

	private WebDriver driver=null;
	/**
	 * Inits the.
	 * @throws Exception 
	 * @throws FileNotFoundException 
	 */
	@Parameters({ "browser","os"})
	@BeforeClass(alwaysRun = true, groups = { "Regression","Sanity" })
	public void init(String browser,String osName) throws  Exception {

		System.out.println("===========================================================");
		System.out.println("==================== My Performance Alert script execution Started ==========================================");
		System.out.println("===========================================================");
		browserName=browser;
		os=osName;
		this.driver=getDriver(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "GridExecution"));
		loadURL(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "web.app.url"), driver,browserName);
	}


	/*************************************************************************
	 * 
	 * TEST SUITE TESTS
	 * 
	 *************************************************************************/

	/*
	 * ****************************************
	 * Objects Creation
	 * ****************************************
	 */

	Login login = new Login();
	Subscriptions subscription = new Subscriptions();
	
	/*
	 * ****************************************
	 * Data Providers
	 * ****************************************
	 */

	@DataProvider(name="LoginDP")
	public Object[][] LoginDP() {
		if("firefox".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("chrome".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("ie".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}
		return null;
	}
	@DataProvider(name="SubscriptionsDP")
	public Object[][] Subscriptions() {
		return SubscriptionsDP.createDP("DR1");
	}
	

	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void loginWithValidCredentials(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogin(dp, driver);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Group Infringementsby Advertiser in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="TM_GroupInfringementsbyAdvertiserDP")
	public Object[][] TM_GroupInfringementsbyAdvertiser() {
		return SubscriptionsDP.createDP("TM_GroupInfringementsbyAdvertiser");
	}
	@Test( dataProvider = "TM_GroupInfringementsbyAdvertiserDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_TM_GroupInfringementsbyAdvertiser(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectInfringementType(dp, driver, dp.td.get("InfringementType"),dp.td.get("BugID"));
		subscription.selectRollupBreakout(dp, driver, dp.td.get("RollupBreakout"),dp.td.get("BugID"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Group Infringements by Keywords in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="TM_GroupInfringementsbyKeywordsDP")
	public Object[][] TM_GroupInfringementsbyKeywords() {
		return SubscriptionsDP.createDP("TM_GroupInfringementsbyKeywords");
	}
	@Test( dataProvider = "TM_GroupInfringementsbyKeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_TM_GroupInfringementsbyKeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectInfringementType(dp, driver, dp.td.get("InfringementType"),dp.td.get("BugID"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Group Infringements by Adcopy in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="TM_GroupInfringementsbyAdcopyDP")
	public Object[][] TM_GroupInfringementsbyAdcopy() {
		return SubscriptionsDP.createDP("TM_GroupInfringementsbyAdcopy");
	}
	@Test( dataProvider = "TM_GroupInfringementsbyAdcopyDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_TM_GroupInfringementsbyAdcopy(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectInfringementType(dp, driver, dp.td.get("InfringementType"),dp.td.get("BugID"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Group Overview in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="TM_GroupOverviewDP")
	public Object[][] TM_GroupOverview() {
		return SubscriptionsDP.createDP("TM_GroupOverview");
	}
	@Test( dataProvider = "TM_GroupOverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_TM_GroupOverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectInfringementType(dp, driver, dp.td.get("InfringementType"),dp.td.get("BugID"));
		subscription.selectRollupBreakout(dp, driver, dp.td.get("RollupBreakout"),dp.td.get("BugID"));
		subscription.enterVerifyAdvertiserExclusion(dp, driver, dp.td.get("Advertiser/UrlExclusions"), "",browserName);
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Global Brand Overview in Group reports.
	 * ****************************************
	 */
	@DataProvider(name="TM_GlobalBrandOverviewDP")
	public Object[][] TM_GlobalBrandOverview() {
		return SubscriptionsDP.createDP("TM_GlobalBrandOverview");
	}
	@Test( dataProvider = "TM_GlobalBrandOverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_TM_GlobalBrandOverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setMultipleBrands(dp, driver, dp.td.get("KeywordGroupValue"));
		subscription.setTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectInfringementType(dp, driver, dp.td.get("InfringementType"),dp.td.get("BugID"));
		subscription.enterVerifyAdvertiserExclusion(dp, driver, dp.td.get("Advertiser/UrlExclusions"), "",browserName);
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Keyword Infringements by AdCopy in Keyword reports.
	 * ****************************************
	 */
	
	@DataProvider(name="TM_KeywordInfringementsbyAdCopyDP")
	public Object[][] TM_KeywordInfringementsbyAdCopy() {
		return SubscriptionsDP.createDP("TM_KeywordInfringementsbyAdCopy");
	}
	@Test( dataProvider = "TM_KeywordInfringementsbyAdCopyDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_TM_KeywordInfringementsbyAdCopy(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.selectInfringementType(dp, driver, dp.td.get("InfringementType"),dp.td.get("BugID"));
		subscription.enterVerifyAdvertiserExclusion(dp, driver, dp.td.get("Advertiser/UrlExclusions"), "",browserName);
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Keyword Infringements by Bidding in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="TM_KeywordOverviewbyBiddingDP")
	public Object[][] TM_KeywordOverviewbyBidding() {
		return SubscriptionsDP.createDP("TM_KeywordOverviewbyBidding");
	}
	@Test( dataProvider = "TM_KeywordOverviewbyBiddingDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_TM_KeywordOverviewbyBidding(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.selectInfringementType(dp, driver, dp.td.get("InfringementType"),dp.td.get("BugID"));
		subscription.enterVerifyAdvertiserExclusion(dp, driver, dp.td.get("Advertiser/UrlExclusions"), "",browserName);
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Keyword Infringements by Advertiser in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="TM_KeywordInfringementsbyAdvertiserDP")
	public Object[][] TM_KeywordInfringementsbyAdvertiser() {
		return SubscriptionsDP.createDP("TM_KeywordInfringementsbyAdvertiser");
	}
	@Test( dataProvider = "TM_KeywordInfringementsbyAdvertiserDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_TM_KeywordInfringementsbyAdvertiser(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.selectInfringementType(dp, driver, dp.td.get("InfringementType"),dp.td.get("BugID"));
		subscription.enterVerifyAdvertiserExclusion(dp, driver, dp.td.get("Advertiser/UrlExclusions"), "",browserName);
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Keyword Infringements by Advertiser in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="TM_KeywordOverviewbyAdCopyDP")
	public Object[][] TM_KeywordOverviewbyAdCopy() {
		return SubscriptionsDP.createDP("TM_KeywordOverviewbyAdCopy");
	}
	@Test( dataProvider = "TM_KeywordOverviewbyAdCopyDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_TM_KeywordOverviewbyAdCopy(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectAKeyword(dp, driver, dp.td.get("Keyword"),browserName);
		subscription.selectInfringementType(dp, driver, dp.td.get("InfringementType"),dp.td.get("BugID"));
		subscription.enterVerifyAdvertiserExclusion(dp, driver, dp.td.get("Advertiser/UrlExclusions"), "",browserName);
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Advertiser Overview in Infringer reports.
	 * ****************************************
	 */
	@DataProvider(name="TM_AdvertiserOverviewDP")
	public Object[][] TM_AdvertiserOverview() {
		return SubscriptionsDP.createDP("TM_AdvertiserOverview");
	}
	@Test( dataProvider = "TM_AdvertiserOverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_TM_AdvertiserOverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectInfringementType(dp, driver, dp.td.get("InfringementType"),dp.td.get("BugID"));
		subscription.selectRollupBreakout(dp, driver, dp.td.get("RollupBreakout"),dp.td.get("BugID"));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,-150)", "");
		subscription.enterVerifyAdvertiserExclusion(dp, driver, dp.td.get("Advertiser/UrlExclusions"), "",browserName);
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Advertiser Infringements by Keyword in Infringer reports.
	 * ****************************************
	 */
	@DataProvider(name="TM_AdvertiserInfringementsbyKeywordDP")
	public Object[][] TM_AdvertiserInfringementsbyKeyword() {
		return SubscriptionsDP.createDP("TM_AdvertiserInfringementsbyKeyword");
	}
	@Test( dataProvider = "TM_AdvertiserInfringementsbyKeywordDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_TM_AdvertiserInfringementsbyKeyword(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectInfringementType(dp, driver, dp.td.get("InfringementType"),dp.td.get("BugID"));
		subscription.enterVerifyAdvertiserExclusion(dp, driver, dp.td.get("Advertiser/UrlExclusions"), "",browserName);
		subscription.selectRollupBreakout(dp, driver, dp.td.get("RollupBreakout"),dp.td.get("BugID"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type Advertiser Infringements by AdCopy in Infringer reports.
	 * ****************************************
	 */
	@DataProvider(name="TM_AdvertiserInfringementsbyAdCopyDP")
	public Object[][] TM_AdvertiserInfringementsbyAdCopy() {
		return SubscriptionsDP.createDP("TM_AdvertiserInfringementsbyAdCopy");
	}
	@Test( dataProvider = "TM_AdvertiserInfringementsbyAdCopyDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_TM_AdvertiserInfringementsbyAdCopy(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectInfringementType(dp, driver, dp.td.get("InfringementType"),dp.td.get("BugID"));
		subscription.enterVerifyAdvertiserExclusion(dp, driver, dp.td.get("Advertiser/UrlExclusions"), "",browserName);
		subscription.selectRollupBreakout(dp, driver, dp.td.get("RollupBreakout"),dp.td.get("BugID"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type SEM
	 * and report type HijackedURL in Infringer reports.
	 * ****************************************
	 */
	@DataProvider(name="TM_HijackedURLDP")
	public Object[][] TM_HijackedURL() {
		return SubscriptionsDP.createDP("TM_HijackedURL");
	}
	@Test( dataProvider = "TM_HijackedURLDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_TM_HijackedURL(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyKwgCampaignBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"),dp.td.get("KWG_SubType"));
		subscription.setTargetSetting(dp, driver, dp.td.get("TargetSetting"));
		subscription.enterVerifyUrlExclusion(dp, driver, dp.td.get("Advertiser/UrlExclusions"), "");
		subscription.selectInfringementType(dp, driver, dp.td.get("InfringementType"),dp.td.get("BugID"));
		subscription.selectSubscriptionDateRange(dp, driver, dp.td.get("DateRangeType"), dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void logoutValidations(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogout(dp, driver);
	}



	/**This section is used to print the execution end and quit the browser*/

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		System.out.println("===============================================================================================");
		System.out.println("====================  Script execution Completed ==============================");
		System.out.println("===============================================================================================");

		try
		{

			if(driver != null)
			{
				driver.quit();

			}
		}catch(Exception e)
		{
			throw new RuntimeException("teardown() method failed to execute " +e);
		}
	}

}
