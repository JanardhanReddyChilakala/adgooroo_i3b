package com.adgooroo.qa.insights.test;

import java.io.FileNotFoundException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.innominds.itaf.driverinit.DriverManager;
import com.innominds.itaf.frameworkengine.Constants;
import com.innominds.itaf.utils.PropertyFileUtils;
import com.adgooroo.qa.insights.alerts.Alerts;
import com.adgooroo.qa.insights.alerts.AlertsDP;
import com.adgooroo.qa.insights.alerts.EditAlertsDP;
import com.adgooroo.qa.insights.login.Login;
import com.adgooroo.qa.insights.login.LoginDP;
import com.innominds.itaf.frameworkengine.CommonUtils;

public class HealthCheckTests extends DriverManager{
	
	Logger logger = LogManager.getLogger(HealthCheckTests.class.getName());

	private WebDriver driver=null;
	/**
	 * Inits the.
	 * @throws Exception 
	 * @throws FileNotFoundException 
	 */
	@Parameters({ "browser","os"})
	@BeforeClass(alwaysRun = true, groups = { "Regression","Sanity" })
	public void init(String browser,String osName) throws  Exception {

		System.out.println("===========================================================");
		System.out.println("==================== My Performance Alert script execution Started ==========================================");
		System.out.println("===========================================================");
		browserName=browser;
		os=osName;
		this.driver=getDriver(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "GridExecution"));
		driver.manage().deleteAllCookies();
		loadURL(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "web.app.url"), driver,browserName);
		driver.manage().window().maximize();
	}


	/*************************************************************************
	 * 
	 * TEST SUITE TESTS
	 * 
	 *************************************************************************/

	/*
	 * ****************************************
	 * Objects Creation
	 * ****************************************
	 */

	Login login = new Login();
	Alerts alert = new Alerts();
	
	/*
	 * ****************************************
	 * Data Providers
	 * ****************************************
	 */

	@DataProvider(name="LoginDP")
	public Object[][] LoginDP() {
		if("firefox".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("chrome".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("ie".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}
		return null;
	}
	
	@DataProvider(name="LoginDP2")
	public Object[][] LoginDP2() {
		if("firefox".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR2");
		}else if("chrome".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR2");
		}else if("ie".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR2");
		}
		return null;
	}
	
	
	@DataProvider(name="AlertsDP")
	public Object[][] AlertsDP() {
		return AlertsDP.createDP("DR1");
	}
	
	@DataProvider(name="EditAlertsDP")
	public Object[][] EditAlertsDP() {
		return EditAlertsDP.createDP("DR1");
	}

	
	@DataProvider(name="SpecCompDP")
	public Object[][] SpecCompDP() {
		//if("firefox".equalsIgnoreCase(browserName)){
		return AlertsDP.createDP("SpecComp");
		//}
		//return null;
	}
	@DataProvider(name="MyperfDP")
	public Object[][] MyperfDP() {
		//if("firefox".equalsIgnoreCase(browserName)){
		return AlertsDP.createDP("MyPerf");
		//}
		//return null;
	}
	@DataProvider(name="NewCompDP")
	public Object[][] NewCompDP() {
		//if("firefox".equalsIgnoreCase(browserName)){
		return AlertsDP.createDP("NewComp");
		//}
		//return null;
	}
	@DataProvider(name="NewInfriDP")
	public Object[][] NewInfriDP() {
		//if("firefox".equalsIgnoreCase(browserName)){
		return AlertsDP.createDP("NewInfri");
		//}
		//return null;
	}
	@DataProvider(name="PerfCompDP")
	public Object[][] PerfCompDP() {
		//if("firefox".equalsIgnoreCase(browserName)){
		return AlertsDP.createDP("PerfComp");
		//}
		//return null;
	}
	@DataProvider(name="BiddingDP")
	public Object[][] BiddingDP() {
		//if("firefox".equalsIgnoreCase(browserName)){
		return AlertsDP.createDP("Bidding");
		//}
		//return null;
	}
	
	
	/*
	 * ****************************************
	 * This test is verifies all the field in the Login page
	 * ****************************************
	 */
	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void validateLoginUI(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLoginUI(dp, driver);
	}
	
	/*
	 * ****************************************
	 * This test is verifies functionality of Login
	 * ****************************************
	 */
	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void loginWithValidCredentials(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogin(dp, driver);
	}
	
	/*
	 * ****************************************
	 * This test is verifies functionality of reLogin
	 * ****************************************
	 */
	@Test( dataProvider = "LoginDP2", enabled = true, groups = { "Regression","Sanity" })
	public void reloginWithValidCredentials(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogin(dp, driver);
	}

	/*
	 * ****************************************
	 * This test is verifies navigation Alert Manager
	 * ****************************************
	 */
	@Test( dataProvider = "AlertsDP", enabled = true, groups = { "Regression","Sanity" })
	public void navigateToAlertManager(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		alert.navigateToAlertManager(dp, driver);
	}
	
	/*
	 * ****************************************
	 * This test is for waitFor AlertTableToLoad
	 * ****************************************
	 */
	@Test( dataProvider = "AlertsDP", enabled = true, groups = { "Regression","Sanity" })
	public void waitForTableToLoad(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		alert.waitForTableLoad(dp, driver);
	}
	
	/*
	 * ****************************************
	 * This test is for AlertTable Sorting
	 * ****************************************
	 */
	@Test( dataProvider = "AlertsDP", enabled = true, groups = { "Regression","Sanity" })
	public void verifyAlertTableSort(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		alert.verifyAlertTableSort(dp, driver);
	}

	/*
	 * ****************************************
	 * This test is for Search of an alert
	 * ****************************************
	 */
	@Test( dataProvider = "AlertsDP", enabled = true, groups = { "Regression","Sanity" })
	public void searchforAlert(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		alert.searchAlert(dp, driver,"Auto");
	}
	
	/*
	 * ****************************************
	 * This test is to verify AlertCreator Permissions
	 * ****************************************
	 */
	@Test( dataProvider = "AlertsDP", enabled = true, groups = { "Regression","Sanity" })
	public void verifyAlertCreatorPermission(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		alert.navigateToAlertManager(dp, driver);
		alert.waitForTableLoad(dp, driver);
		alert.searchAlert(dp, driver,"Auto");
		alert.verifyAlertCreatorPermission(dp, driver);
	}
	
	/*
	 * ****************************************
	 * This test is to verify Non AlertCreator Permissions
	 * ****************************************
	 */
	@Test( dataProvider = "AlertsDP", enabled = true, groups = { "Regression","Sanity" })
	public void verifyAlertSharedPermission(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		alert.navigateToAlertManager(dp, driver);
		alert.waitForTableLoad(dp, driver);
		alert.searchAlert(dp, driver,"Auto");
		alert.verifyAlertSharedPermission(dp, driver);
	}
	
	/*
	 * ****************************************
	 * This test is to navigate to Alert Manager and Search for it
	 * ****************************************
	 */
	@Test( dataProvider = "AlertsDP", enabled = true, groups = { "Regression","Sanity" })
	public void alertNavigateAndSearch(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		alert.navigateToAlertManager(dp, driver);
		alert.waitForTableLoad(dp, driver);
		alert.searchAlert(dp, driver,"Auto");
	}
	
	/*
	 * ****************************************
	 * This test is to verify Actions Edit/Disable/Enable alert
	 * ****************************************
	 */
	@Test( dataProvider = "EditAlertsDP", enabled = true, groups = { "Regression","Sanity" })
	public void verifyEditAlertDisableEnable(EditAlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		alert.editAlertDisableEnable(dp, driver);
	}
	
	/*
	 * ****************************************
	 * This test is to verify Actions Delete alert
	 * ****************************************
	 */
	@Test( dataProvider = "EditAlertsDP", enabled = true, groups = { "Regression","Sanity" })
	public void verifyEditAlertDelete(EditAlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		//alert.editAlertDelete(dp, driver);
	}
	
	/*
	 * ****************************************
	 * This test is to verify switch Account Validation
	 * ****************************************
	 */
	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void switchAccountValidation(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateSwitchAccount(dp, driver);
	}
	
	
	/*
	 * ****************************************
	 * This test is to validateLogout
	 * ****************************************
	 */
	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void logoutValidations(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogout(dp, driver);
	}
	
	
	/*
	 * ****************************************
	 * This test is to create MyPerformance alert 
	 * ****************************************
	 */
	@Test( dataProvider = "MyperfDP", enabled = true, groups = { "Regression","Sanity" })
	public void create_MyPerf_SEMKW_DateD_FreqD_Checked(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("AlertName")+CommonUtils.getCurrentTimeStamp();//Auto_MP_SKW_DD_FD_Checked
		alert.createAlertName(dp, driver,uniqueName);
		alert.addDescription(dp, driver,dp.td.get("Add Message"));
		alert.selectAlertType(dp, driver,dp.td.get("AlertType"));
		alert.selectAdvertiser(dp, driver,dp.td.get("Advertiser"));
		alert.selectProductType(dp, driver,dp.td.get("ProductType"));
		alert.selectKeywordGroupBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"));
		alert.selectEngine(dp, driver,dp.td.get("Engine"));
		alert.selectRegion(dp, driver,dp.td.get("Region"));
		alert.selectDateRange(dp, driver,dp.td.get("DateRangeType"),dp.td.get("DateRangeValue"));
		alert.VerifyMultipleMetrcisThresholdAndSelect(dp, driver, dp.td.get("Metrics"), dp.td.get("Rule"), dp.td.get("ThresholdInput"), "");
		alert.selectAlertFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		alert.addMultipleRecipients(dp, driver,dp.td.get("EmailIds"));
		alert.verifyReceiveOnlyCheckbox(dp, driver,dp.td.get("ReceiveOnlyCheckbox"));
		alert.clickOnCreate(dp, driver,uniqueName);
		alert.navigateToAlertManager(dp, driver);
		alert.searchAlert(dp, driver,uniqueName);

	}
	
	/*
	 * ****************************************
	 * This test is to create New Competitor alert 
	 * ****************************************
	 */
	@Test( dataProvider = "NewCompDP", enabled = true, groups = { "Regression","Sanity" })
	public void create_NewComp_SEMCamp_DateW_FreqD_Checked(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("AlertName")+CommonUtils.getCurrentTimeStamp();//Auto_NC_SCamp_DW_FD_Checked,auto_Newcomp_healthcheck_
		alert.createAlertName(dp, driver,uniqueName);
		alert.addDescription(dp, driver,dp.td.get("Add Message"));
		alert.selectAlertType(dp, driver,dp.td.get("AlertType"));
		alert.selectCompetitorPerformanceType(dp,driver,dp.td.get("CompetitorPerformanceType"));
		alert.verifyCreateButtonDisabled(dp, driver);
		alert.selectProductType(dp, driver,dp.td.get("ProductType"));
		alert.selectKeywordGroupBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"));
		alert.selectEngine(dp, driver,dp.td.get("Engine"));
		alert.selectRegion(dp, driver,dp.td.get("Region"));
		alert.setCompetitorsNewInLast(dp, driver,dp.td.get("WhoNewInTheLastValue"),dp.td.get("WhoNewInTheLastRange"));
		alert.VerifyMultipleMetrcisThresholdAndSelect(dp, driver, dp.td.get("Metrics"), dp.td.get("Rule"), dp.td.get("ThresholdInput"), "");
		alert.selectAlertFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		alert.addMultipleRecipients(dp, driver,dp.td.get("EmailIds"));
		alert.verifyReceiveOnlyCheckbox(dp, driver,dp.td.get("ReceiveOnlyCheckbox"));
		alert.clickOnCreate(dp, driver,uniqueName);
		alert.navigateToAlertManager(dp, driver);
		alert.searchAlert(dp, driver,uniqueName);

	}
	
	/*
	 * ****************************************
	 * This test is to create New Infringers alert 
	 * ****************************************
	 */
	@Test( dataProvider = "NewInfriDP", enabled = true, groups = { "Regression","Sanity" })
	public void create_NewInfri_Brand_DateD_FreqD_Checked(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("AlertName")+CommonUtils.getCurrentTimeStamp();//Auto_NI_Brand_DD_FD_Checked
		alert.createAlertName(dp, driver,uniqueName);
		alert.addDescription(dp, driver,dp.td.get("Add Message"));
		alert.selectAlertType(dp, driver,dp.td.get("AlertType"));
		alert.selectTrademarkInfringementsType(dp, driver,dp.td.get("TrademarkAlertType"));
		alert.verifyCreateButtonDisabled(dp, driver);
		alert.selectKeywordGroupBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"));
		alert.verifyCreateButtonDisabled(dp, driver);
		alert.setTargetSetting(dp, driver,dp.td.get("TargetSetting"));
		alert.verifyCreateButtonDisabled(dp, driver);
		alert.setCompetitorsNewInLast(dp, driver,dp.td.get("WhoNewInTheLastValue"),dp.td.get("WhoNewInTheLastRange"));
		alert.selectAlertFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		alert.addMultipleRecipients(dp, driver,dp.td.get("EmailIds"));
		alert.verifyReceiveOnlyCheckbox(dp, driver,dp.td.get("ReceiveOnlyCheckbox"));
		alert.clickOnCreate(dp, driver,uniqueName);
		alert.navigateToAlertManager(dp, driver);
		alert.searchAlert(dp, driver,uniqueName);

	}
	
	/*
	 * ****************************************
	 * This test is to create Specific Competitor alert 
	 * ****************************************
	 */
	@Test( dataProvider = "SpecCompDP", enabled = true, groups = { "Regression","Sanity" })
	public void create_SpecComp_SEML_DateM_FreqW_Unchecked(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("AlertName")+CommonUtils.getCurrentTimeStamp();//Auto_SC_SL_DM_FW_Unchecked,auto_Speccomp_healthcheck_
		alert.createAlertName(dp, driver,uniqueName);
		alert.selectAlertType(dp, driver,dp.td.get("AlertType"));
		alert.addDescription(dp, driver,dp.td.get("Add Message"));
		alert.selectCompetitorPerformanceType(dp, driver,dp.td.get("CompetitorPerformanceType"));
		alert.verifyCreateButtonDisabled(dp, driver);
		alert.selectProductType(dp, driver,dp.td.get("ProductType"));
		alert.selectCompetitorSetDropdown(dp, driver, dp.td.get("CompetitorSet"));
		alert.verifyFieldsunderSEMLocalSelectKeyword(dp, driver,dp.td.get("KeywordGroupValue"));
		alert.setTargetSettingSEMLocal(dp, driver, dp.td.get("TargetSetting"));
		alert.selectDateRange(dp, driver,dp.td.get("DateRangeType"),dp.td.get("DateRangeValue"));
		alert.VerifyMultipleMetrcisThresholdAndSelect(dp, driver, dp.td.get("Metrics"), dp.td.get("Rule"), dp.td.get("ThresholdInput"), dp.td.get("AndOrRule"));
		alert.selectAlertFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		alert.addMultipleRecipients(dp, driver,dp.td.get("EmailIds"));
		alert.verifyReceiveOnlyCheckbox(dp, driver,dp.td.get("ReceiveOnlyCheckbox"));
		alert.clickOnCreate(dp, driver,uniqueName);
		alert.navigateToAlertManager(dp, driver);
		alert.searchAlert(dp, driver,uniqueName);

	}
	
	/*
	 * ****************************************
	 * This test is to create Performance Comparison alert 
	 * ****************************************
	 */
	@Test( dataProvider = "PerfCompDP", enabled = true, groups = { "Regression","Sanity" })
	public void create_PerfComp_PLAKW_DateW_FreqW_Unchecked(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("AlertName")+CommonUtils.getCurrentTimeStamp();//Auto_PC_PKW_DW_FW_Unchecked
		alert.createAlertName(dp, driver,uniqueName);
		alert.addDescription(dp, driver,dp.td.get("Add Message"));
		alert.selectAlertType(dp, driver,dp.td.get("AlertType"));
		alert.selectCompetitorPerformanceType(dp, driver,dp.td.get("CompetitorPerformanceType"));
		alert.verifyCreateButtonDisabled(dp, driver);
		alert.selectProductType(dp, driver,dp.td.get("ProductType"));
		alert.selectCompetingAdvertisers(dp, driver,dp.td.get("Advertiser1"),dp.td.get("Advertiser2"));
		alert.selectKeywordGroupBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"));
		alert.selectEngine(dp, driver,dp.td.get("Engine"));
		alert.selectRegion(dp, driver,dp.td.get("Region"));
		alert.selectDateRange(dp, driver,dp.td.get("DateRangeType"),dp.td.get("DateRangeValue"));
		alert.selectMetricforPerfComp(dp, driver, dp.td.get("Metrics"), dp.td.get("AndOrRule"));
		alert.selectAlertFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		alert.addMultipleRecipients(dp, driver,dp.td.get("EmailIds"));
		alert.verifyReceiveOnlyCheckbox(dp, driver,dp.td.get("ReceiveOnlyCheckbox"));
		alert.clickOnCreate(dp, driver,uniqueName);
		alert.navigateToAlertManager(dp, driver);
		alert.searchAlert(dp, driver,uniqueName);

	}
	
	/*
	 * ****************************************
	 * This test is to create Bidding Position alert 
	 * ****************************************
	 */
	@Test( dataProvider = "BiddingDP", enabled = true, groups = { "Regression","Sanity" })
	public void create_BiddPos_KW_DateM_FreqD_Unchecked(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("AlertName")+CommonUtils.getCurrentTimeStamp();//Auto_BD_KW_DM_FD_Unchecked
		alert.createAlertName(dp, driver,uniqueName);
		alert.addDescription(dp, driver,dp.td.get("Add Message"));
		alert.selectAlertType(dp, driver,dp.td.get("AlertType"));
		alert.selectTrademarkInfringementsType(dp, driver,dp.td.get("TrademarkAlertType"));
		alert.verifyCreateButtonDisabled(dp, driver);
		alert.selectKeywordGroupBrand(dp, driver, dp.td.get("KeywordGroupType"), dp.td.get("KeywordGroupValue"));
		alert.verifyCreateButtonDisabled(dp, driver);
		alert.setTargetSetting(dp, driver,dp.td.get("TargetSetting"));
		alert.selectCompetitorSetDropdown(dp, driver, dp.td.get("CompetitorSet"));
		alert.verifyCreateButtonDisabled(dp, driver);
		alert.scrollDown(driver);
		alert.selectDateRange(dp, driver,dp.td.get("DateRangeType"),dp.td.get("DateRangeValue"));
		alert.scrollDown(driver);
		alert.verifyAndSelectBiddingPosition(dp, driver,dp.td.get("BiddingValue"));
		alert.scrollDown(driver);
		alert.selectAlertFrequency(dp, driver,dp.td.get("FrequencyType"),dp.td.get("FrequencyValue"));
		alert.addMultipleRecipients(dp, driver,dp.td.get("EmailIds"));
		alert.verifyReceiveOnlyCheckbox(dp, driver,dp.td.get("ReceiveOnlyCheckbox"));
		alert.clickOnCreate(dp, driver,uniqueName);
		alert.navigateToAlertManager(dp, driver);
		alert.searchAlert(dp, driver,uniqueName);

	}





	@AfterClass(alwaysRun = true)
	public void tearDown() {
		System.out.println("===============================================================================================");
		System.out.println("====================  My Performance Alert script execution Completed ==============================");
		System.out.println("===============================================================================================");

		try
		{

			if(driver != null)
			{
				driver.quit();

			}
		}catch(Exception e)
		{
			throw new RuntimeException("teardown() method failed to execute " +e);
		}
	}


}

