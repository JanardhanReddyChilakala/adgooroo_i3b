package com.adgooroo.qa.insights.test;

import java.io.FileNotFoundException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.eclipse.jetty.websocket.api.SuspendToken;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.innominds.itaf.driverinit.DriverManager;
import com.innominds.itaf.frameworkengine.Constants;
import com.innominds.itaf.utils.PropertyFileUtils;
import com.adgooroo.qa.insights.subscriptions.Subscriptions;
import com.adgooroo.qa.insights.subscriptions.SubscriptionsDP;
import com.adgooroo.qa.insights.alerts.AlertsDP;
import com.adgooroo.qa.insights.login.Login;
import com.adgooroo.qa.insights.login.LoginDP;
import com.innominds.itaf.frameworkengine.CommonUtils;

public class CreateIndustryPPCSubscriptionsTest extends DriverManager {
	
	Logger logger = LogManager.getLogger(CreateSubscriptionsTest.class.getName());

	private WebDriver driver=null;
	/**
	 * Inits the.
	 * @throws Exception 
	 * @throws FileNotFoundException 
	 */
	@Parameters({ "browser","os"})
	@BeforeClass(alwaysRun = true, groups = { "Regression","Sanity" })
	public void init(String browser,String osName) throws  Exception {

		System.out.println("===========================================================");
		System.out.println("==================== MyCreate_Industry_PPC_KeywordComparison Performance Alert script execution Started ==========================================");
		System.out.println("===========================================================");
		browserName=browser;
		os=osName;
		this.driver=getDriver(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "GridExecution"));
		loadURL(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "web.app.url"), driver,browserName);
	}


	/*************************************************************************
	 * 
	 * TEST SUITE TESTS
	 * 
	 *************************************************************************/

	/*
	 * ****************************************
	 * Objects Creation
	 * ****************************************
	 */

	Login login = new Login();
	Subscriptions subscription = new Subscriptions();
	
	/*
	 * ****************************************
	 * Data Providers
	 * ****************************************
	 */

	@DataProvider(name="LoginDP")
	public Object[][] LoginDP() {
		if("firefox".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("chrome".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("ie".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}
		return null;
	}
	@DataProvider(name="SubscriptionsDP")
	public Object[][] Subscriptions() {
		return SubscriptionsDP.createDP("DR1");
	}
	

	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void loginWithValidCredentials(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogin(dp, driver);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type PPC
	 * and report type Movers and shakers in industry reports. 
	 * ****************************************
	 */
	
	@DataProvider(name="Industry_PPC_MoversShakersDP")
	public Object[][] Industry_PPC_MoversShakers() {
		return SubscriptionsDP.createDP("Industry_PPC_MoversShakers");
	}
	@Test( dataProvider = "Industry_PPC_MoversShakersDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_PPC_MoversShakers(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver, dp.td.get("TaxonomyTree"), "");
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type PPC
	 * and report type Industry Overview in industry reports. 
	 * ****************************************
	 */
	@DataProvider(name="Industry_PPC_IndustryOverviewDP")
	public Object[][] Industry_PPC_IndustryOverview() {
		return SubscriptionsDP.createDP("Industry_PPC_IndustryOverview");
	}
	@Test( dataProvider = "Industry_PPC_IndustryOverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_PPC_IndustryOverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver, dp.td.get("TaxonomyTree"), "");
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}

	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type PPC
	 * and report type Industry Trends in industry reports. 
	 * ****************************************
	 */
	@DataProvider(name="Industry_PPC_IndustryTrendsDP")
	public Object[][] Industry_PPC_IndustryTrends() {
		return SubscriptionsDP.createDP("Industry_PPC_IndustryTrends");
	}
	@Test( dataProvider = "Industry_PPC_IndustryTrendsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_PPC_IndustryTrends(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"),dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver, dp.td.get("TaxonomyTree"), "");
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type PPC
	 * and report type Industry Keywords in industry reports. 
	 * ****************************************
	 */
	@DataProvider(name="Industry_PPC_IndustryKeywordsDP")
	public Object[][] Industry_PPC_IndustryKeywords() {
		return SubscriptionsDP.createDP("Industry_PPC_IndustryKeywords");
	}
	@Test( dataProvider = "Industry_PPC_IndustryKeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_PPC_IndustryKeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver, dp.td.get("TaxonomyTree"), "");
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type PPC
	 * and report type Top Advertisers in industry reports. 
	 * ****************************************
	 */
	@DataProvider(name="Industry_PPC_TopAdvertisersDP")
	public Object[][] Industry_PPC_TopAdvertisers() {
		return SubscriptionsDP.createDP("Industry_PPC_TopAdvertisers");
	}
	@Test( dataProvider = "Industry_PPC_TopAdvertisersDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_PPC_TopAdvertisers(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver, dp.td.get("TaxonomyTree"), "");
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type PPC
	 * and report type Advertiser Creative in Advertiser reports. 
	 * ****************************************
	 */
	@DataProvider(name="Industry_PPC_AdvertiserCreativeDP")
	public Object[][] Industry_PPC_AdvertiserCreative() {
		return SubscriptionsDP.createDP("Industry_PPC_AdvertiserCreative");
	}
	@Test( dataProvider = "Industry_PPC_AdvertiserCreativeDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_PPC_AdvertiserCreative(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver, dp.td.get("TaxonomyTree"), "");
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type PPC
	 * and report type Advertiser Overview in Advertiser reports. 
	 * ****************************************
	 */
	@DataProvider(name="Industry_PPC_AdvertiserOverviewDP")
	public Object[][] Industry_PPC_AdvertiserOverview() {
		return SubscriptionsDP.createDP("Industry_PPC_AdvertiserOverview");
	}
	@Test( dataProvider = "Industry_PPC_AdvertiserOverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_PPC_AdvertiserOverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver, dp.td.get("TaxonomyTree"), "");
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type PPC
	 * and report type Advertiser Keywords in Advertiser reports. 
	 * ****************************************
	 */
	@DataProvider(name="Industry_PPC_AdvertiserKeywordsDP")
	public Object[][] Industry_PPC_AdvertiserKeywords() {
		return SubscriptionsDP.createDP("Industry_PPC_AdvertiserKeywords");
	}
	@Test( dataProvider = "Industry_PPC_AdvertiserKeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_PPC_AdvertiserKeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), "PPC");
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver, dp.td.get("TaxonomyTree"), "");
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}

	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type PPC
	 * and report type Keyword Comparison in Comparison reports. 
	 * ****************************************
	 */
	@DataProvider(name="Industry_PPC_KeywordComparisonDP")
	public Object[][] Industry_PPC_KeywordComparison() {
		return SubscriptionsDP.createDP("Industry_PPC_KeywordComparison");
	}
	@Test( dataProvider = "Industry_PPC_KeywordComparisonDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_PPC_KeywordComparison(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver, dp.td.get("TaxonomyTree"), "");
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		subscription.selectSecondAdvertisersEngineRegionDaterange(dp, driver, dp.td.get("Advertiser2"), dp.td.get("Engine"), dp.td.get("Region"), dp.td.get("DateRangeValue"),dp.td.get("BugID"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type PPC
	 * and report type Advertiser Comparison in Comparison reports. 
	 * ****************************************
	 */
	@DataProvider(name="Industry_PPC_AdvertiserComparisonDP")
	public Object[][] Industry_PPC_AdvertiserComparison() {
		return SubscriptionsDP.createDP("Industry_PPC_AdvertiserComparison");
	}
	@Test( dataProvider = "Industry_PPC_AdvertiserComparisonDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_PPC_AdvertiserComparison(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver, dp.td.get("TaxonomyTree"), "");
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		subscription.selectSecondAdvertisersEngineRegionDaterange(dp, driver, dp.td.get("Advertiser2"), dp.td.get("Engine"), dp.td.get("Region"), dp.td.get("DateRangeValue"),dp.td.get("BugID"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void logoutValidations(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogout(dp, driver);
	}



	/**This section is used to print the execution end and quit the browser*/

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		System.out.println("===============================================================================================");
		System.out.println("====================  Script execution Completed ==============================");
		System.out.println("===============================================================================================");

		try
		{

			if(driver != null)
			{
				driver.quit();

			}
		}catch(Exception e)
		{
			throw new RuntimeException("teardown() method failed to execute " +e);
		}
	}

}
