package com.adgooroo.qa.insights.test;

import java.io.FileNotFoundException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.innominds.itaf.driverinit.DriverManager;
import com.innominds.itaf.frameworkengine.Constants;
import com.innominds.itaf.utils.PropertyFileUtils;
import com.adgooroo.qa.insights.subscriptions.Subscriptions;
import com.adgooroo.qa.insights.subscriptions.SubscriptionsDP;
import com.adgooroo.qa.insights.login.Login;
import com.adgooroo.qa.insights.login.LoginDP;
import com.innominds.itaf.frameworkengine.CommonUtils;

public class CreateSubscriptionsTest extends DriverManager{
	
	Logger logger = LogManager.getLogger(CreateSubscriptionsTest.class.getName());

	private WebDriver driver=null;
	/**
	 * Inits the.
	 * @throws Exception 
	 * @throws FileNotFoundException 
	 */
	@Parameters({ "browser","os"})
	@BeforeClass(alwaysRun = true, groups = { "Regression","Sanity" })
	public void init(String browser,String osName) throws  Exception {

		System.out.println("===========================================================");
		System.out.println("==================== My Performance Alert script execution Started ==========================================");
		System.out.println("===========================================================");
		browserName=browser;
		os=osName;
		this.driver=getDriver(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "GridExecution"));
		loadURL(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "web.app.url"), driver,browserName);
	}


	/*************************************************************************
	 * 
	 * TEST SUITE TESTS
	 * 
	 *************************************************************************/

	/*
	 * ****************************************
	 * Objects Creation
	 * ****************************************
	 */

	Login login = new Login();
	Subscriptions subscription = new Subscriptions();
	
	/*
	 * ****************************************
	 * Data Providers
	 * ****************************************
	 */

	@DataProvider(name="LoginDP")
	public Object[][] LoginDP() {
		if("firefox".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("chrome".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("ie".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}
		return null;
	}
	@DataProvider(name="SubscriptionsDP")
	public Object[][] Subscriptions() {
		return SubscriptionsDP.createDP("DR1");
	}
	

	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void loginWithValidCredentials(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogin(dp, driver);
	}

	@Test( dataProvider = "SubscriptionsDP", enabled = true, groups = { "Regression","Sanity" })
	public void testScripts(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		driver.findElement(By.xpath("//a[@title='Subscriptions']/span")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//a[contains(text(), 'Subscription')]")).click();
		Thread.sleep(2000);
		subscription.selectVerifyProductType(dp, driver, "trademark", "");
		subscription.selectVerifyReportType(dp, driver, "Group Infringements by Ad Copy");
		subscription.selectVerifyKwgCampaignBrand(dp, driver, "brAnd", "TEST_QA_ALL", "mobile");
	}
	
	
	
	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void logoutValidations(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogout(dp, driver);
	}





	@AfterClass(alwaysRun = true)
	public void tearDown() {
		System.out.println("===============================================================================================");
		System.out.println("====================  Script execution Completed ==============================");
		System.out.println("===============================================================================================");

		try
		{

			if(driver != null)
			{
				driver.quit();

			}
		}catch(Exception e)
		{
			throw new RuntimeException("teardown() method failed to execute " +e);
		}
	}


}

