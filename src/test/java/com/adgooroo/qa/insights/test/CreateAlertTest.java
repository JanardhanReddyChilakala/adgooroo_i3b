package com.adgooroo.qa.insights.test;

import java.io.FileNotFoundException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.innominds.itaf.driverinit.DriverManager;
import com.innominds.itaf.frameworkengine.Constants;
import com.innominds.itaf.utils.PropertyFileUtils;
import com.adgooroo.qa.insights.alerts.Alerts;
import com.adgooroo.qa.insights.alerts.AlertsDP;
import com.adgooroo.qa.insights.login.Login;
import com.adgooroo.qa.insights.login.LoginDP;
import com.innominds.itaf.frameworkengine.CommonUtils;

public class CreateAlertTest extends DriverManager{
	
	Logger logger = LogManager.getLogger(CreateAlertTest.class.getName());

	private WebDriver driver=null;
	/**
	 * Inits the.
	 * @throws Exception 
	 * @throws FileNotFoundException 
	 */
	@Parameters({ "browser","os"})
	@BeforeClass(alwaysRun = true, groups = { "Regression","Sanity" })
	public void init(String browser,String osName) throws  Exception {

		System.out.println("===========================================================");
		System.out.println("==================== My Performance Alert script execution Started ==========================================");
		System.out.println("===========================================================");
		browserName=browser;
		os=osName;
		this.driver=getDriver(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "GridExecution"));
		loadURL(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "web.app.url"), driver,browserName);
	}


	/*************************************************************************
	 * 
	 * TEST SUITE TESTS
	 * 
	 *************************************************************************/

	/*
	 * ****************************************
	 * Objects Creation
	 * ****************************************
	 */

	Login login = new Login();
	Alerts alert = new Alerts();
	
	/*
	 * ****************************************
	 * Data Providers
	 * ****************************************
	 */

	@DataProvider(name="LoginDP")
	public Object[][] LoginDP() {
		if("firefox".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("chrome".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("ie".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}
		return null;
	}
	@DataProvider(name="AlertsDP")
	public Object[][] AlertsDP() {
		return AlertsDP.createDP("DR1");
	}
	

	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void loginWithValidCredentials(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogin(dp, driver);
	}

	@Test( dataProvider = "AlertsDP", enabled = true, groups = { "Regression","Sanity" })
	public void navigateToAlertManager(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		alert.navigateToAlertManager(dp, driver);
	}
	
	@Test( dataProvider = "AlertsDP", enabled = true, groups = { "Regression","Sanity" })
	public void searchforAlert(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		alert.searchAlert(dp, driver,"QA_MyPerformance_Auto170924170537");
	}
	
	@Test( dataProvider = "AlertsDP", enabled = true, groups = { "Regression","Sanity" })
	public void createMyPerformanceAlertCustom(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = "QA_MyPerformance_Auto"+CommonUtils.getCurrentTimeStamp();
		alert.createAlertName(dp, driver,uniqueName);
		alert.addDescription(dp, driver,"test message");
		alert.selectAlertType(dp, driver,"My Performance");
		alert.selectAdvertiser(dp, driver,"newe");
		alert.selectProductType(dp, driver,"SEM");
		alert.selectKeywordGroup(dp, driver,"NewEgg");
		alert.selectEngine(dp, driver,"Google");
		alert.selectRegion(dp, driver,"US");
		alert.selectDateRangeDaily(dp, driver,"30");
		alert.selectAlertFrequencyDailyAll(dp, driver);
		alert.addRecipient(dp, driver,"test@test.com");
		alert.clickOnCreate(dp, driver, uniqueName);
		alert.navigateToAlertManager(dp, driver);
		alert.searchAlert(dp, driver,uniqueName);

	}
	
	
	@Test( dataProvider = "AlertsDP", enabled = true, groups = { "Regression","Sanity" })
	public void createCompetitorsPerformaceAlertCustom(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = "QA_CompetitorsPerformace_Auto"+CommonUtils.getCurrentTimeStamp();
		alert.createAlertName(dp, driver,uniqueName);
		alert.addDescription(dp, driver,"test message");
		alert.selectAlertType(dp, driver,"Competitor(s) Performace");
		alert.selectCompetitorPerformanceType(dp, driver,"New Competitors");
		alert.selectProductType(dp, driver,"SEM");
		alert.selectKeywordGroup(dp, driver,"NewEgg");
		alert.selectEngine(dp, driver,"Google");
		alert.selectRegion(dp, driver,"US");
		alert.setCompetitorsNewInLast(dp, driver,"2","week(s)");
		alert.selectAlertFrequencyDailyAll(dp, driver);
		alert.addRecipient(dp, driver,"test@test.com");
		alert.clickOnCreate(dp, driver, uniqueName);
		alert.navigateToAlertManager(dp, driver);
		alert.searchAlert(dp, driver,uniqueName);

	}
	
	@Test( dataProvider = "AlertsDP", enabled = true, groups = { "Regression","Sanity" })
	public void createTrademarkInfringementsAlertCustom(AlertsDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = "QA_TrademarkInfringements_Auto"+CommonUtils.getCurrentTimeStamp();
		alert.createAlertName(dp, driver,uniqueName);
		alert.addDescription(dp, driver,"test message");
		alert.selectAlertType(dp, driver,"Trademark Infringements");
		alert.selectTrademarkInfringementsType(dp, driver,"New Infringers");
		alert.selectKeywordGroupBrand(dp, driver,"AdGooroo");
		alert.setTargetSetting(dp, driver,"Google-US");
		alert.setCompetitorsNewInLast(dp, driver,"2","month(s)");
		alert.selectAlertFrequencyDailyAll(dp, driver);
		alert.addRecipient(dp, driver,"test@test.com");
		alert.clickOnCreate(dp, driver, uniqueName);
		alert.navigateToAlertManager(dp, driver);
		alert.searchAlert(dp, driver,uniqueName);

	}
	
	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void logoutValidations(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogout(dp, driver);
	}





	@AfterClass(alwaysRun = true)
	public void tearDown() {
		System.out.println("===============================================================================================");
		System.out.println("====================  My Performance Alert script execution Completed ==============================");
		System.out.println("===============================================================================================");

		try
		{

			if(driver != null)
			{
				driver.quit();

			}
		}catch(Exception e)
		{
			throw new RuntimeException("teardown() method failed to execute " +e);
		}
	}


}

