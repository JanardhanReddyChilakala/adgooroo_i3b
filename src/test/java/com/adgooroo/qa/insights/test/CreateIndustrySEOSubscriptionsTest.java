/**
 * 
 */
package com.adgooroo.qa.insights.test;

import java.io.FileNotFoundException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.adgooroo.qa.insights.login.Login;
import com.adgooroo.qa.insights.login.LoginDP;
import com.adgooroo.qa.insights.subscriptions.Subscriptions;
import com.adgooroo.qa.insights.subscriptions.SubscriptionsDP;
import com.innominds.itaf.driverinit.DriverManager;
import com.innominds.itaf.frameworkengine.CommonUtils;
import com.innominds.itaf.frameworkengine.Constants;
import com.innominds.itaf.utils.PropertyFileUtils;

/**
 * @author ndorepalli
 *
 */
public class CreateIndustrySEOSubscriptionsTest extends DriverManager {

	Logger logger = LogManager.getLogger(CreateSubscriptionsTest.class.getName());

	private WebDriver driver=null;
	/**
	 * Inits the.
	 * @throws Exception 
	 * @throws FileNotFoundException 
	 */
	@Parameters({ "browser","os"})
	@BeforeClass(alwaysRun = true, groups = { "Regression","Sanity" })
	public void init(String browser,String osName) throws  Exception {

		System.out.println("===========================================================");
		System.out.println("==================== My Performance Alert script execution Started ==========================================");
		System.out.println("===========================================================");
		browserName=browser;
		os=osName;
		this.driver=getDriver(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "GridExecution"));
		loadURL(PropertyFileUtils.getPropValuesFromConfig(Constants.WEB_PROPERTIES_FILE, "web.app.url"), driver,browserName);
	}


	/*************************************************************************
	 * 
	 * TEST SUITE TESTS
	 * 
	 *************************************************************************/

	/*
	 * ****************************************
	 * Objects Creation
	 * ****************************************
	 */

	Login login = new Login();
	Subscriptions subscription = new Subscriptions();
	
	/*
	 * ****************************************
	 * Data Providers
	 * ****************************************
	 */

	@DataProvider(name="LoginDP")
	public Object[][] LoginDP() {
		if("firefox".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("chrome".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}else if("ie".equalsIgnoreCase(browserName)){
			return LoginDP.createDP("DR1");
		}
		return null;
	}
	@DataProvider(name="SubscriptionsDP")
	public Object[][] Subscriptions() {
		return SubscriptionsDP.createDP("DR1");
	}
	

	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void loginWithValidCredentials(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogin(dp, driver);
	}

	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type SEO
	 * and report type Industry overview in industry reports.
	 * Logout from the application 
	 * ****************************************
	 */
	
	@DataProvider(name="Industry_IndustryoverviewDP")
	public Object[][] Industry_Industryoverview() {
		return SubscriptionsDP.createDP("Industry_Industryoverview");
	}
	
	@Test( dataProvider = "Industry_IndustryoverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_Industryoverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver,dp.td.get("TaxonomyTree"),dp.td.get("TaxonomySubType"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		driver.findElement(By.id("root_includeSubdomains")).click();
		//subscription.selectVerifyCompetitorSet(dp, driver, dp.td.get("competitor"));
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	@Test( dataProvider = "LoginDP", enabled = true, groups = { "Regression","Sanity" })
	public void logoutValidations(LoginDP dp,ITestContext context)
	{
		context.setAttribute("dpName", dp.td.get("DataRow"));
		login.validateLogout(dp, driver);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type SEO
	 * and report type Industry keywords in industry reports.
	 * ****************************************
	 */
	@DataProvider(name="Industry_IndustrykeywordsDP")
	public Object[][] Industry_Industrykeywords() {
		return SubscriptionsDP.createDP("Industry_Industrykeywords");
	}
	
	@Test( dataProvider = "Industry_IndustrykeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_Industrykeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver,dp.td.get("TaxonomyTree"),dp.td.get("TaxonomySubType"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		driver.findElement(By.id("root_includeSubdomains")).click();
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type SEO
	 * and report type Top advertisers in industry reports.
	 * ****************************************
	 */
	@DataProvider(name="Industry_TopadvertisersDP")
	public Object[][] Industry_Topadvertisers() {
		return SubscriptionsDP.createDP("Industry_Topadvertisers");
	}
	
	@Test( dataProvider = "Industry_TopadvertisersDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_Topadvertisers(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver,dp.td.get("TaxonomyTree"),dp.td.get("TaxonomySubType"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		driver.findElement(By.id("root_includeSubdomains")).click();
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type SEO
	 * and report type Advertiser overview in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="Industry_AdvertiseroverviewDP")
	public Object[][] Industry_Advertiseroverview() {
		return SubscriptionsDP.createDP("Industry_Advertiseroverview");
	}
	
	@Test( dataProvider = "Industry_AdvertiseroverviewDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_Advertiseroverview(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver,dp.td.get("TaxonomyTree"),dp.td.get("TaxonomySubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		driver.findElement(By.id("root_includeSubdomains")).click();
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type SEO
	 * and report type Advertiser keywords in Advertiser reports.
	 * ****************************************
	 */
	@DataProvider(name="Industry_AdvertiserkeywordsDP")
	public Object[][] Industry_Advertiserkeywords() {
		return SubscriptionsDP.createDP("Industry_Advertiserkeywords");
	}
	
	@Test( dataProvider = "Industry_AdvertiserkeywordsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_Advertiserkeywords(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver,dp.td.get("TaxonomyTree"),dp.td.get("TaxonomySubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		driver.findElement(By.id("root_includeSubdomains")).click();
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type SEO
	 * and report type Advertiser comparison in comparison reports.
	 * ****************************************
	 */
	@DataProvider(name="Industry_AdvertisercomparisonDP")
	public Object[][] Industry_Advertisercomparison() {
		return SubscriptionsDP.createDP("Industry_Advertisercomparison");
	}
	
	@Test( dataProvider = "Industry_AdvertisercomparisonDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_Advertisercomparison(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver,dp.td.get("TaxonomyTree"),dp.td.get("TaxonomySubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		subscription.selectSecondAdvertisersEngineRegionDaterange(dp, driver, dp.td.get("Advertiser2"), dp.td.get("Engine"), dp.td.get("Region"), dp.td.get("DateRangeValue"),dp.td.get("BugID"));
		driver.findElement(By.id("root_includeSubdomains")).click();
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	
	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type SEO
	 * and report type Keyword comparison in comparison reports.
	 * ****************************************
	 */
	@DataProvider(name="Industry_KeywordcomparisonDP")
	public Object[][] Industry_Keywordcomparison() {
		return SubscriptionsDP.createDP("Industry_Keywordcomparison");
	}
	
	@Test( dataProvider = "Industry_KeywordcomparisonDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_Keywordcomparison(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver,dp.td.get("TaxonomyTree"),dp.td.get("TaxonomySubType"));
		subscription.selectAdvertiser(dp, driver, dp.td.get("Advertiser1"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		subscription.selectSecondAdvertisersEngineRegionDaterange(dp, driver, dp.td.get("Advertiser2"), dp.td.get("Engine"), dp.td.get("Region"), dp.td.get("dateNumber"),dp.td.get("BugID"));
		//subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		//subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("dateNumber"));
		driver.findElement(By.id("root_includeSubdomains")).click();
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/*
	 * ****************************************
	 * This test is used to create subscription product type Industry,industry type SEO
	 * and report type Keyword rankings in Keyword reports.
	 * ****************************************
	 */
	@DataProvider(name="Industry_KeywordrankingsDP")
	public Object[][] Industry_Keywordrankings() {
		return SubscriptionsDP.createDP("Industry_Keywordrankings");
	}
	
	@Test( dataProvider = "Industry_KeywordrankingsDP", enabled = true, groups = { "Regression","Sanity" })
	public void Create_Industry_Keywordrankings(SubscriptionsDP dp,ITestContext context) throws InterruptedException
	{
		
		context.setAttribute("dpName", dp.td.get("DataRow"));
		String uniqueName = dp.td.get("SubscriptionName")+CommonUtils.getCurrentTimeStamp();
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.createSubscriptionName(dp, driver, uniqueName, "");
		subscription.addDescription(dp, driver, dp.td.get("Add Message"), "");
		subscription.selectVerifyProductType(dp, driver, dp.td.get("ProductType"), dp.td.get("Product_SubType"));
		subscription.selectVerifyReportType(dp, driver, dp.td.get("ReportType"));
		subscription.selectVerifyTaxonomy(dp, driver,dp.td.get("TaxonomyTree"),dp.td.get("TaxonomySubType"));
		subscription.selectEngineRegionForSubscription(dp, driver, dp.td.get("Engine"), dp.td.get("Region"));
		subscription.selectIndustryKeyword(dp, driver, dp.td.get("Keyword"));
		subscription.selectSubscriptionDateRangeIndustry(dp, driver, dp.td.get("DateRangeValue"));
		//driver.findElement(By.id("root_includeSubdomains")).click();
		subscription.selectSubscriptionFrequency(dp, driver, dp.td.get("FrequencyType"), dp.td.get("FrequencyValue"));
		subscription.verifyCreateButtonDisabled(dp, driver);
		subscription.addMultipleRecipientsInSubscriptions(dp, driver, dp.td.get("EmailIds"));
		subscription.clickOnCreate(dp, driver, uniqueName);
		subscription.navigateToSubscriptionManager(dp, driver);
		subscription.searchSubscription(dp, driver, uniqueName);
	}
	/**This section is used to print the execution end and quit the browser*/
@AfterClass(alwaysRun = true)
public void tearDown() {
	System.out.println("===============================================================================================");
	System.out.println("====================  Script execution Completed ==============================");
	System.out.println("===============================================================================================");

	try
	{

		if(driver != null)
		{
			driver.quit();

		}
	}catch(Exception e)
	{
		throw new RuntimeException("teardown() method failed to execute " +e);
	}
}

}

