package com.adgooroo.qa.insights.alerts;

import static org.testng.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;

import com.innominds.itaf.frameworkengine.Constants;
import com.innominds.itaf.frameworkengine.PageActionUtils;

public class Alerts extends PageActionUtils{
	public static final int TIMEOUT = 300000;
	public static final int PULSE = 100;
	Logger logger = LogManager.getLogger(Alerts.class.getName());
	WebElement element = null;
	public WebDriverWait wait;
	//This is a test Comment
	HashMap objectMap=null;
	/**Navigates to alert manager screen*/
	public void navigateToAlertManager(AlertsDP dp, WebDriver driver)
	{		
		try
		{
		
			Reporter.log("........Insights Alert Manager........", true);
			getWebElement(dp.or, "alertManager",driver).click();
			waitForElementPresent(getWebElement(dp.or, "addAlert",driver),TIMEOUT, PULSE);
			String headerVal=getWebElement(dp.or, "header",driver).getText();
			Reporter.log(headerVal, true);
			System.out.println(headerVal);
			Thread.sleep(2000);
						
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}
	
	/**scrolls down the driver
	 * @param dp, driver, typeName
	 *
	 */
	public void scrollDown(WebDriver driver){
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("window.scrollBy(0,250)", "");
	}
	
	/** Searches for an alert in manager screen
	 * @param dp, driver, searchString
	 * @return boolean 
	 * */
	public boolean searchAlert(AlertsDP dp, WebDriver driver, String searchString)
	{	
		Boolean found = false;	
		try
		{
			
			Reporter.log("........Insights Alert Manager Search........", true);
			Assert.assertTrue(getWebElement(dp.or, "alertFilterButton",driver).isDisplayed());
			getWebElement(dp.or, "alertFilterButton",driver).click();
			WebElement alertFilterName = driver.findElements(By.xpath("//div[contains(@class,'HeaderCell')]//input")).get(7);
			clearText(alertFilterName);
			enterText(alertFilterName,searchString);
			Thread.sleep(2000);
			String searchResults=driver.findElement(By.xpath("//div[@class='react-grid-Canvas']//div[contains(@class,'Row')][1]/div[contains(@class,'Cell')][9]")).getText();
			if (searchResults.contains(searchString)){
				found=true;
			Reporter.log(searchString+" is found", true);}
			System.out.println(found);
			
						
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
		
		return found;

	}
	
	/** waits for the alert table to load 
	 * @param dp, driver, searchString
	 * @return boolean 
	 * */
	public void waitForTableLoad(AlertsDP dp, WebDriver driver)
	{
		try {	
		for (int i=0;i<Constants.EXPLICIT_TIMEOUT;i++){
			if (!(driver.findElement(By.tagName("body")).getText().contains("Loading your alerts...")))
				{System.out.println("Breaking loop for alert table load at: "+i);
				break;}
			else
				//System.out.println(getWebElement(dp.or, "welcomeNote",driver).isDisplayed());
			Thread.sleep(1000);	
			}
			
			Thread.sleep(7000);
		} catch(Exception e)
		{
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
	}
	
	/** Verifies the Alert table Sort on each column Name,Created,Frequency,Last run and Recipients
	 * @param dp, driver
	 *
	 * */
	public void verifyAlertTableSort(AlertsDP dp, WebDriver driver)
	{
		List <WebElement> tableHeaders = driver.findElements(By.xpath("//th[@class='griddle-table-heading-cell']/span"));
		List <String> strTableHeaders = Arrays.asList("NAME", "TYPE", "CREATED", "FREQUENCY","LAST RUN", "RECIPIENTS");
		/*This is to verify whether table headers are as expected */
			for (int i=0; i<strTableHeaders.size();++i)
				Assert.assertEquals(tableHeaders.get(i).getText().replace("▼", "").toLowerCase(), strTableHeaders.get(i).toLowerCase(), "verification for table headers");
			/* This is to verify the sorting of the table based on each column in both orders */
			/*   xPath for the table cells with y= row and x= column: //tr[@class='griddle-row'][y]/td[@class='griddle-cell'][x]*/
				for (int x=0; x<strTableHeaders.size();++x){
					/*Ascending*/
					tableHeaders.get(x).click();
					int totalRows = driver.findElements(By.xpath("//tr[@class='griddle-row']")).size();
					String[] cellElements = new String[totalRows]; 
						for (int y=0;y<totalRows;y++)
							cellElements[y] = driver.findElement(By.xpath("//tr[@class='griddle-row']["+String.valueOf(y+1)+"]/td[@class='griddle-cell']["+String.valueOf(x+1)+"]")).getText();
					List <String> copyOfCellElementsAsc = Arrays.asList(cellElements);
					Collections.sort(copyOfCellElementsAsc);
					Assert.assertTrue(Arrays.asList(cellElements).equals(copyOfCellElementsAsc), "Verified the order in ascending order");
					/*Descending*/
					tableHeaders.get(x).click();
					cellElements =  new String[totalRows]; 
						for (int y=0;y<totalRows;y++)
							cellElements[y] = driver.findElement(By.xpath("//tr[@class='griddle-row']["+String.valueOf(y+1)+"]/td[@class='griddle-cell']["+String.valueOf(x+1)+"]")).getText();
					List <String> copyOfCellElementsDesc = Arrays.asList(cellElements);
					Collections.sort(copyOfCellElementsDesc, Collections.reverseOrder());
					Assert.assertTrue(Arrays.asList(cellElements).equals(copyOfCellElementsDesc), "Verified the order in ascending order");
				}
	
	}
	
	/** Verifies the Alert table Sort on each column Name,Created,Frequency,Last run and Recipients
	 * @param dp, driver
	 *
	 * */
	public void verifyAlertCreatorPermission(AlertsDP dp, WebDriver driver)
	{
		WebElement actionCell =	driver.findElement(By.xpath("//div[@class='react-grid-Canvas']//div[contains(@class,'Row')][1]/div[contains(@class,'Cell')][8]"));	
		Assert.assertTrue(actionCell.isDisplayed());
		actionCell.findElement(By.xpath(".//select")).click();
		List<WebElement> availableActions = actionCell.findElements(By.xpath(".//option[not(@hidden)]"));
		List<String> actionNames = Arrays.asList("Recipients","Edit","Disable","Delete");
		/*Assertion for count of available actions*/
		Assert.assertEquals(availableActions.size(), actionNames.size(),"verification of number of actions available");
			for(int itr=0; itr<actionNames.size();++itr)
			{
				/*verification of each action name*/
				Assert.assertEquals(actionNames.get(itr),availableActions.get(itr).getText());
			} 
	}
	
	/** Verifies the number of actions available to the Alert Creator
	 * @param dp, driver
	 *
	 * */
	public void verifyAlertSharedPermission(AlertsDP dp, WebDriver driver)
	{
		WebElement actionCell =	driver.findElement(By.xpath("//div[@class='react-grid-Canvas']//div[contains(@class,'Row')][1]/div[contains(@class,'Cell')][8]"));	
		Assert.assertTrue(actionCell.isDisplayed());
		actionCell.findElement(By.xpath(".//select")).click();
		List<WebElement> availableActions = actionCell.findElements(By.xpath(".//option[not(@hidden)]"));
		List<String> actionNames = Arrays.asList("Recipients","Edit","Disable","Delete");
		/*Assertion for count of available actions*/
		Assert.assertEquals(availableActions.size(), 1,"verification of number of actions available");
		/*verification of available action name*/
		Assert.assertEquals(actionNames.get(0),availableActions.get(0).getText());
	
	}
	
	/** edit the Recipients by adding new email to the selected Alert
	 * @param dp, driver, emailAddr
	 *
	 * */
	public void editAlertAddRecipient(EditAlertsDP dp, WebDriver driver, String emailAddr)
	{
		WebElement actionCell =	driver.findElement(By.xpath("//tr[@class='griddle-row'][1]/td[@class='griddle-cell'][7]"));	
		Assert.assertTrue(actionCell.isDisplayed());
		actionCell.findElement(By.xpath(".//button[@id='actionDropDownMenu']")).click();
		WebElement btnRecipient = actionCell.findElement(By.xpath(".//a[text()='Recipients']"));
		wait.until(ExpectedConditions.visibilityOf(btnRecipient));
		btnRecipient.click();
		try {
		if (emailAddr.contains(";")||emailAddr.contains(","))
		{
			emailAddr = emailAddr.replace(",", ";");
			String[] emailsEach =emailAddr.split(";");
			
			List<WebElement> emailsAdded =driver.findElements(By.xpath("//div[contains(@class, 'emailTag')]"));
			String emailAddresses = null;
				for (int i=0; i<emailsAdded.size();++i)
					emailAddresses+=emailsAdded.get(i).getText();
			for (String emailAddrInList: emailsEach)
				Assert.assertFalse(emailAddresses.contains(emailAddrInList));
			wait.until(ExpectedConditions.visibilityOf(getWebElement(dp.or, "popupRecipientHeader",driver)));
			Assert.assertTrue(getWebElement(dp.or, "inputEmail",driver).isDisplayed());
			getWebElement(dp.or, "inputEmail",driver).sendKeys(emailAddr);
			getWebElement(dp.or, "buttonAddEmail",driver).click();
			List<WebElement> emailsAddedNew =driver.findElements(By.xpath("//div[contains(@class, 'emailTag')]"));
			emailAddresses = null;
				for (int i=0; i<emailsAddedNew.size();++i)
					emailAddresses+=emailsAddedNew.get(i).getText();
			for (String emailAddrInList: emailsEach)
				Assert.assertTrue(emailAddresses.contains(emailAddrInList));
		}
		else{
		
			List<WebElement> emailsAdded =driver.findElements(By.xpath("//div[contains(@class, 'emailTag')]"));
			String emailAddresses = null;
			for (int i=0; i<emailsAdded.size();++i)
			emailAddresses+=emailsAdded.get(i).getText();
			Assert.assertFalse(emailAddresses.contains(emailAddr));
			wait.until(ExpectedConditions.visibilityOf(getWebElement(dp.or, "popupRecipientHeader",driver)));
			Assert.assertTrue(getWebElement(dp.or, "inputEmail",driver).isDisplayed());
			getWebElement(dp.or, "inputEmail",driver).sendKeys(emailAddr);
			getWebElement(dp.or, "buttonAddEmail",driver).click();
			List<WebElement> emailsAddedNew =driver.findElements(By.xpath("//div[contains(@class, 'emailTag')]"));
			Assert.assertTrue(emailsAddedNew.get(emailsAddedNew.size()-1).getText().contains(emailAddr));
			getWebElement(dp.or, "inputEmail",driver).sendKeys(emailAddr);
			getWebElement(dp.or, "buttonAddEmail",driver).click();
			Assert.assertTrue(getWebElement(dp.or, "EmailError",driver).isDisplayed());
		
		}
		getWebElement(dp.or, "buttonSave",driver).click();
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
	}
	
	/** edit the Recipients by removing the existing email to the selected Alert
	 * @param dp, driver, emailAddr
	 *
	 * */
	public void editAlertRemoveRecipient(EditAlertsDP dp, WebDriver driver, String emailAddr)
	{
		WebElement actionCell =	driver.findElement(By.xpath("//tr[@class='griddle-row'][1]/td[@class='griddle-cell'][7]"));	
		Assert.assertTrue(actionCell.isDisplayed());
		actionCell.findElement(By.xpath(".//button[@id='actionDropDownMenu']")).click();
		WebElement btnRecipient = actionCell.findElement(By.xpath(".//a[text()='Recipients']"));
		wait.until(ExpectedConditions.visibilityOf(btnRecipient));
		btnRecipient.click();
		try {
			wait.until(ExpectedConditions.visibilityOf(getWebElement(dp.or, "popupRecipientHeader",driver)));
			WebElement emailRemove = driver.findElement(By.xpath("//span[@name='"+emailAddr+"' and contains(@class,'closeTag')]"));
			wait.until(ExpectedConditions.visibilityOf(emailRemove));
			emailRemove.click();
			Assert.assertFalse(driver.findElement(By.xpath("//span[text()='"+emailAddr+"']")).isDisplayed());
			getWebElement(dp.or, "buttonSave",driver).click();
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
		
	}
	
	/** Verifies error msg while adding the incorrect email format to the selected Alert
	 * @param dp, driver, emailAddr
	 *
	 * */
	public void incorrectEmailRecipient(EditAlertsDP dp, WebDriver driver, String emailAddr)
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(getWebElement(dp.or, "popupRecipientHeader",driver)));
			Assert.assertTrue(getWebElement(dp.or, "inputEmail",driver).isDisplayed());
			getWebElement(dp.or, "inputEmail",driver).sendKeys(emailAddr);
			getWebElement(dp.or, "buttonAddEmail",driver).click();Thread.sleep(2000);
			Assert.assertTrue(getWebElement(dp.or, "emailError",driver).isDisplayed(), getWebElement(dp.or, "emailError",driver).getText());
			
		} catch (Exception e) {
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
	}
	
	/** Verifies error msg while adding the existing email to the selected Alert
	 * @param dp, driver, emailAddr
	 *
	 * */
	public void existingEmailRecipient(EditAlertsDP dp, WebDriver driver, String emailAddr)
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(getWebElement(dp.or, "popupRecipientHeader",driver)));
			Assert.assertTrue(getWebElement(dp.or, "inputEmail",driver).isDisplayed());
			getWebElement(dp.or, "inputEmail",driver).sendKeys(emailAddr);
			getWebElement(dp.or, "buttonAddEmail",driver).click();Thread.sleep(2000);
			Assert.assertTrue(getWebElement(dp.or, "emailError",driver).isDisplayed(), getWebElement(dp.or, "emailError",driver).getText());
			
		} catch (Exception e) {
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
	}
	
	public void editAlertEditMetrics(AlertsDP dp, WebDriver driver)
	{
		
	}
	
	/**selects/edits frequency of the selected Alert
	 * @param dp, driver
	 *
	 * */
	public void editAlertUpdateFrequency(AlertsDP dp, WebDriver driver)
	{
		
		WebElement actionCell =	driver.findElement(By.xpath("//tr[@class='griddle-row'][1]/td[@class='griddle-cell'][7]"));	
		Assert.assertTrue(actionCell.isDisplayed());
		actionCell.findElement(By.xpath(".//button[@id='actionDropDownMenu']")).click();
		WebElement btnUpdateFrequency = actionCell.findElement(By.xpath(".//a[text()='Update Frequency']"));
		wait.until(ExpectedConditions.visibilityOf(btnUpdateFrequency));
		btnUpdateFrequency.click();
				
		try {
			
		Random r = new Random();
		int n =r.nextInt(3)+1;
		switch(n){
		case 1:
			wait.until(ExpectedConditions.visibilityOf(getWebElement(dp.or, "popupUpdateFrequencyHeader",driver)));
			getWebElement(dp.or, "alertFrequencyDaily",driver).click();
			n =r.nextInt(3)+1;
			break;  
		case 2:
			wait.until(ExpectedConditions.visibilityOf(getWebElement(dp.or, "popupUpdateFrequencyHeader",driver)));
			getWebElement(dp.or, "alertFrequencyWeekly",driver).click();
			break;
		case 3:
			wait.until(ExpectedConditions.visibilityOf(getWebElement(dp.or, "popupUpdateFrequencyHeader",driver)));
			getWebElement(dp.or, "alertFrequencyMonthly",driver).click();
			break;
		    
		default:     
			Reporter.log("alert type not correct", true);
		
		}
		} catch (Exception e) {
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
		
	}
	
	/**Disables/enables the selected Alert
	 * @param dp, driver
	 *
	 * */
	public void editAlertDisableEnable(EditAlertsDP dp, WebDriver driver)
	{
		WebElement actionCell =	driver.findElement(By.xpath("//div[@class='react-grid-Canvas']//div[contains(@class,'Row')][1]/div[contains(@class,'Cell')][8]"));	
		String alertName =	driver.findElement(By.xpath("//div[@class='react-grid-Canvas']//div[contains(@class,'Row')][1]/div[contains(@class,'Cell')][9]")).getAttribute("value").trim();
		String alertLastRun =	driver.findElement(By.xpath("//div[@class='react-grid-Canvas']//div[contains(@class,'Row')][1]/div[contains(@class,'Cell')][6]")).getAttribute("value").trim();
		Assert.assertTrue(actionCell.isDisplayed());
		actionCell.findElement(By.xpath(".//select")).click();
		WebElement btnDisable = actionCell.findElement(By.xpath(".//option[@value='Disable']"));
		try {
			waitForElementPresent(btnDisable,TIMEOUT, PULSE);
		} catch (Exception e) {
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
		//wait.until(ExpectedConditions.visibilityOf(btnDisable));
		btnDisable.click();
		String toastMessageDisable = verifyEditToast(dp,driver);
		Assert.assertEquals(toastMessageDisable.trim(), alertName+" was successfully disabled!");
		try {
			Thread.sleep(5000);
			WebElement alertFilterName = driver.findElements(By.xpath("//div[contains(@class,'HeaderCell')]//input")).get(7);
			clearText(alertFilterName);
			enterText(alertFilterName,alertName);
			Thread.sleep(500);
		} catch (Exception e) {
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
		Assert.assertEquals(driver.findElement(By.xpath("//div[@class='react-grid-Canvas']//div[contains(@class,'Row')][1]/div[contains(@class,'Cell')][6]")).getAttribute("value").trim(),"Disabled");
		/*following steps are for enable alert verification*/
		actionCell.findElement(By.xpath(".//select")).click();
		WebElement btnEnable = actionCell.findElement(By.xpath(".//option[@value='Enable']"));
		try {
			waitForElementPresent(btnEnable,TIMEOUT, PULSE);
		} catch (Exception e) {
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
		//wait.until(ExpectedConditions.visibilityOf(btnEnable));
		btnEnable.click();
		String toastMessageEnable = verifyEditToast(dp,driver);
		Assert.assertEquals(toastMessageEnable.trim(), alertName+" was successfully enabled!");
		try {
			Thread.sleep(2000);
		} catch (Exception e) {
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
		Assert.assertEquals(driver.findElement(By.xpath("//div[@class='react-grid-Canvas']//div[contains(@class,'Row')][1]/div[contains(@class,'Cell')][6]")).getAttribute("value").trim(),alertLastRun);
	}
	
	/**Disables/enables the selected Alert
	 * @param dp, driver
	 *
	 * */
	public String verifyEditToast(EditAlertsDP dp, WebDriver driver)
	{
		
		try {
			
			WebElement toastNotification = getWebElement(dp.or, "toastNotification",driver);
			waitForElementPresent(toastNotification,TIMEOUT, PULSE);
			Thread.sleep(1500);
			String toastMessage = toastNotification.getText();
			Thread.sleep(2000);
			return toastMessage;
			
		
		} catch (Exception e) {
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			return null;
		}
	}
	
	/**Deletes the selected Alert
	 * @param dp, driver
	 *
	 * */
	public void editAlertDelete(EditAlertsDP dp, WebDriver driver)
	{
		WebElement actionCell =	driver.findElement(By.xpath("//div[@class='react-grid-Canvas']//div[contains(@class,'Row')][1]/div[contains(@class,'Cell')][8]"));	
		String alertName =	driver.findElement(By.xpath("//div[@class='react-grid-Canvas']//div[contains(@class,'Row')][1]/div[contains(@class,'Cell')][9]")).getAttribute("value").trim();
		Assert.assertTrue(actionCell.isDisplayed());
		actionCell.findElement(By.xpath(".//select")).click();
		WebElement btnDelete = actionCell.findElement(By.xpath(".//option[@value='Delete']"));
		try {
			waitForElementPresent(btnDelete,TIMEOUT, PULSE);
		} catch (Exception e) {
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
		//wait.until(ExpectedConditions.visibilityOf(btnDelete));
		btnDelete.click();
		try {
			waitForElementPresent(getWebElement(dp.or, "popupDeleteHeader",driver),TIMEOUT, PULSE);
			//wait.until(ExpectedConditions.visibilityOf(getWebElement(dp.or, "popupDeleteHeader",driver)));
			Assert.assertEquals(getWebElement(dp.or, "popupDeleteText",driver).getText().trim(), "Are you sure you wish to delete "+alertName+"?");
			getWebElement(dp.or, "buttonOk",driver).click();
			Thread.sleep(1000);
			String toastMessageDelete = verifyEditToast(dp,driver);
			Assert.assertEquals(toastMessageDelete.trim(), "Nice! You successfully deleted the "+alertName+" alert!");
			Thread.sleep(3000);
			//Assert.assertTrue(getWebElement(dp.or, "alertFilterButton",driver).isDisplayed());
			//getWebElement(dp.or, "alertFilterButton",driver).click();
			WebElement alertFilterName = driver.findElements(By.xpath("//div[contains(@class,'HeaderCell')]//input")).get(7);
			clearText(alertFilterName);
			enterText(alertFilterName,alertName);
			Thread.sleep(500);
			Assert.assertTrue(driver.findElement(By.xpath("//div[@class='react-grid-Empty']/div[text()='No Alert Data to display.']")).isDisplayed());
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
	}
	
	//need to know
	public void editDeleteExtra(AlertsDP dp, WebDriver driver)
	{
		navigateToAlertManager(dp, driver);
		waitForTableLoad(dp, driver);
		List <WebElement> tableHeaders = driver.findElements(By.xpath("//th[@class='griddle-table-heading-cell']/span"));
		
		WebElement actionCell = null;
		WebElement btnDelete = null;
		while(searchAlert(dp, driver, "healthcheck"))
		{
		
		try {
			Thread.sleep(500);
			tableHeaders = driver.findElements(By.xpath("//th[@class='griddle-table-heading-cell']/span"));
			tableHeaders.get(4).click();
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		actionCell =	driver.findElement(By.xpath("//tr[@class='griddle-row'][1]/td[@class='griddle-cell'][7]"));	
		String alertName =	driver.findElement(By.xpath("//tr[@class='griddle-row'][1]/td[@class='griddle-cell'][1]")).getText().trim();
		//Assert.assertTrue(actionCell.isDisplayed());
		actionCell.findElement(By.xpath(".//button[@id='actionDropDownMenu']")).click();
		btnDelete = actionCell.findElement(By.xpath(".//a[text()='Delete']"));
		try {
			waitForElementPresent(btnDelete,TIMEOUT, PULSE);
		} catch (Exception e) {
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
		//wait.until(ExpectedConditions.visibilityOf(btnDelete));
		btnDelete.click();
		try {
			waitForElementPresent(getWebElement(dp.or, "popupDeleteHeader",driver),TIMEOUT, PULSE);
			//wait.until(ExpectedConditions.visibilityOf(getWebElement(dp.or, "popupDeleteHeader",driver)));
			//Assert.assertEquals(getWebElement(dp.or, "popupDeleteText",driver).getText().trim(), "Are you sure you wish to delete "+alertName+"?");
			getWebElement(dp.or, "buttonOk",driver).click();
			//String toastMessageDelete = verifyEditToast(dp,driver);
			//Assert.assertEquals(toastMessageDelete.trim(), "Nice! You successfully deleted the "+alertName+" alert!");
			Thread.sleep(4000);
			//Assert.assertTrue(driver.findElement(By.className("griddle-noResults")).isDisplayed());
			//getWebElement(dp.or, "addAlert",driver).click();
			//navigateToAlertManager(dp, driver);
			//waitForTableLoad(dp, driver);
			tableHeaders=null;
			actionCell = null;
			btnDelete = null;
		
		} catch (Exception e) {
		 
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
		}
		
	}
	
	/**enters the text into create alert name field
	 * @param dp, driver,alertName
	 *
	 * */
	public void createAlertName(AlertsDP dp, WebDriver driver, String alertName)
	{		
		try
		{		
			Reporter.log("Creating alert name", true);
			getWebElement(dp.or, "addAlert",driver).click();
			waitForElementPresent(getWebElement(dp.or, "alertName",driver),TIMEOUT, PULSE);
			clearText(getWebElement(dp.or, "alertName",driver));
			enterText(getWebElement(dp.or, "alertName",driver),alertName);
			Thread.sleep(2000);
			Assert.assertEquals(getWebElement(dp.or, "alertName",driver).getAttribute("value"), alertName);
			Reporter.log("Alert name Entered", true);
			Thread.sleep(2000);
			scrollDown(driver);
			String pathValue="//body/*";
			driver.findElement(By.xpath(pathValue)).click();			
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}
	
	
	/*public void selectAlertType(AlertsDP dp, WebDriver driver, String typeName)
	{		
		try
		{		
			Reporter.log("Selecting alert type", true);
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "myPerformance",driver)) && isElementDisplayed(getWebElement(dp.or, "competitorsPerformace",driver)) && isElementDisplayed(getWebElement(dp.or, "trademarkInfringements",driver)));
			switch(typeName){    
			case "My Performance":    
				getWebElement(dp.or, "myPerformance",driver).click();
				Assert.assertTrue(getWebElement(dp.or, "myPerformance",driver).getAttribute("class").contains("active"));
				Reporter.log("alert type selected - "+typeName, true);
			 break;  //optional  
			case "Competitor(s) Performace":    
				getWebElement(dp.or, "competitorsPerformace",driver).click();
				Assert.assertTrue(getWebElement(dp.or, "competitorsPerformace",driver).getAttribute("class").contains("active"));
				Reporter.log("alert type selected - "+typeName, true);
				break;  //optional  
			case "Trademark Infringements":    
				getWebElement(dp.or, "trademarkInfringements",driver).click();
				Assert.assertTrue(getWebElement(dp.or, "trademarkInfringements",driver).getAttribute("class").contains("active"));
				Reporter.log("alert type selected - "+typeName, true);
				 break;    
			    
			default:     
				Reporter.log("alert type not correct", true);
			}
			scrollDown(driver);
			
									
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}*/
	
	/*public void selectCompetitorPerformanceType(AlertsDP dp, WebDriver driver, String typeName)
	{		
		try
		{		
			Reporter.log("Selecting Competitor Performance Type", true);
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "newCompetitors",driver)) && isElementDisplayed(getWebElement(dp.or, "specificCompetitors",driver)) && isElementDisplayed(getWebElement(dp.or, "headToHead",driver)));
			switch(typeName){    
			case "New Competitors":    
				getWebElement(dp.or, "newCompetitors",driver).click();
				Assert.assertTrue(getWebElement(dp.or, "newCompetitors",driver).getAttribute("class").contains("active"));
				Reporter.log("Competitor Performance type selected - "+typeName, true);
			 break;  //optional  
			case "Specific Competitors":    
				getWebElement(dp.or, "specificCompetitors",driver).click();
				Assert.assertTrue(getWebElement(dp.or, "specificCompetitors",driver).getAttribute("class").contains("active"));
				Reporter.log("Competitor Performance type selected - "+typeName, true);
				break;  //optional  
			case "Head To Head":    
				getWebElement(dp.or, "headToHead",driver).click();
				Assert.assertTrue(getWebElement(dp.or, "headToHead",driver).getAttribute("class").contains("active"));
				Reporter.log("Competitor Performance type selected - "+typeName, true);
				 break;    
			    
			default:     
				Reporter.log("Competitor Performance type not correct", true);
			}
			scrollDown(driver);
			
									
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}*/
	
	/*public void selectTrademarkInfringementsType(AlertsDP dp, WebDriver driver, String typeName)
	{		
		try
		{		
			Reporter.log("Selecting Trademark Infringements Type", true);
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "newInfringers",driver)) && isElementDisplayed(getWebElement(dp.or, "biddingPosition",driver)));
			switch(typeName){    
			case "New Infringers":    
				getWebElement(dp.or, "newInfringers",driver).click();
				Assert.assertTrue(getWebElement(dp.or, "newInfringers",driver).getAttribute("class").contains("active"));
				Reporter.log("Trademark Infringements type selected - "+typeName, true);
			 break;  //optional  
			case "Bidding Position":    
				getWebElement(dp.or, "biddingPosition",driver).click();
				Assert.assertTrue(getWebElement(dp.or, "biddingPosition",driver).getAttribute("class").contains("active"));
				Reporter.log("Trademark Infringements type selected - "+typeName, true);
				break;  //optional  
					    
			default:     
				Reporter.log("Trademark Infringements type not correct", true);
			}
			scrollDown(driver);
			
									
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}*/
	
	/**selects the advertiser
	 * @param dp, driver, advertisertName
	 *
	 * */
	public void selectAdvertiser(AlertsDP dp, WebDriver driver, String advertisertName)
	{		
		try
		{		
			Reporter.log("Selecting Advertiser", true);
			//getWebElement(dp.or, "selectAdvertiserClearButton",driver).click();
			enterText(getWebElement(dp.or, "selectAdvertiserInput",driver),advertisertName);
			Thread.sleep(2000);
			getWebElement(dp.or, "selectAdvertiserList",driver).findElement(By.xpath("//div[@role='option' and text()='"+advertisertName+"']")).click();
			String selectedValue = getWebElement(dp.or, "selectAdvertiserText",driver).getText();
			Assert.assertEquals(selectedValue,advertisertName);
			Reporter.log("Selected Advertiser", true);
			scrollDown(driver);
						
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}
	
	/**selects the Target Setting
	 * @param dp, driver, targetName
	 *
	 * */
	public void setTargetSetting(AlertsDP dp, WebDriver driver, String targetName)
	{		
		try
		{		
			Reporter.log("Selecting Target Setting", true);
			enterText(getWebElement(dp.or, "targetSettingInput",driver),targetName);
			Thread.sleep(1000);
			driver.findElement(By.xpath("//strong[text()='"+targetName+"']")).click();
			//String selectedValue = getWebElement(dp.or, "targetSettingInput",driver).getText();
			//Assert.assertEquals(selectedValue,advertisertName);
			Reporter.log("Selected Target Setting", true);
			scrollDown(driver);
						
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}
	
	/**selects the Product Type
	 * @param dp, driver, typeName
	 *
	 */
	public void selectProductType(AlertsDP dp, WebDriver driver, String typeName)
	{		
		try
		{		
			Reporter.log("Selecting Product type", true);
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or, "semProduct",driver)) && isElementDisplayed(getWebElement(dp.or, "semLocalProduct",driver)) && isElementDisplayed(getWebElement(dp.or, "plaProduct",driver)));
			switch(typeName){    
			case "SEM":    
				getWebElement(dp.or, "semProduct",driver).click();
				Assert.assertTrue(getWebElement(dp.or, "semProduct",driver).getAttribute("class").contains("active"));
				Reporter.log("Product type selected - "+typeName, true);
			 break;  //optional  
			case "SEM Local":    
				getWebElement(dp.or, "semLocalProduct",driver).click();
				Assert.assertTrue(getWebElement(dp.or, "semLocalProduct",driver).getAttribute("class").contains("active"));
				Reporter.log("Product type selected - "+typeName, true);
				break;  //optional  
			case "PLA":    
				getWebElement(dp.or, "plaProduct",driver).click();
				Assert.assertTrue(getWebElement(dp.or, "plaProduct",driver).getAttribute("class").contains("active"));
				Reporter.log("Product type selected - "+typeName, true);
				 break;    
			    
			default:     
				Reporter.log("Product type not correct", true);
			}
			scrollDown(driver);
						
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}
	
	/**selects the Keyword Group
	 * @param dp, driver, keywordGroup
	 *
	 */
	public void selectKeywordGroup(AlertsDP dp, WebDriver driver, String keywordGroup)
	{		
		try
		{		
			Reporter.log("Selecting Keyword Group", true);
			getWebElement(dp.or, "selectKeywordCampaign",driver).click();
			Assert.assertTrue(getWebElement(dp.or, "keywordButton",driver).isDisplayed() && getWebElement(dp.or, "campaignsButton",driver).isDisplayed());
			enterText(getWebElement(dp.or, "searchKeywordCampaign",driver),keywordGroup);
			Thread.sleep(500);
			driver.findElement(By.xpath("//button[text()='"+keywordGroup+"']")).click();
			Thread.sleep(500);
			Assert.assertTrue(getWebElement(dp.or, "selectKeywordCampaign",driver).getText().contains(keywordGroup));
			Reporter.log("Selecting Keyword Group", true);
			scrollDown(driver);
						
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
	}
	
	/**selects the Keyword Group/Brand
	 * @param dp, driver, keywordGroup/brand
	 *
	 */
	public void selectKeywordGroupBrand(AlertsDP dp, WebDriver driver, String keywordGroup)
	{		
		try
		{		
			Reporter.log("Selecting Keyword Group/Brand", true);
			getWebElement(dp.or, "selectKeywordCampaign",driver).click();
			Assert.assertTrue(getWebElement(dp.or, "searchKeywordCampaign",driver).isDisplayed());
			enterText(getWebElement(dp.or, "searchKeywordCampaign",driver),keywordGroup);
			Thread.sleep(500);
			driver.findElement(By.xpath("//button[text()='"+keywordGroup+"']")).click();
			Thread.sleep(500);
			Assert.assertTrue(getWebElement(dp.or, "selectKeywordCampaign",driver).getText().contains(keywordGroup));
			Reporter.log("Selecting Keyword Group/Brand", true);
			scrollDown(driver);
						
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
	}
	
	/**selects the Engine
	 * @param dp, driver, engineName
	 *
	 */
	public void selectEngine(AlertsDP dp, WebDriver driver, String engineName)
	{		
		try
		{		
			Reporter.log("Selecting Engine", true);
			selectByVisibleText(getWebElement(dp.or, "engineSelect",driver),engineName);
			Thread.sleep(1000);
			Reporter.log("Selected Engine", true);
			scrollDown(driver);
						
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}

	/**selects the Region
	 * @param dp, driver, regionName
	 *
	 */
	public void selectRegion(AlertsDP dp, WebDriver driver, String regionName)
	{		
		try
		{		
			Reporter.log("Selecting Region", true);
			selectByVisibleText(getWebElement(dp.or, "regionSelect",driver),regionName);
			Thread.sleep(1000);
			Reporter.log("Selected Region", true);
			scrollDown(driver);
						
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}
	/**selects Date range for Daily
	 * @param dp, driver, dateNumber
	 *
	 */
	public void selectDateRangeDaily(AlertsDP dp, WebDriver driver, String dateNumber)
	{		
		try
		{		
			Reporter.log("Selecting Date Range", true);
			getWebElement(dp.or, "dateRangeDaily",driver).click();
			Thread.sleep(500);
			Assert.assertTrue(getWebElement(dp.or, "chooseLastDaysRadio",driver).isDisplayed());
			getWebElement(dp.or, "chooseLastDaysRadio",driver).click();
			selectByVisibleText(getWebElement(dp.or, "chooseLastDaysSelect",driver),dateNumber);
			getWebElement(dp.or, "saveButton",driver).click();
			Reporter.log("Selected Date Range", true);
			scrollDown(driver);
						
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}
	
	/**selects Competitor set who are New In Last 
	 * @param dp, driver, dateNumber, timeType
	 *
	 */
	public void setCompetitorsNewInLast(AlertsDP dp, WebDriver driver, String dateNumber, String timeType)
	{		
		try
		{		
			Reporter.log("Selecting Competitors New In Last", true);
			Assert.assertTrue(getWebElement(dp.or, "competitorsNewInLastNumber",driver).isDisplayed());
			selectByVisibleText(getWebElement(dp.or, "competitorsNewInLastType",driver),timeType);
			enterText(getWebElement(dp.or, "competitorsNewInLastNumber",driver),dateNumber);
			Reporter.log("Selected Competitors New In Last", true);
			scrollDown(driver);
						
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}
	
	/**selects AlertFrequency of all days for Daily
	 * @param dp, driver
	 *
	 */
	public void selectAlertFrequencyDailyAll(AlertsDP dp, WebDriver driver)
	{		
		try
		{		
			Reporter.log("Selecting Alert Frequency", true);
			getWebElement(dp.or, "alertFrequencyDaily",driver).click();
			Thread.sleep(500);
			Assert.assertTrue(getWebElement(dp.or, "dailyFrequencyAllDaysButton",driver).isDisplayed());
			getWebElement(dp.or, "dailyFrequencyAllDaysButton",driver).click();
			Thread.sleep(500);
			getWebElement(dp.or, "saveButton",driver).click();
			Reporter.log("Selected Alert Frequency", true);
			scrollDown(driver);
						
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}
	
	/**adds Recipients while creation of the alert
	 * @param dp, driver, emailID
	 *
	 */
	public void addRecipient(AlertsDP dp, WebDriver driver, String emailID)
	{		
		try
		{		
			Reporter.log("Adding recepient email", true);
			enterText(getWebElement(dp.or, "alertRecipient",driver),emailID);
			getWebElement(dp.or, "addRecipientButton",driver).click();
			Thread.sleep(500);
			Assert.assertTrue(getWebElement(dp.or, "addedRecipients",driver).getText().contains(emailID));
			Reporter.log("Added recepient email", true);
			scrollDown(driver);
						
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}
	
	/**adds Description while creation of the alert
	 * @param dp, driver, message
	 *
	 */
	public void addDescription(AlertsDP dp, WebDriver driver, String message)
	{		
		try
		{		
			Reporter.log("Adding message", true);
			getWebElement(dp.or, "showOptionalDescription",driver).click();
			Thread.sleep(500);
			enterText(getWebElement(dp.or, "optionalDescription",driver),message);
			Assert.assertTrue(getWebElement(dp.or, "optionalDescription",driver).getText().contains(message));
			Reporter.log("Added message", true);
			scrollDown(driver);
						
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}
	
	/**Clicks on Create button, also verifies whether it got created or not
	 * @param dp, driver, alertName
	 *
	 */
	public void clickOnCreate(AlertsDP dp, WebDriver driver, String alertName) {
		try {
			Reporter.log("Clicking On Create", true);
			Assert.assertTrue(getWebElement(dp.or, "createButton", driver)
					.isEnabled());
			getWebElement(dp.or, "createButton", driver).click();
			waitForElementPresent(
					getWebElement(dp.or, "toastNotification", driver),
					TIMEOUT, PULSE);
			Thread.sleep(1500);
			String successText = getWebElement(dp.or, "toastNotification", driver).getText();
			Assert.assertEquals(successText.trim(), "Successfully created "+alertName+"!", "Validation for alert created success message");
			Reporter.log("Clicked On Create", true);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**Verifies the create button is disabled or not
	 * @param dp, driver
	 *
	 */
	public void verifyCreateButtonDisabled(AlertsDP dp, WebDriver driver) {
		try {
			Reporter.log("verify Create Button Disabled or not", true);
			Assert.assertTrue(!(getWebElement(dp.or, "createButton", driver)
					.isEnabled()));
			Reporter.log("Create Button Disabled", true);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**selects Advertiser For Competitor
	 * @param dp, driver, advertisertName
	 *
	 */
	public void selectAdvertiserForCompetitor(AlertsDP dp, WebDriver driver,
			String advertisertName) {
		try {
			Reporter.log("Selecting Advertiser", true);
			enterText(getWebElement(dp.or, "selectAdvertiserForSpecComp", driver),advertisertName);
			getWebElement(dp.or, "addButtonForCompetitor", driver).click();
			Thread.sleep(2000);
			Assert.assertTrue(driver.findElement(By.xpath("//p[text()='For Competitor(s)']/following-sibling::div/div[2]/span[text()='"+advertisertName+"']")).isDisplayed());
			Reporter.log("Selected Advertiser", true);
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**selects Competitor Set Dropdown
	 * @param dp, driver, competitor
	 *
	 */
	public void selectCompetitorSetDropdown(AlertsDP dp, WebDriver driver,
			String competitor) {
		try {
			Reporter.log("Selecting Competitor set Dropdown", true);
			
			Select compSelect = new Select(getWebElement(dp.or, "competitorSetDropdown", driver));
			compSelect.selectByVisibleText(competitor);
			scrollDown(driver);
			getWebElement(dp.or, "addButtonCompetitorSet", driver).click();
			Reporter.log("Selected Competitor set Dropdown", true);
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**Verifies and selects keywordGroup for Local product
	 * @param dp, driver, keywordGroup
	 *
	 */
	public void verifyFieldsunderSEMLocalSelectKeyword(AlertsDP dp, WebDriver driver,String keywordGroup) {
		try {
			Reporter.log("Verifying fields under SEM Local", true);
			waitForElementPresent(
					getWebElement(dp.or, "selectKeywordGroupLabel", driver),
					TIMEOUT, PULSE);
			Assert.assertTrue(getWebElement(dp.or, "selectKeywordGroupLabel", driver).isDisplayed());
			Assert.assertTrue(getWebElement(dp.or, "selectKeywordGroupButton", driver).isDisplayed());
			getWebElement(dp.or, "selectKeywordGroupButton", driver).click();
			Reporter.log("clicked", true);
			waitForElementPresent(
					getWebElement(dp.or, "selectKeywordGroupSearch", driver),
					TIMEOUT, PULSE);
			Assert.assertTrue(getWebElement(dp.or, "selectKeywordGroupSearch", driver).isDisplayed());
			Assert.assertTrue(getWebElement(dp.or, "cancelButton", driver).isDisplayed());
			Reporter.log("Fields under SEM Local are verified", true);
			negativeValidationOfSearchSEMLocal(dp, driver);
			enterText(getWebElement(dp.or, "selectKeywordGroupSearch", driver),keywordGroup);
			driver.findElement(By.xpath("//button[text()='" + keywordGroup + "']")).click();
			waitForElementPresent(
					getWebElement(dp.or, "selectKeywordCampaign", driver),
					TIMEOUT, PULSE);
			waitForElementPresent(
					getWebElement(dp.or, "spinnerIcon", driver),
					TIMEOUT, PULSE);
			Thread.sleep(1500);
			String expected = "Keyword Group : "+keywordGroup;
			Assert.assertEquals(getWebElement(dp.or, "selectKeywordCampaign", driver).getText(),expected);
			Assert.assertTrue(getWebElement(dp.or, "selectTargetLabel", driver).isDisplayed());
			Assert.assertTrue(getWebElement(dp.or, "selectTargetDropdown", driver).isEnabled());
			Reporter.log("Verified fields under SEM Local", true);
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**selects TargetSetting for Local product
	 * @param dp, driver, targetName
	 *
	 */
	public void setTargetSettingSEMLocal(AlertsDP dp, WebDriver driver,
			String targetName) {
		try {
			Reporter.log("Selecting Target Setting for SEM Local", true);
			getWebElement(dp.or, "selectTargetDropdown", driver).click();			
			driver.findElement(By.xpath("//option[text()='" + targetName + "']")).click();
			Assert.assertTrue(driver.findElement(By.xpath("//option[text()='" + targetName + "']")).isDisplayed());
			Reporter.log("Selected Target Setting", true);
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	
	/**selects Competing Advertisers for Performance Comparison alert
	 * @param dp, driver, advertisertName1, advertisertName2
	 *
	 */
	public void selectCompetingAdvertisers(AlertsDP dp, WebDriver driver,
			String advertisertName1, String advertisertName2) {
		try {
			Reporter.log("Selecting Competing Advertisers", true);
			enterText(getWebElement(dp.or, "firstAdvertiserInput", driver),advertisertName1);
			Thread.sleep(2000);
			getWebElement(dp.or, "firstAdvertiserListPerfComp", driver).findElement(
					By.xpath("//div[@role='option' and text()='"
							+ advertisertName1 + "']")).click();
			String selectedValue1 = getWebElement(dp.or, "firstAdvertiserTextPerfComp",
					driver).getText();
			Assert.assertEquals(selectedValue1, advertisertName1);
			
			enterText(getWebElement(dp.or, "secondAdvertiserInput", driver),advertisertName2);
			Thread.sleep(2000);
			getWebElement(dp.or, "secondAdvertiserListPerfComp", driver).findElement(
					By.xpath("//div[@role='option' and text()='"
							+ advertisertName2 + "']")).click();
			String selectedValue2 = getWebElement(dp.or, "secondAdvertiserTextPerfComp",
					driver).getText();
			Assert.assertEquals(selectedValue2, advertisertName2);
			Reporter.log("Selected Competing Advertisers", true);
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	/**selects Metrics for Performance Comparison alert
	 * @param dp, driver, metrics, andOr
	 *
	 */
	public void selectMetricforPerfComp(AlertsDP dp, WebDriver driver,String metrics,String andOr) {
		try {
			Reporter.log("Verify AND OR Reset rules and select metrics", true);
			Assert.assertTrue(getWebElement(dp.or, "addAndRule", driver).isDisplayed());
			Assert.assertTrue(getWebElement(dp.or, "addORRule", driver).isDisplayed());
			if(metrics.contains(",")){
			String[] metricsArray = metrics.split(",");
			int numOfMetrics =  metricsArray.length;
			for(int i=0;i<numOfMetrics;i++){
				if(i>0){
			if(andOr.equals("AND")){
				getWebElement(dp.or, "addAndRule", driver).click();
				System.out.println("and clicked");
				Assert.assertTrue(driver.findElement(By.xpath("//u[text()='"+andOr+"']/parent::button")).getAttribute("disabled").contains(""));
					}if(andOr.equals("OR")){
						getWebElement(dp.or, "addORRule", driver).click();
						Assert.assertTrue(driver.findElement(By.xpath("//u[text()='"+andOr+"']/parent::button")).getAttribute("disabled").contains(""));
					}
				}
			List<WebElement> metricsList = driver.findElements(By.xpath("//h3[text()='METRICS AND THRESHOLDS']/following-sibling::div//span[@class='Select-arrow-zone']"));
			metricsList.get(i).click();
			if ((metricsArray[i].equals("Clicks") || metricsArray[i].equals("Coverage") || metricsArray[i].equals("CPC")) && i==0){
				Actions selectAction = new Actions(driver);
				selectAction.sendKeys(Keys.ARROW_UP).sendKeys(Keys.ARROW_UP).sendKeys(Keys.ARROW_UP).sendKeys(Keys.ARROW_UP).build().perform();
				} else if ((metricsArray[i].equals("Impressions") || metricsArray[i].equals("Impressions Share") || metricsArray[i].equals("# of Keywords")) && i>0){
					Actions selectAction = new Actions(driver);
					selectAction.sendKeys(Keys.ARROW_UP).build().perform();
				}
			driver.findElement(By.xpath("//div[@role= 'option' and text()='"+metricsArray[i]+"']")).click();
			}
			}else{
				
				driver.findElement(By.xpath("//h3[text()='METRICS AND THRESHOLDS']/following-sibling::div//span[@class='Select-arrow-zone']")).click();
				if (metrics.equals("Clicks") || metrics.equals("Coverage") || metrics.equals("CPC")){
					Actions selectAction = new Actions(driver);
					selectAction.sendKeys(Keys.ARROW_UP).sendKeys(Keys.ARROW_UP).sendKeys(Keys.ARROW_UP).sendKeys(Keys.ARROW_UP).build().perform();
					}
				driver.findElement(By.xpath("//div[@role= 'option' and text()='"+metrics+"']")).click();
				Thread.sleep(500);
				Assert.assertTrue(driver.findElement(By.xpath("//h3[text()='METRICS AND THRESHOLDS']/following-sibling::div//div/span[text()='"+metrics+"']")).isDisplayed());
			}
			Reporter.log("Verified AND OR Reset rules and select metrics", true);
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**selects Advertiser for Bidding Position alert
	 * @param dp, driver, advertisertName
	 *
	 */
	public void selectAdvertiserBidding(AlertsDP dp, WebDriver driver,
			String advertisertName) {
		try {
			Reporter.log("Selecting Advertiser", true);
			enterText(getWebElement(dp.or, "selectAdvertiserBidding", driver),advertisertName);
			getWebElement(dp.or, "addButtonForCompetitor", driver).click();
			Thread.sleep(2000);
			Assert.assertTrue(driver.findElement(By.xpath("//p[text()='Select Advertiser(s)']/following-sibling::div/div[2]/span[text()='"+advertisertName+"']")).isDisplayed());
			Reporter.log("Selected Advertiser", true);
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**Verifies and selects Bidding position value for Bidding Position alert
	 * @param dp, driver, biddingValue
	 *
	 */
	public void verifyAndSelectBiddingPosition(AlertsDP dp, WebDriver driver,String biddingValue) {
		try {
			Reporter.log("Selecting Bidding position", true);
			double minforGreaterThan = 0.0;
			double maxforGreaterThan = 7.9;
			double minforLessThan = 0.1;
			double maxforLessThan = 8.0;
			List<WebElement> nav= getWebElement(dp.or, "biddingPositionValue", driver).findElements(By.xpath("./following-sibling::b"));
			WebElement navUp = nav.get(0);
			WebElement navDown = nav.get(1);
			Reporter.log(String.valueOf(nav.size()));
			Reporter.log("N1");
			
			Assert.assertTrue(getWebElement(dp.or, "biddingPositionRadiobtn", driver).isSelected());
			double currentVal = Double.valueOf(getWebElement(dp.or, "biddingPositionValue", driver).getAttribute("value"));
			
			int clickCount=Math.abs((int) ((minforGreaterThan-currentVal)/0.1));
			while (clickCount>0)
				{
					navDown.click();
					 --clickCount;
				}
			currentVal = Double.valueOf(getWebElement(dp.or, "biddingPositionValue", driver).getAttribute("value"));
			
			Reporter.log(String.valueOf(currentVal));
			
			Assert.assertEquals(currentVal, minforGreaterThan);
			navDown.click();
			currentVal = Double.valueOf(getWebElement(dp.or, "biddingPositionValue", driver).getAttribute("value"));
			Assert.assertEquals(currentVal, minforGreaterThan);
			
			clickCount=Math.abs((int) ((maxforGreaterThan-currentVal)/0.1));
			while (clickCount>0)
				{
					navUp.click();
					 --clickCount;
				}
			currentVal = Double.valueOf(getWebElement(dp.or, "biddingPositionValue", driver).getAttribute("value"));
			Assert.assertEquals(currentVal, maxforGreaterThan);
			navUp.click();
			currentVal = Double.valueOf(getWebElement(dp.or, "biddingPositionValue", driver).getAttribute("value"));
			Assert.assertEquals(currentVal, maxforGreaterThan);
			
			clickCount=Math.abs((int) ((Double.valueOf(biddingValue)-currentVal)/0.1));
			while (clickCount>0)
				{
					if (Double.valueOf(biddingValue)>currentVal)
					{navUp.click();
					 --clickCount;}
					else
					{navDown.click();
					 --clickCount;}	
				}
			currentVal = Double.valueOf(getWebElement(dp.or, "biddingPositionValue", driver).getAttribute("value"));
			Assert.assertEquals(currentVal, Double.valueOf(biddingValue));
			Reporter.log("Selected Bidding position", true);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	
	
	/**selects KeywordGroup or Brand
	 * @param dp, driver, typeName, keywordGroup
	 *
	 */
	public void selectKeywordGroupBrand(AlertsDP dp, WebDriver driver,
			String typeName,String keywordGroup) {
		try {
			Reporter.log("Selecting Keyword Group/Brand", true);
			String expected = typeName.substring(0,typeName.length()-1)+" : "+keywordGroup;
			getWebElement(dp.or, "selectKeywordCampaign", driver).click();
			Thread.sleep(1000);
			Assert.assertTrue(getWebElement(dp.or, "cancelButton", driver).isDisplayed());
			switch (typeName) {
			case "Keyword Groups":
				Assert.assertTrue(getWebElement(dp.or, "keywordButton", driver).getAttribute("class").contains("active"));
				Reporter.log("Keyword Groups selected - "+ typeName, true);
				negativeValidationOfSearch(dp, driver, typeName);
				enterText(getWebElement(dp.or, "searchKeywordCampaign", driver),keywordGroup.toLowerCase());
				Thread.sleep(500);
				driver.findElement(By.xpath("//button[text()='" + keywordGroup + "']")).click();
				Thread.sleep(500);
				// Add a similar switch case to build verification String =
				// Type+" :"+ keywordGroup
				Reporter.log("expected is"+expected, true);
				Assert.assertEquals(getWebElement(dp.or, "selectKeywordCampaign",driver).getText(),expected);
				break; // optional
			case "Campaigns":
				getWebElement(dp.or, "campaignsButton", driver).click();
				Assert.assertTrue(getWebElement(dp.or, "campaignsButton", driver).getAttribute("class").contains("active"));
				Reporter.log("campaigns Button selected - "+ typeName, true);
				negativeValidationOfSearch(dp, driver, typeName);
				enterText(getWebElement(dp.or, "searchCampaign", driver),keywordGroup.toLowerCase());
				Thread.sleep(500);
				driver.findElement(By.xpath("//button[text()='" + keywordGroup + "']")).click();
				Thread.sleep(500);
				// Add a similar switch case to build verification String =
				// Type+" :"+ keywordGroup
				Reporter.log("expected is"+expected, true);
				Assert.assertEquals(getWebElement(dp.or, "selectKeywordCampaign",driver).getText(),expected);
				break; // optional
			case "Brands":
				Reporter.log("In side Brands- "+ typeName, true);
				getWebElement(dp.or, "brandButton", driver).click();
				Assert.assertTrue(getWebElement(dp.or, "brandButton",driver).getAttribute("class").contains("active"));
				Reporter.log("Brands selected - "+ typeName, true);
				negativeValidationOfSearch(dp, driver, typeName);
				enterText(getWebElement(dp.or, "searchBrand", driver),keywordGroup.toLowerCase());
				Thread.sleep(500);
				driver.findElement(By.xpath("//button[text()='" + keywordGroup + "']")).click();
				Thread.sleep(500);
				// Add a similar switch case to build verification String =
				// Type+" :"+ keywordGroup
				Reporter.log("expected is"+expected, true);
				Assert.assertEquals(getWebElement(dp.or, "selectKeywordCampaign",driver).getText(),expected);
				break; // optional

			default:
				Reporter.log("Trademark Infringements type not correct", true);
			}
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**selects Date range
	 * @param dp, driver, dateRangeType, dateNumber
	 *
	 */
	public void selectDateRange(AlertsDP dp, WebDriver driver,
			String dateRangeType,String dateNumber) {
		try {
			Reporter.log("Selecting Date Range", true);
			if(dateRangeType.equals("Monthly")){
			getWebElement(dp.or, "dateRangeMonthly", driver).click();
			}
			if(dateRangeType.equals("Weekly")){
				getWebElement(dp.or, "dateRangeWeekly", driver).click();
				}
			if(dateRangeType.equals("Daily")){
				getWebElement(dp.or, "dateRangeDaily", driver).click();
				}
			Thread.sleep(500);
			Assert.assertTrue(getWebElement(dp.or, "chooseLastDaysRadio",
					driver).isDisplayed());
			Assert.assertTrue(getWebElement(dp.or, "saveButton",
					driver).isDisplayed());
			Assert.assertTrue(getWebElement(dp.or, "cancelButton",
					driver).isDisplayed());
			getWebElement(dp.or, "chooseLastDaysRadio", driver).click();
			selectByVisibleText(
					getWebElement(dp.or, "chooseLastDaysSelect", driver),
					dateNumber);
			getWebElement(dp.or, "saveButton", driver).click();
			Reporter.log("Selected Date Range", true);
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**selects Metrics based on AND or OR or None selection
	 * @param dp, driver, metrics, rule, thresholdInput, andOr
	 *
	 */
	public void VerifyMultipleMetrcisThresholdAndSelect(AlertsDP dp, WebDriver driver, String metrics,String rule,String thresholdInput,String andOr) {
		try {
			Reporter.log("selecting Metrics and Thresholds", true);
			if(metrics.contains(",")){
			String metricsArray[] = metrics.split(",");
			String ruleArray[] = rule.split(",");
			String thresholdArray[] = thresholdInput.split(",");
			int numOfMetrics =  metricsArray.length;
			scrollDown(driver);
			Assert.assertTrue(getWebElement(dp.or, "addAndRule", driver).isDisplayed());
			Assert.assertTrue(getWebElement(dp.or, "addORRule", driver).isDisplayed());
			for(int i=0;i<numOfMetrics;i++){
				if(i>0){
			if(andOr.equals("AND")){
				getWebElement(dp.or, "addAndRule", driver).click();
				System.out.println("and clicked");
				Assert.assertTrue(driver.findElement(By.xpath("//u[text()='"+andOr+"']/parent::button")).getAttribute("disabled").contains(""));
					}if(andOr.equals("OR")){
						getWebElement(dp.or, "addORRule", driver).click();
						Assert.assertTrue(driver.findElement(By.xpath("//u[text()='"+andOr+"']/parent::button")).getAttribute("disabled").contains(""));
					}
				}
			Thread.sleep(1000);
			List<WebElement> metricsList = driver.findElements(By.xpath("//div[contains(@class,'metricName')]//div[@role='combobox']/parent::span/following-sibling::span"));
			List<WebElement> ruleList = driver.findElements(By.xpath("//div[contains(@class,'operatorSelector')]//select"));
			List<WebElement> thresholdList = driver.findElements(By.xpath("//div[contains(@class,'operatorSelector')]//input"));
			metricsList.get(i).click();
			if ((metricsArray[i].equals("Clicks") || metricsArray[i].equals("Coverage")) && i==0){
				Actions selectAction = new Actions(driver);
				selectAction.sendKeys(Keys.ARROW_UP).sendKeys(Keys.ARROW_UP).sendKeys(Keys.ARROW_UP).sendKeys(Keys.ARROW_UP).build().perform();
				} else if ((metricsArray[i].equals("Impressions") || metricsArray[i].equals("Impressions Share") || metricsArray[i].equals("# of Keywords")) && i>0){
					Actions selectAction = new Actions(driver);
					selectAction.sendKeys(Keys.ARROW_UP).build().perform();
				}
			
			driver.findElement(By.xpath("//div[@role= 'option' and text()='"+metricsArray[i]+"']")).click();
			Thread.sleep(500);
			Assert.assertTrue(driver.findElements(By.xpath("//div[contains(@class,'metricName')]//div[@role='combobox']/parent::span")).get(i).getText().contains(metricsArray[i]));
			ruleList.get(i).click();
			driver.findElements(By.xpath("//select/option[text()='"+ruleArray[i]+"']")).get(i).click();
			enterText(thresholdList.get(i), thresholdArray[i]);
			Assert.assertTrue(thresholdList.get(i).getAttribute("value").contains(thresholdArray[i]));
			Assert.assertTrue(driver.findElement(By.xpath("//u[text()='"+andOr+"']/parent::button")).isEnabled());
			}
			}
			else{
				driver.findElement(By.xpath("//div[contains(@class,'metricName')]//div[@role='combobox']/parent::span/following-sibling::span")).click();
				WebElement targetSelection = driver.findElement(By.xpath("//div[@role= 'option' and text()='"+metrics+"']"));
				if (metrics.equals("Clicks") || metrics.equals("Coverage")){
				Actions selectAction = new Actions(driver);
				selectAction.sendKeys(Keys.ARROW_UP).sendKeys(Keys.ARROW_UP).sendKeys(Keys.ARROW_UP).sendKeys(Keys.ARROW_UP).build().perform();
				}
				//selectAction.sendKeys(Keys.ARROW_UP).build().perform();
				targetSelection.click();
				Thread.sleep(500);
				Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'metricName')]//div[@role='combobox']/parent::span")).getText().contains(metrics));
				driver.findElement(By.xpath("//div[contains(@class,'operatorSelector')]//input")).click();
				driver.findElement(By.xpath("//select/option[text()='"+rule+"']")).click();
				enterText(driver.findElement(By.xpath("//div[contains(@class,'operatorSelector')]//input")), thresholdInput);
				Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'operatorSelector')]//input")).getAttribute("value").contains(thresholdInput));
			}
			Reporter.log("selected Metrics and Thresholds", true);
			scrollDown(driver);
			}

		 catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**selects Alert Frequency
	 * @param dp, driver, alertFrequencyType, daysValue
	 *
	 */
	public void selectAlertFrequency(AlertsDP dp, WebDriver driver,String alertFrequencyType,String daysValue) {
		try {
			Reporter.log("Selecting Alert Frequency", true);
			System.out.println("in weekly "+alertFrequencyType);
			if(alertFrequencyType.equals("Daily")){
			getWebElement(dp.or, "alertFrequencyDaily", driver).click();
			Thread.sleep(500);
			Assert.assertTrue(getWebElement(dp.or,"dailyFrequencyAllDaysButton", driver).isDisplayed());
			getWebElement(dp.or, "dailyFrequencyAllDaysButton", driver).click();
			Thread.sleep(500);
			}
			if(alertFrequencyType.equals("SpecificDays")){
				String[] values = daysValue.split(",");
				getWebElement(dp.or, "alertFrequencyDaily", driver).click();
				Thread.sleep(500);
				for (int i = 0; i < values.length; i++) {
					driver.findElement(By.xpath("//input[@value='" + values[i] + "']")).click();
					Thread.sleep(500);
				}
			}
			if(alertFrequencyType.equals("Weekly")){
				System.out.println("in weekly "+alertFrequencyType);
				getWebElement(dp.or, "alertFrequencyWeekly", driver).click();
				Thread.sleep(500);
				driver.findElement(By.xpath("//span[text()='"+daysValue+"']/following-sibling::span")).click();
				Thread.sleep(500);
			}
			if(alertFrequencyType.equals("Monthly")){
				getWebElement(dp.or, "alertFrequencyMonthly", driver).click();
				Thread.sleep(500);
				Assert.assertTrue(getWebElement(dp.or,"firstDayofMonth", driver).isDisplayed());
				getWebElement(dp.or, "firstDayofMonth", driver).click();
				Thread.sleep(500);
			}
			Assert.assertTrue(getWebElement(dp.or,"saveButton", driver).isEnabled());
			Assert.assertTrue(getWebElement(dp.or,"cancelButton", driver).isDisplayed());
			getWebElement(dp.or, "saveButton", driver).click();
			Reporter.log("Selected Alert Frequency", true);
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**Add s multiple Recipients
	 * @param dp, driver, alertFrequencyType, daysValue
	 *
	 */
	public void addMultipleRecipients(AlertsDP dp, WebDriver driver,
			String emailID) {
		try {
			Reporter.log("Adding recepient email", true);
			String[] emailIDs = emailID.split(";");
			enterText(getWebElement(dp.or, "alertRecipient", driver), emailID);
			getWebElement(dp.or, "addRecipientButton", driver).click();
			Thread.sleep(500);
			for (int i = 0; i < emailIDs.length; i++) {
				Assert.assertTrue(driver.findElement(
						By.xpath("//span[contains(text(),'" + emailIDs[i]
								+ "')]")).isDisplayed());
			}
			Reporter.log("Added multiple recepient emails", true);
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**Verifies and selects/de-selects ReceiveOnlyCheckbox
	 * @param dp, driver, alertFrequencyType, daysValue
	 *
	 */
	public void verifyReceiveOnlyCheckbox(AlertsDP dp, WebDriver driver,String flag) {
		try {
			Reporter.log("verify Receive Only Checkbox", true);
			scrollDown(driver);
			scrollDown(driver);
			Actions a = new Actions(driver);
			WebElement we = getWebElement(dp.or, "receiveOnlyHelpicon", driver);
			a.moveToElement(we).build().perform();
			Assert.assertTrue(getWebElement(dp.or, "receiveOnlyCheckbox", driver).isDisplayed());
			if(flag.equals("uncheck")){
				getWebElement(dp.or, "receiveOnlyCheckbox", driver).click();
			}
			/*Assert.assertTrue(driver.findElement(By.xpath("//input[@type='checkbox']/parent::label/following-sibling::i[@aria-describedby='tooltip']"))
					.getText().contains("You are opting in"));*/
			Thread.sleep(500);
			Reporter.log("verified Receive Only Checkbox", true);
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**Verifies negative Validation Of Keyword Group Search in SEM Local
	 * @param dp, driver
	 *
	 */
	public void negativeValidationOfSearchSEMLocal(AlertsDP dp, WebDriver driver) {
		try {
			Reporter.log("Validating Search of Keyword Group for SEM local", true);
				Assert.assertTrue(getWebElement(dp.or, "selectKeywordGroupSearch",driver).isDisplayed());
				getWebElement(dp.or, "selectKeywordGroupSearch",driver).sendKeys("no result");
				Assert.assertFalse(getWebElement(dp.or, "noSearchResults",driver).getText().contains("no result"));
				clearText(getWebElement(dp.or, "selectKeywordGroupSearch", driver));
				Reporter.log("Negative Validation of Search Keyword Groups Verified - ", true);
				
		
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**Verifies negative Validation Of Keyword Group/Brand Search in SEM/PLA/TM
	 * @param dp, driver, typeName
	 *
	 */
	public void negativeValidationOfSearch(AlertsDP dp, WebDriver driver,
			String typeName) {
		try {
			Reporter.log("Validating Search of Keyword Group/Brand", true);
			switch (typeName) {
			case "Keyword Groups":
				Assert.assertTrue(getWebElement(dp.or, "searchKeywordCampaign",driver).isDisplayed());
				getWebElement(dp.or, "searchKeywordCampaign",driver).sendKeys("no result");
				Assert.assertFalse(getWebElement(dp.or, "noSearchResults",driver).getText().contains("no result"));
				clearText(getWebElement(dp.or, "searchKeywordCampaign", driver));
				Reporter.log("Negative Validation of Search Keyword Groups Verified - "+ typeName, true);
				break; // optional
			case "Brands":
				Assert.assertTrue(getWebElement(dp.or, "searchBrand",driver).isDisplayed());
				getWebElement(dp.or, "searchBrand",driver).sendKeys("no result");
				Assert.assertFalse(getWebElement(dp.or, "noSearchResults",driver).getText().contains("no result"));
				clearText(getWebElement(dp.or, "searchBrand", driver));
				Reporter.log("Negative Validation of Search Brands Verified - "+ typeName, true);
				break; // optional
			case "Campaigns":
				Assert.assertTrue(getWebElement(dp.or, "searchCampaign",driver).isDisplayed());
				getWebElement(dp.or, "searchCampaign",driver).sendKeys("no result");
				Assert.assertFalse(getWebElement(dp.or, "noSearchResults",driver).getText().contains("no result"));
				clearText(getWebElement(dp.or, "searchCampaign", driver));
				Reporter.log("Negative Validation of Search Brands Verified - "+ typeName, true);
				break; // optional

			default:
				Reporter.log("Search Keyword Groups not Verified", true);
			}
			Thread.sleep(1000);
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	/**selects CompetitorPerformance Type alert
	 * @param dp, driver, typeName
	 *
	 */
	public void selectCompetitorPerformanceType(AlertsDP dp, WebDriver driver,
			String typeName) {
		try {
			Reporter.log("Selecting Competitor Performance Type", true);
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or,
					"newCompetitors", driver))
					&& isElementDisplayed(getWebElement(dp.or,
							"specificCompetitors", driver))
					&& isElementDisplayed(getWebElement(dp.or, "performanceComparison",
							driver)));
			switch (typeName) {
			case "New Competitors":
				getWebElement(dp.or, "newCompetitors", driver).click();
				Assert.assertTrue(getWebElement(dp.or, "newCompetitors", driver)
						.getAttribute("class").contains("active"));
				Reporter.log("Competitor Performance type selected - "
						+ typeName, true);
				break; // optional
			case "Specific Competitor(s)":
				getWebElement(dp.or, "specificCompetitors", driver).click();
				Assert.assertTrue(getWebElement(dp.or, "specificCompetitors",
						driver).getAttribute("class").contains("active"));
				Reporter.log("Competitor Performance type selected - "
						+ typeName, true);
				break; // optional
			case "Performance Comparison":
				getWebElement(dp.or, "performanceComparison", driver).click();
				Assert.assertTrue(getWebElement(dp.or, "performanceComparison", driver)
						.getAttribute("class").contains("active"));
				Reporter.log("Competitor Performance type selected - "
						+ typeName, true);
				break;

			default:
				Reporter.log("Competitor Performance type not correct", true);
			}
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**selects TrademarkInfringements Type alert
	 * @param dp, driver, typeName
	 *
	 */
	public void selectTrademarkInfringementsType(AlertsDP dp, WebDriver driver,
			String typeName) {
		try {
			Reporter.log("Selecting Trademark Infringements Type", true);
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or,
					"newInfringers", driver))
					&& isElementDisplayed(getWebElement(dp.or,
							"biddingPosition", driver)));
			switch (typeName) {
			case "New Infringers":
				getWebElement(dp.or, "newInfringers", driver).click();
				Assert.assertTrue(getWebElement(dp.or, "newInfringers", driver)
						.getAttribute("class").contains("active"));
				Reporter.log("Trademark Infringements type selected - "
						+ typeName, true);
				break; // optional
			case "Bidding Position":
				getWebElement(dp.or, "biddingPosition", driver).click();
				Assert.assertTrue(getWebElement(dp.or, "biddingPosition",
						driver).getAttribute("class").contains("active"));
				Reporter.log("Trademark Infringements type selected - "
						+ typeName, true);
				break; // optional

			default:
				Reporter.log("Trademark Infringements type not correct", true);
			}
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	/**selects any type of alert given
	 * @param dp, driver, typeName
	 *
	 */
	public void selectAlertType(AlertsDP dp, WebDriver driver, String typeName) {
		try {
			Reporter.log("Selecting alert type", true);
			Assert.assertTrue(isElementDisplayed(getWebElement(dp.or,
					"myPerformance", driver))
					&& isElementDisplayed(getWebElement(dp.or,
							"competitorsPerformace", driver))
					&& isElementDisplayed(getWebElement(dp.or,
							"trademarkInfringements", driver)));
			switch (typeName) {
			case "My Performance":
				getWebElement(dp.or, "myPerformance", driver).click();
				Assert.assertTrue(getWebElement(dp.or, "myPerformance", driver)
						.getAttribute("class").contains("active"));
				Reporter.log("alert type selected - " + typeName, true);
				break; // optional
			case "Competitor(s) Performance":
				getWebElement(dp.or, "competitorsPerformace", driver).click();
				Assert.assertTrue(getWebElement(dp.or, "competitorsPerformace",
						driver).getAttribute("class").contains("active"));
				Reporter.log("alert type selected - " + typeName, true);
				break; // optional
			case "Trademark Infringements":
				getWebElement(dp.or, "trademarkInfringements", driver).click();
				Assert.assertTrue(getWebElement(dp.or,
						"trademarkInfringements", driver).getAttribute("class")
						.contains("active"));
				Reporter.log("alert type selected - " + typeName, true);
				break;

			default:
				Reporter.log("alert type not correct", true);
			}
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
   
}
