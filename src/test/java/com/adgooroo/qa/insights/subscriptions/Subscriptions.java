package com.adgooroo.qa.insights.subscriptions;

import static org.testng.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import org.testng.Reporter;

import com.innominds.itaf.frameworkengine.Constants;
import com.innominds.itaf.frameworkengine.PageActionUtils;

public class Subscriptions extends PageActionUtils{
	public static final int TIMEOUT = 300000;
	public static final int PULSE = 100;
	Logger logger = LogManager.getLogger(Subscriptions.class.getName());
	WebElement element = null;
	
	HashMap objectMap=null;
	SoftAssert softAssert = new SoftAssert();
	
	
	public void scrollDown(WebDriver driver){
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		}
	
	public void scrollUp(WebDriver driver){
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,-250)", "");
		}
	
	
	/**Selects the product type, sub type and verify the same in the summary*/
	public void selectVerifyProductType(SubscriptionsDP dp, WebDriver driver, String productType, String subType)
	{		
		try
		{
			WebElement targetElement=null;
			switch (productType.toUpperCase())
			{
			case "SEM":
				getWebElement(dp.or, "semBtn",driver).click();
				targetElement=getWebElement(dp.or, "semBtn",driver);
				break;
			
			case "LOCAL":
				getWebElement(dp.or, "localBtn",driver).click();
				targetElement=getWebElement(dp.or, "localBtn",driver);
				break;
				
			case "PLA":
				getWebElement(dp.or, "plaBtn",driver).click();
				targetElement=getWebElement(dp.or, "plaBtn",driver);
				break;
			
			case "TRADEMARK":
				getWebElement(dp.or, "trademarkBtn",driver).click();
				targetElement=getWebElement(dp.or, "trademarkBtn",driver);
				break;
			
			case "INDUSTRY":
				getWebElement(dp.or, "industryBtn",driver).click();
				targetElement=getWebElement(dp.or, "industryBtn",driver);
				break;
				
			default:     
				Reporter.log("product type not correct", true);
				
			}
			
			WebDriverWait waitSelected = new WebDriverWait(driver,10);
			waitSelected.until(ExpectedConditions.attributeContains(targetElement, "class", "active"));
			System.out.println(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][contains(text(),'Product:')]/parent::li")).getText().toLowerCase());
			System.out.println(productType.toLowerCase());
			Assert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][contains(text(),'Product:')]/parent::li")).getText().toLowerCase().contains(productType.toLowerCase()));
			
			if (productType.equalsIgnoreCase("INDUSTRY"))
			{
				switch (subType.toUpperCase())
				{
				case "SEO":
					getWebElement(dp.or, "seoBtn",driver).click();
					targetElement=getWebElement(dp.or, "seoBtn",driver);
					break;
				
				case "PPC":
					getWebElement(dp.or, "ppcBtn",driver).click();
					targetElement=getWebElement(dp.or, "ppcBtn",driver);
					break;
					
				default:     
					Reporter.log("sub type not correct", true);
					
				}
				
				waitSelected.until(ExpectedConditions.attributeContains(targetElement, "class", "active"));
				Assert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][contains(text(),'Product:')]/parent::li")).getText().toLowerCase().contains(subType.toLowerCase()));
				
			}
			
			scrollDown(driver);
	
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}
	
	/**Selects the product type and verify the same in the summary*/
	public void selectVerifyReportType(SubscriptionsDP dp, WebDriver driver, String reportType)
	{		
		try
		{
		  Select reportSelect = new Select(getWebElement(dp.or, "reportSelect",driver));
		  reportSelect.selectByVisibleText(reportType);
		  String selectedValue = reportSelect.getFirstSelectedOption().getText().trim();
		  Assert.assertEquals(selectedValue, reportType);
		  Assert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][contains(text(),'Report')]/parent::li")).getText().toLowerCase().contains(reportType.toLowerCase()));
				
		  scrollDown(driver);
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}		

	}
	
	
	
	/**This method selects keyword group or Campaign or Brand*/
	public void selectVerifyKwgCampaignBrand(SubscriptionsDP dp, WebDriver driver, String selectionType, String selection, String SEM_subtype)
	{		
		try
		{
			Reporter.log("Selecting Keyword Group/Campaign/Brand", true);
			if (selectionType.toLowerCase().equals("keyword group"))
				selectionType="keywordgroup";
			Assert.assertTrue(getWebElement(dp.or, "kwgCampaignBrandBtn",driver).isDisplayed());
			getWebElement(dp.or, "kwgCampaignBrandBtn",driver).click();
			WebDriverWait waitFor = new WebDriverWait(driver, 10);
			waitFor.until(ExpectedConditions.visibilityOf(getWebElement(dp.or, "kwgCampaignBrandHeader",driver)));
			Thread.sleep(3000);
			if (selectionType.toLowerCase().equals("keywordgroup") && (getWebElement(dp.or, "kwgCampaignBrandHeader",driver).getText().contains("Brand") || getWebElement(dp.or, "kwgCampaignBrandHeader",driver).getText().contains("Campaign"))){
				getWebElement(dp.or, "keywordGroupsBtn",driver).click();
				switch (SEM_subtype.toLowerCase()){
				
				case "desktop":
					getWebElement(dp.or, "desktopBtn",driver).click();
					break;
					
				case "mobile":
					getWebElement(dp.or, "mobileBtn",driver).click();
					break;
					
				default:     
					Reporter.log("sub type is not for SEM", true);
					
				}
				
				}else if (selectionType.toLowerCase().equals("brand")){
					getWebElement(dp.or, "brandsBtn",driver).click();}
				else if (selectionType.toLowerCase().equals("campaign")){
					getWebElement(dp.or, "campaignsBtn",driver).click();}
			
			String placeholderCheck = selectionType.toLowerCase();
			if (selectionType.toLowerCase().equals("keywordgroup"))
					placeholderCheck = "keyword group";
			
			Assert.assertTrue(getWebElement(dp.or, "kwgCampaignBrandInput",driver).getAttribute("placeholder").toLowerCase().contains(placeholderCheck));
			getWebElement(dp.or, "kwgCampaignBrandInput",driver).sendKeys(selection);
			driver.findElement(By.xpath("//div[contains(@class,'kwCampaignGroupList')]/button[normalize-space() = '"+selection+"']")).click();
			WebElement buttonAfterSelection = driver.findElement(By.xpath("//select[@placeholder='Select a report']/parent::div/parent::div/following-sibling::div[1]//button"));
			waitFor.until(ExpectedConditions.textToBePresentInElement(buttonAfterSelection, selection));
			//System.out.println(buttonAfterSelection.getText().toLowerCase());
			Assert.assertTrue(buttonAfterSelection.getText().toLowerCase().contains(placeholderCheck) && buttonAfterSelection.getText().contains(selection));
			
			
			List <WebElement> summaryPanel = driver.findElements(By.xpath("//div[contains(@class,'contentHeader')]/following-sibling::ul//li"));
			boolean tFlag = false;	
			for (int i=0; i<summaryPanel.size();i++)
			{
				String summaryListText=summaryPanel.get(i).getText();
				if (summaryListText.toLowerCase().contains(placeholderCheck) && summaryListText.contains(selection))
					tFlag=true;				
			}
			
			softAssert.assertTrue(tFlag,"This is to assert that fields are getting populated in the summary");
			
					
		} catch (Exception e) {
			
			Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
		}
		
	}	
		
	public void selectSubscriptionDateRange(SubscriptionsDP dp, WebDriver driver,
			String dateRangeType,String dateNumber) {
		try {
			Reporter.log("Selecting Date Range", true);
			if(dateRangeType.equals("Monthly")){
			getWebElement(dp.or, "subscriptiondateRangeMonthly", driver).click();
			}
			if(dateRangeType.equals("Weekly")){
				getWebElement(dp.or, "subscriptiondateRangeWeekly", driver).click();
				}
			if(dateRangeType.equals("Daily")){
				getWebElement(dp.or, "subscriptiondateRangeDaily", driver).click();
				}
			Thread.sleep(500);
			Assert.assertTrue(getWebElement(dp.or, "chooseLastDaysRadio",
					driver).isDisplayed());
			Assert.assertTrue(getWebElement(dp.or, "saveButton",
					driver).isDisplayed());
			Assert.assertTrue(getWebElement(dp.or, "cancelButton",
					driver).isDisplayed());
			getWebElement(dp.or, "chooseLastDaysRadio", driver).click();
			selectByVisibleText(
					getWebElement(dp.or, "chooseLastDaysSelect", driver),
					dateNumber);
			getWebElement(dp.or, "saveButton", driver).click();
			if(dateRangeType.equals("Daily")){
			Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'Last "+dateNumber+" days')]")).isDisplayed());
			}
			if(dateRangeType.equals("Weekly")){
				Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'Last "+dateNumber+" weeks')]")).isDisplayed());
				}
			if(dateRangeType.equals("Monthly")){
				Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'Last "+dateNumber+" months')]")).isDisplayed());
				}
			Reporter.log("Selected Date Range", true);
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	public void addMultipleRecipientsInSubscriptions(SubscriptionsDP dp, WebDriver driver,
			String emailID) {
		try {
			Reporter.log("Adding recepient email", true);
			scrollDown(driver);
			String[] emailIDs = emailID.split(";");
			enterText(getWebElement(dp.or, "alertRecipient", driver), emailID);
			getWebElement(dp.or, "addRecipientButton", driver).click();
			Thread.sleep(500);
			for (int i = 0; i < emailIDs.length; i++) {
				Assert.assertTrue(driver.findElement(
						By.xpath("//span[contains(text(),'" + emailIDs[i]
								+ "')]")).isDisplayed());
				Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li/div[contains(text(),'"+emailIDs[i]+"')]")).isDisplayed());
			}
			Reporter.log("Added multiple recepient emails", true);
			scrollDown(driver);

		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}
	
	public void selectEngineRegionForSubscription(SubscriptionsDP dp, WebDriver driver, String engineName, String regionName) {
		try {
			Reporter.log("Selecting Engine & Region", true);
			selectByVisibleText(getWebElement(dp.or, "engineSelect", driver),
					engineName);
			Thread.sleep(1000);
			selectByVisibleText(getWebElement(dp.or, "regionSelect", driver),
					regionName);
			Thread.sleep(1000);
			Reporter.log("Selecting Engine & Region", true);
			Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'"+engineName+"')]")).isDisplayed());
			//Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'"+regionName+"')]")).isDisplayed());
			scrollDown(driver);
			
		} catch (Exception e) {

			Assert.assertTrue(false,
					"Test case failed due to exception " + e.getMessage());
		}

	}

	public void selectSubscriptionFrequency(SubscriptionsDP dp, WebDriver driver,String alertFrequencyType,String daysValue) {
		try {
			Reporter.log("Selecting Alert Frequency", true);
			scrollDown(driver);
			System.out.println("in weekly "+alertFrequencyType);
			if(alertFrequencyType.equals("Daily")){
			getWebElement(dp.or, "subscriptionFrequencyDaily", driver).click();
			Thread.sleep(500);
			Assert.assertTrue(getWebElement(dp.or,"saveButton", driver).isDisplayed());
			Assert.assertTrue(getWebElement(dp.or,"cancelButton", driver).isDisplayed());
			Thread.sleep(500);
			Assert.assertTrue(getWebElement(dp.or,"dailyFrequencyAllDaysButton", driver).isDisplayed());
			getWebElement(dp.or, "dailyFrequencyAllDaysButton", driver).click();
			getWebElement(dp.or, "saveButton", driver).click();
			Thread.sleep(500);
			Assert.assertEquals(driver.findElements(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li/span[contains(text(),'day')]")).size(),7);
			}
			if(alertFrequencyType.equals("SpecificDays")){
				String[] values = daysValue.split(",");
				getWebElement(dp.or, "subscriptionFrequencyDaily", driver).click();
				Assert.assertTrue(getWebElement(dp.or,"saveButton", driver).isDisplayed());
				Assert.assertTrue(getWebElement(dp.or,"cancelButton", driver).isDisplayed());
				Thread.sleep(500);
				for (int i = 0; i < values.length; i++) {
					driver.findElement(By.xpath("//input[@value='" + values[i] + "']")).click();
					Thread.sleep(500);
				}
				getWebElement(dp.or, "saveButton", driver).click();
				Assert.assertEquals(driver.findElements(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li/span[contains(text(),'day')]")).size(),values.length);
			}
			if(alertFrequencyType.equals("Weekly")){
				System.out.println("in weekly "+alertFrequencyType);
				getWebElement(dp.or, "subscriptionFrequencyWeekly", driver).click();
				Assert.assertTrue(getWebElement(dp.or,"saveButton", driver).isDisplayed());
				Assert.assertTrue(getWebElement(dp.or,"cancelButton", driver).isDisplayed());
				Thread.sleep(500);
				driver.findElement(By.xpath("//span[text()='"+daysValue+"']/following-sibling::span")).click();
				Thread.sleep(500);
				getWebElement(dp.or, "saveButton", driver).click();
				Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'Every week on "+daysValue+"s')]")).isDisplayed());
			}
			if(alertFrequencyType.equals("Monthly")){
				getWebElement(dp.or, "subscriptionFrequencyMonthly", driver).click();
				Thread.sleep(500);
				Assert.assertTrue(getWebElement(dp.or,"saveButton", driver).isDisplayed());
				Assert.assertTrue(getWebElement(dp.or,"cancelButton", driver).isDisplayed());
				Assert.assertTrue(getWebElement(dp.or,"frequencySpecificDay", driver).isDisplayed());
				if(daysValue.equals("first")){
				getWebElement(dp.or, "firstDayofMonth", driver).click();
				Thread.sleep(500);
				getWebElement(dp.or, "saveButton", driver).click();
				Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'first') and contains(text(),'month')]")).isDisplayed());
				}
			if(daysValue.equals("last")){
				getWebElement(dp.or, "lastDayofMonth", driver).click();
				Thread.sleep(500);
				getWebElement(dp.or, "saveButton", driver).click();
				Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'last') and contains(text(),'month')]")).isDisplayed());
			}
			if(alertFrequencyType.equals("SpecificDate")){
				getWebElement(dp.or, "frequencySpecificDay", driver).click();
				driver.findElement(By.xpath("//div[@role='option' and text()='"+daysValue+"']")).click();
				Thread.sleep(500);
				getWebElement(dp.or, "saveButton", driver).click();
				Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'"+daysValue+"') and contains(text(),'month')]")).isDisplayed());
			}
			}
			
			Reporter.log("Selected Alert Frequency", true);
			
		} catch (Exception e) {

			Assert.assertTrue(false,"Test case failed due to exception " + e.getMessage());
		}

	}

	public String randomString(int length) {
		String chars = "abcdefghijklmnopqrstuvwxyz";
		  Random rand = new Random();
		  StringBuilder buf = new StringBuilder();
		  for (int i=0; i<length; i++) {
		    buf.append(chars.charAt(rand.nextInt(chars.length())));
		  }
		  return buf.toString();
		}
	
	public void createSubscriptionName(SubscriptionsDP dp, WebDriver driver, String subscriptionName, String optionalCheck)
	 {  
	  try
	  { 
	   getWebElement(dp.or, "addSubscription",driver).click();
	   if(optionalCheck.equals("yes")){
	   String myLongString=randomString(161);
	   String mySmallString=randomString(1);
	   Reporter.log("Creating subscription name", true);
	   waitForElementPresent(getWebElement(dp.or, "subscriptionName",driver),TIMEOUT, PULSE);
	   clearText(getWebElement(dp.or, "subscriptionName",driver));
	   WebElement elem = getWebElement(dp.or, "subscriptionName",driver);
	   elem.sendKeys(myLongString);
	   int realLength = elem.getAttribute("value").length();
	   System.out.println(realLength);
	   Assert.assertEquals(160, realLength);
	   elem.clear();
	   Thread.sleep(1000);
	   elem.sendKeys(mySmallString);
	   Assert.assertEquals(mySmallString.length(), elem.getText().length());
	   Thread.sleep(2000);
	   Assert.assertEquals(elem.getAttribute("value"), subscriptionName);
	   Reporter.log("Subscription name Entered", true);
	   Thread.sleep(2000);
	   Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'previewContainer')]//div[contains(text(),'"+subscriptionName+"')]")).isDisplayed());
	   }
	   else{
	   enterText(getWebElement(dp.or, "subscriptionName",driver),subscriptionName);
	   System.out.println("Entered name");
	   Thread.sleep(2000);
	   Assert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][contains(text(),'Subscription Name:')]/following-sibling::div[text()='"+subscriptionName+"']")).isDisplayed());
	   System.out.println("execution sucess");
	   }
	   scrollDown(driver);
	  } catch (Exception e) {
	   
	   Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
	  }
	  }
	 public void addDescription(SubscriptionsDP dp, WebDriver driver, String Message,String optionalCheck)
	 {
	  try
	  {  
	   Reporter.log("Adding Subscription Description", true);
	   scrollDown(driver);
	   getWebElement(dp.or, "addRemoveMessage",driver).click();
	   Thread.sleep(500);
	   if(optionalCheck.equals("yes")){
	    WebElement elem=getWebElement(dp.or, "optionalMessage",driver);
	    elem.sendKeys(Message=randomString(1000));
	   Thread.sleep(2000);
	   int realLength = elem.getAttribute("value").length();
	   System.out.println(realLength);
	   Assert.assertEquals(1000, realLength);
	   System.out.println("maximum length veerified");
	   Assert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][contains(text(),'Description:')]/following-sibling::div[text()='"+Message+"']")).isDisplayed());
	   elem.clear();
	   Thread.sleep(1000);
	   elem.sendKeys(Message=randomString(1));
	   Assert.assertEquals(1, elem.getText().length());
	   Thread.sleep(2000);
	   Assert.assertEquals(elem.getAttribute("value"), Message);
	   Assert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][contains(text(),'Description:')]/following-sibling::div[text()='"+Message+"']")).isDisplayed());
	   System.out.println("minimum length veerified");
	   }
	   else{
	    enterText(getWebElement(dp.or, "optionalMessage",driver),Message);
	    Assert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][contains(text(),'Description:')]/following-sibling::div[text()='"+Message+"']")).isDisplayed());
	                 System.out.println("Description field verified");
	   }
	   getWebElement(dp.or, "hideDescription",driver).click();
	   Reporter.log("Added message", true);
	   scrollDown(driver);
	      
	  } catch (Exception e) {
	   
	   Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
	  }  

	 }
	 

	 public void verifyCreateButtonDisabled(SubscriptionsDP dp, WebDriver driver) {
			try {
				Reporter.log("verify Create Button Disabled or not", true);
				Assert.assertTrue(!(getWebElement(dp.or, "createButton", driver)
						.isEnabled()));
				Reporter.log("Create Button Disabled", true);

			} catch (Exception e) {

				Assert.assertTrue(false,
						"Test case failed due to exception " + e.getMessage());
			}

		}

		public void clickOnCreate(SubscriptionsDP dp, WebDriver driver, String subscriptionName) {
			try {
				Reporter.log("Clicking On Create", true);
				Assert.assertTrue(getWebElement(dp.or, "createButton", driver)
						.isEnabled());
				getWebElement(dp.or, "createButton", driver).click();
				waitForElementPresent(
						getWebElement(dp.or, "toastNotification", driver),
						TIMEOUT, PULSE);
				String successText = verifyGetToastMessage(dp, driver);
				Assert.assertEquals(successText.trim(), "Successfully created "+subscriptionName+"!", "Validation for subscription created success message");
				Reporter.log("Clicked On Create", true);

			} catch (Exception e) {

				Assert.assertTrue(false,
						"Test case failed due to exception " + e.getMessage());
			}

		}

		/**Navigates to Subscription manager screen*/
		public void navigateToSubscriptionManager(SubscriptionsDP dp, WebDriver driver)
		{		
			try
			{
			
				Reporter.log("........Insights Alert Manager........", true);
				getWebElement(dp.or, "subscriptionManager",driver).click();
				waitForElementPresent(getWebElement(dp.or, "addSubscription",driver),TIMEOUT, PULSE);
				String headerVal=getWebElement(dp.or, "header",driver).getText();
				Reporter.log(headerVal, true);
				System.out.println(headerVal);
				Thread.sleep(2000);
							
			} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}		

		}
		
		public boolean searchSubscription(SubscriptionsDP dp, WebDriver driver, String searchString)
		{	
			Boolean found = false;	
			try
			{
				
				Reporter.log("........Insights Subscription Manager Search........", true);
				Assert.assertTrue(getWebElement(dp.or, "subscriptionFilterButton",driver).isDisplayed());
				getWebElement(dp.or, "subscriptionFilterButton",driver).click();
				WebElement subscriptionFilterName = driver.findElements(By.xpath("//div[contains(@class,'HeaderCell')]//input")).get(7);
				clearText(subscriptionFilterName);
				enterText(subscriptionFilterName,searchString);
				Thread.sleep(2000);
				String searchResults=driver.findElement(By.xpath("//div[@class='react-grid-Canvas']//div[contains(@class,'Row')][1]/div[contains(@class,'Cell')][9]")).getText();
				if (searchResults.contains(searchString)){
					found=true;
				Reporter.log(searchString+" is found", true);}
				System.out.println(found);
				
							
			} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}
			
			return found;

		}
			
		public void setTargetSetting(SubscriptionsDP dp, WebDriver driver, String targetName)
		{		
			try
			{		
				Reporter.log("Selecting Target Setting", true);
				enterText(getWebElement(dp.or, "targetSettingInput",driver),targetName);
				Thread.sleep(1000);
				driver.findElement(By.xpath("//strong[text()='"+targetName+"']")).click();
				//String selectedValue = getWebElement(dp.or, "targetSettingInput",driver).getText();
				//Assert.assertEquals(selectedValue,advertisertName);
				softAssert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li/div[contains(text(),'"+targetName+"')]")).isDisplayed());
				Reporter.log("Selected Target Setting", true);
				scrollDown(driver);
							
			} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}		

		}
		
		public void setLocalTargetSetting(SubscriptionsDP dp, WebDriver driver, String targetName)
		{		
			try
			{		
				Reporter.log("Selecting Target Setting", true);
				enterText(getWebElement(dp.or, "localTargetSettingInput",driver),targetName);
				Thread.sleep(1000);
				driver.findElement(By.xpath("//strong[text()='"+targetName+"']")).click();
				//String selectedValue = getWebElement(dp.or, "targetSettingInput",driver).getText();
				//Assert.assertEquals(selectedValue,advertisertName);
				softAssert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li/div[contains(text(),'"+targetName+"')]")).isDisplayed());
				Reporter.log("Selected Target Setting", true);
				scrollDown(driver);
							
			} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}		

		}
		
		public void selectAdvertiser(SubscriptionsDP dp, WebDriver driver, String advertisertName)
		{		
			try
			{		
				Reporter.log("Selecting Advertiser", true);
				//getWebElement(dp.or, "selectAdvertiserClearButton",driver).click();
				scrollDown(driver);
				enterText(getWebElement(dp.or, "selectAdvertiserInput",driver),advertisertName);
				Thread.sleep(2000);
				getWebElement(dp.or, "selectAdvertiserList",driver).findElement(By.xpath("//div[@role='option' and text()='"+advertisertName+"']")).click();
				String selectedValue = getWebElement(dp.or, "selectAdvertiserText",driver).getText();
				System.out.println("selected value"+selectedValue);
				Assert.assertEquals(selectedValue,advertisertName);
				softAssert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'"+advertisertName+"')]")).isDisplayed());
				Reporter.log("Selected Advertiser", true);
				scrollDown(driver);
							
			} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}		

		}
		
		public void selectInfringementType(SubscriptionsDP dp, WebDriver driver, String infringementType,String BugID)
		{		
			try
			{		
				Reporter.log("Selecting InfringementType", true);
				driver.findElement(By.xpath("//button[text()='"+infringementType+"']")).click();
	} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}
			try{	
				softAssert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'"+infringementType.toLowerCase()+"')]")).isDisplayed());
			} catch(NoSuchElementException e)
			{
				if (driver.findElements(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'"+infringementType.toLowerCase()+"')]")).size() != 1 )
					Reporter.getCurrentTestResult().setAttribute("warn", "Bug ID: "+BugID+"");
				
			}
				Reporter.log("Selected InfringementType", true);
				scrollDown(driver);
							
					

		}
		
		public void selectRollupBreakout(SubscriptionsDP dp, WebDriver driver, String rollupBreakout, String BugID)
		{		
			try
			{		
				Reporter.log("Selecting RollupBreakout", true);
				driver.findElement(By.xpath("//button[text()='"+rollupBreakout+"']")).click();
} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}
			try{
				softAssert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'"+rollupBreakout+"')]")).isDisplayed());
			} catch(NoSuchElementException e)
			{
				if (driver.findElements(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'"+rollupBreakout+"')]")).size() != 1 )
					Reporter.getCurrentTestResult().setAttribute("warn", "Bug ID: "+BugID+"");
				
			}
				Reporter.log("Selected RollupBreakout", true);
				scrollDown(driver);
							
			}	

		
			
		public void setTargetSettingSEMLocal(SubscriptionsDP dp, WebDriver driver,
				String targetName) {
			try {
				Reporter.log("Selecting Target Setting for SEM Local", true);
				//wait.until(ExpectedConditions.visibilityOf(getWebElement(dp.or, "spinnerIcon", driver)));
				//wait.until(ExpectedConditions.invisibilityOf(getWebElement(dp.or, "spinnerIcon", driver)));
				/*waitForElementPresent(
						getWebElement(dp.or, "spinnerIcon", driver),
						TIMEOUT, PULSE);*/
				getWebElement(dp.or, "selectTargetDropdown", driver).click();			
				driver.findElement(By.xpath("//option[text()='" + targetName + "']")).click();
				Assert.assertTrue(driver.findElement(By.xpath("//option[text()='" + targetName + "']")).isDisplayed());
				softAssert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'"+targetName+"')]")).isDisplayed());
				Reporter.log("Selected Target Setting", true);
				scrollDown(driver);

			} catch (Exception e) {

				Assert.assertTrue(false,
						"Test case failed due to exception " + e.getMessage());
			}

		}

		
		public void selectAKeyword(SubscriptionsDP dp, WebDriver driver, String keyword,String browser) {
			try
			{		
				Reporter.log("Selecting Keyword", true);
				//getWebElement(dp.or, "selectAdvertiserClearButton",driver).click();
				if(browser.toLowerCase().equals("firefox")){
					scrollDown(driver);
				}
				getWebElement(dp.or, "selectKeywordInput",driver).click();
				Thread.sleep(2000);
				enterText(getWebElement(dp.or, "selectKeywordInput",driver),keyword);
				Thread.sleep(2000);
				getWebElement(dp.or, "selectKeywordList",driver).findElement(By.xpath("//div[@role='option' and text()='"+keyword+"']")).click();
				String selectedValue = getWebElement(dp.or, "selectKeywordText",driver).getText().trim();
				Assert.assertEquals(selectedValue,keyword);
				Reporter.log("Selected Keyword", true);
				
				softAssert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][text()='Keyword:']/parent::li[text()='"+keyword+"']")).isDisplayed());
				scrollDown(driver);
							
			} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}

		}
		
		
		/* This method selects two advertiser for a comparison report*/
		public void selectComparisonAdvertisers(SubscriptionsDP dp, WebDriver driver, String Advertiser1, String Advertiser2,String BugID,String browser) {
			try
			{		
				Reporter.log("Selecting first Advertiser", true);
				if(browser.toLowerCase().equals("firefox")){
					scrollDown(driver);
				}
				driver.findElement(By.xpath("//label[@for='root_advertiser']/following-sibling::div//input")).sendKeys(Advertiser1);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//label[@for='root_advertiser']/following-sibling::div//div[@role='listbox']//div[@role='option' and text()='"+Advertiser1+"']")).click();
				String selectedValue = driver.findElement(By.xpath("//label[@for='root_advertiser']/following-sibling::div//child::span[@class='Select-value-label']")).getText().trim();
				Assert.assertEquals(selectedValue,Advertiser1);
				Reporter.log("Selected first Advertiser", true);
			} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}
				
			try {
				softAssert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][text()='Advertiser 1:']/parent::li[text()='"+Advertiser1+"']")).isDisplayed());
			} catch (NoSuchElementException ex){	
					
					if (driver.findElements(By.xpath("//span[contains(@class,'previewLabel')][text()='Advertiser 1:']/parent::li[text()='"+Advertiser1+"']")).size() != 1 )
						Reporter.getCurrentTestResult().setAttribute("warn", "Bug ID: "+BugID+"");
					
			}
				
			try {		
					
				scrollDown(driver);
				scrollDown(driver);
				Reporter.log("Selecting second Advertiser", true);
				driver.findElement(By.xpath("//label[@for='root_advertiser2']/following-sibling::div//input")).sendKeys(Advertiser2);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//label[@for='root_advertiser2']/following-sibling::div//div[@role='listbox']//div[@role='option' and text()='"+Advertiser2+"']")).click();
				String selectedValue2 = driver.findElement(By.xpath("//label[@for='root_advertiser2']/following-sibling::div//child::span[@class='Select-value-label']")).getText().trim();
				Assert.assertEquals(selectedValue2,Advertiser2);
				Reporter.log("Selected second Advertiser", true);
			} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}
				
			try{	
				softAssert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][text()='Advertiser 2:']/parent::li[text()='"+Advertiser2+"']")).isDisplayed());
			} catch (NoSuchElementException ex){	
				
				if (driver.findElements(By.xpath("//span[contains(@class,'previewLabel')][text()='Advertiser 2:']/parent::li[text()='"+Advertiser2+"']")).size() != 1 )
					Reporter.getCurrentTestResult().setAttribute("warn", "Bug ID: KMA-671");
				
			}
				scrollUp(driver);
				
			

		}
		
		/**Selects the competitor set and verify the same in the summary*/
		public void selectVerifyCompetitorSet(SubscriptionsDP dp, WebDriver driver, String competitor)
		{		
			try
			{
			  Select competitorSelect = new Select(getWebElement(dp.or, "competitorSetSelect",driver));
			  competitorSelect.selectByVisibleText(competitor);
			  String selectedValue = competitorSelect.getFirstSelectedOption().getText().trim();
			  Assert.assertEquals(selectedValue, competitor);
			  softAssert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][text()='Competitor:']/parent::li[text()='"+competitor+"']")).isDisplayed());
			  scrollDown(driver);
			} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}		

		}

		/**Selects the Taxonomy and verify the same in the summary*/
		public void selectVerifyTaxonomy(SubscriptionsDP dp, WebDriver driver, String taxonomyType, String taxonomySubType)
		{		
			try
			{
				Reporter.log("Selecting Taxonomy Tree", true);
				Assert.assertTrue(getWebElement(dp.or, "taxonomyTreeBtn",driver).isDisplayed());
				getWebElement(dp.or, "taxonomyTreeBtn",driver).click();
				WebDriverWait waitFor = new WebDriverWait(driver, 10);
				waitFor.until(ExpectedConditions.visibilityOf(getWebElement(dp.or, "taxonomyPopupHeader",driver)));
				Thread.sleep(3000);
				String selectedVal = "";
				if (taxonomyType.toLowerCase().equals("all"))
				{
					driver.findElement(By.xpath("//span[@title='All']")).click();
					selectedVal = "All";
				}
				else{
					
					driver.findElement(By.xpath("//span[@title='All']/preceding-sibling::span[contains(@class,'tree-switcher')]")).click();
					Thread.sleep(500);
					String titledCaseTaxonomyType = Arrays.stream(taxonomyType.toLowerCase().split(" ")).map(word -> Character.toTitleCase(word.charAt(0)) + word.substring(1)).collect(Collectors.joining(" "));
					if (taxonomySubType.isEmpty()){
						driver.findElement(By.xpath("//span[@title='"+titledCaseTaxonomyType+"']")).click();
						selectedVal = titledCaseTaxonomyType;
					}
					else{
							driver.findElement(By.xpath("//span[@title='"+titledCaseTaxonomyType+"']/preceding-sibling::span[contains(@class,'tree-switcher')]")).click();
							Thread.sleep(500);
							String titledCaseTaxonomySubType = Arrays.stream(taxonomySubType.toLowerCase().split(" ")).map(word -> Character.toTitleCase(word.charAt(0)) + word.substring(1)).collect(Collectors.joining(" "));
							driver.findElement(By.xpath("//span[@title='"+titledCaseTaxonomySubType+"']")).click();
							selectedVal = titledCaseTaxonomySubType;
							}
						
					}
					
				Assert.assertTrue(getWebElement(dp.or, "taxonomyTreeBtn",driver).getText().split("Category: ")[1].equals(selectedVal));
				softAssert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][text()='Category:']/parent::li[text()='"+selectedVal+"']")).isDisplayed());
				scrollDown(driver);
			
			
			} catch (Exception e) {
					
					Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
				}		

			}
		
		/**enters the urls to exclude and verify the same in the summary*/
		public void enterVerifyUrlExclusion(SubscriptionsDP dp, WebDriver driver, String uRLS, String testFieldOptional)
		{		
			try
			{
				Reporter.log("Selecting url texts", true);
				Assert.assertTrue(getWebElement(dp.or, "urlExclusionsInput",driver).isDisplayed());
				if (uRLS.contains(",")){
					String[] multiUrls = uRLS.split(",");
					for (int i = 0; i<multiUrls.length; i++)
					{
						getWebElement(dp.or, "urlExclusionsInput",driver).sendKeys(multiUrls[i]);
						getWebElement(dp.or, "urlExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[contains(@class,'col')]/following-sibling::div/button[text()='Add']")).click();
						String messageToast = verifyGetToastMessage(dp, driver);
						Assert.assertEquals(messageToast, "exclusion added successfully");
						Assert.assertTrue(getWebElement(dp.or, "urlExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[@class='row']/parent::div//span[contains(@class,'email')][text()='"+multiUrls[i].toLowerCase()+"']")).isDisplayed());
						new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOf(getWebElement(dp.or, "toastNotification",driver)));
					}
					//Summary verification
					for (int i = 0; i<multiUrls.length; i++)
						softAssert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][text()='Url Exclusions:']/parent::li/div[contains(@class,'emailRecord')][text()='"+multiUrls[i].toLowerCase()+"']")).isDisplayed());
										
				}else{
					getWebElement(dp.or, "urlExclusionsInput",driver).sendKeys(uRLS);
					getWebElement(dp.or, "urlExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[contains(@class,'col')]/following-sibling::div/button[text()='Add']")).click();
					String messageToast = verifyGetToastMessage(dp, driver);
					Assert.assertEquals(messageToast, "exclusion added successfully");
					Assert.assertTrue(getWebElement(dp.or, "urlExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[@class='row']/parent::div//span[contains(@class,'email')][text()='"+uRLS.toLowerCase()+"']")).isDisplayed());
					//summary verification
					softAssert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][text()='Url Exclusions:']/parent::li/div[contains(@class,'emailRecord')][text()='"+uRLS.toLowerCase()+"']")).isDisplayed());
				}
				
				if (testFieldOptional.toLowerCase().equals("yes")){
					String duplicateUrl = getWebElement(dp.or, "urlExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[@class='row']/parent::div//div[contains(@class,'email')][1]/span[contains(@class,'email')]")).getText();
					//testing for duplicate
					getWebElement(dp.or, "urlExclusionsInput",driver).sendKeys(duplicateUrl);
					getWebElement(dp.or, "urlExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[contains(@class,'col')]/following-sibling::div/button[text()='Add']")).click();
					int duplicateCount = 0;
					for(int i=0;i<9;++i){
						List<WebElement> duplicateList = getWebElement(dp.or, "urlExclusionsInput",driver).findElements(By.xpath(".//ancestor::div[@class='row']/parent::div//span[contains(@class,'email')][text()='"+duplicateUrl.toLowerCase()+"']"));
						if(duplicateList.size() > 1) {
							duplicateCount++;
						}
						Thread.sleep(333);
					}
					
					Assert.assertEquals(duplicateCount, 0, "This is to verify that the count for duplicate has not increased");
					//summary verification whether the duplicate is not added
					softAssert.assertEquals(driver.findElements(By.xpath("//span[contains(@class,'previewLabel')][text()='Url Exclusions:']/parent::li/div[contains(@class,'emailRecord')][text()='"+duplicateUrl.toLowerCase()+"']")).size(),1);
					//testing for remove
					getWebElement(dp.or, "urlExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[@class='row']/parent::div//span[contains(@class,'email')][text()='"+duplicateUrl.toLowerCase()+"']/following-sibling::span")).click();
					String messageToast = verifyGetToastMessage(dp, driver);
					Assert.assertEquals(messageToast, "exclusion removed successfully");
					List<WebElement> deletedUrl = getWebElement(dp.or, "urlExclusionsInput",driver).findElements(By.xpath(".//ancestor::div[@class='row']/parent::div//span[contains(@class,'email')][text()='"+duplicateUrl.toLowerCase()+"']"));
					Assert.assertTrue(deletedUrl.isEmpty(), "This is to verify that the count for deleted url is 0");
					//summary verification for delete
					softAssert.assertTrue(driver.findElements(By.xpath("//span[contains(@class,'previewLabel')][text()='Url Exclusions:']/parent::li/div[contains(@class,'emailRecord')][text()='"+duplicateUrl.toLowerCase()+"']")).isEmpty());
					//adding the url again
					getWebElement(dp.or, "urlExclusionsInput",driver).clear();
					getWebElement(dp.or, "urlExclusionsInput",driver).sendKeys(duplicateUrl);
					getWebElement(dp.or, "urlExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[contains(@class,'col')]/following-sibling::div/button[text()='Add']")).click();
					messageToast = verifyGetToastMessage(dp, driver);
					Assert.assertEquals(messageToast, "exclusion added successfully");
					Assert.assertTrue(getWebElement(dp.or, "urlExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[@class='row']/parent::div//span[contains(@class,'email')][text()='"+duplicateUrl.toLowerCase()+"']")).isDisplayed());
					//summary verification for re-adding
					softAssert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][text()='Url Exclusions:']/parent::li/div[contains(@class,'emailRecord')][text()='"+duplicateUrl.toLowerCase()+"']")).isDisplayed());
				}
				
				
				scrollDown(driver);
			} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}		

		}
		
		
		
		/**enters advertisers to exclude and verify the same in the summary. It would fetch multiple if separated by comma*/
		public void enterVerifyAdvertiserExclusion(SubscriptionsDP dp, WebDriver driver, String advertisers, String testFieldOptional,String browser)
		{		
			try
			{
				Reporter.log("Selecting url texts", true);
				scrollDown(driver);
				Assert.assertTrue(getWebElement(dp.or, "advertiserExclusionsInput",driver).isDisplayed());
				if (advertisers.contains(",")){
					String[] multiAdvertisers = advertisers.split(",");
					for (int i = 0; i<multiAdvertisers.length; i++)
					{
						getWebElement(dp.or, "advertiserExclusionsInput",driver).sendKeys(multiAdvertisers[i]);
						if(browser.toLowerCase().equals("firefox")){
							scrollDown(driver);
						}
						getWebElement(dp.or, "advertiserExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[contains(@class,'col')]/following-sibling::div/button[text()='Add']")).click();
						String messageToast = verifyGetToastMessage(dp, driver);
						Assert.assertEquals(messageToast, "exclusion added successfully");
						Assert.assertTrue(getWebElement(dp.or, "advertiserExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[@class='row']/parent::div//span[contains(@class,'email')][text()='"+multiAdvertisers[i].toLowerCase()+"']")).isDisplayed());
						new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOf(getWebElement(dp.or, "toastNotification",driver)));
					}
					//verifying the summary
					for (int i = 0; i<multiAdvertisers.length; i++)
						softAssert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][text()='Ad Exclusions:']/parent::li/div[contains(@class,'emailRecord')][text()='"+multiAdvertisers[i].toLowerCase()+"']")).isDisplayed());
										
				}else{
					getWebElement(dp.or, "advertiserExclusionsInput",driver).sendKeys(advertisers);
					if(browser.toLowerCase().equals("firefox")){
						scrollDown(driver);
						scrollDown(driver);
					}
					getWebElement(dp.or, "advertiserExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[contains(@class,'col')]/following-sibling::div/button[text()='Add']")).click();
					Thread.sleep(2000);
					String messageToast = verifyGetToastMessage(dp, driver);
					Assert.assertEquals(messageToast, "exclusion added successfully");
					Assert.assertTrue(getWebElement(dp.or, "advertiserExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[contains(@class,'col')]/following-sibling::div//span[contains(@class,'email')][text()='"+advertisers.toLowerCase()+"']")).isDisplayed());
					//summary verification
					softAssert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][text()='Ad Exclusions:']/parent::li/div[contains(@class,'emailRecord')][text()='"+advertisers.toLowerCase()+"']")).isDisplayed());
				}
				
				if (testFieldOptional.toLowerCase().equals("yes")){
					String duplicateAdvertiser = getWebElement(dp.or, "advertiserExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[@class='row']/parent::div//div[contains(@class,'email')][1]/span[contains(@class,'email')]")).getText();
					//testing for duplicate
					getWebElement(dp.or, "advertiserExclusionsInput",driver).sendKeys(duplicateAdvertiser);
					getWebElement(dp.or, "advertiserExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[contains(@class,'col')]/following-sibling::div/button[text()='Add']")).click();
					int duplicateCount = 0;
					for(int i=0;i<9;++i){
						List<WebElement> duplicateList = getWebElement(dp.or, "advertiserExclusionsInput",driver).findElements(By.xpath(".//ancestor::div[@class='row']/parent::div//span[contains(@class,'email')][text()='"+duplicateAdvertiser.toLowerCase()+"']"));
						if(duplicateList.size() > 1) {
							duplicateCount++;
						}
						Thread.sleep(333);
					}
					
					Assert.assertEquals(duplicateCount, 0, "This is to verify that the count for duplicate has not increased");
					//summary verification whether the duplicate is not added
					softAssert.assertEquals(driver.findElements(By.xpath("//span[contains(@class,'previewLabel')][text()='Ad Exclusions:']/parent::li/div[contains(@class,'emailRecord')][text()='"+duplicateAdvertiser.toLowerCase()+"']")).size(),1);
					//testing for remove
					getWebElement(dp.or, "advertiserExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[@class='row']/parent::div//span[contains(@class,'email')][text()='"+duplicateAdvertiser.toLowerCase()+"']/following-sibling::span")).click();
					String messageToast = verifyGetToastMessage(dp, driver);
					Assert.assertEquals(messageToast, "exclusion removed successfully");
					List<WebElement> deletedAdvertiser = getWebElement(dp.or, "advertiserExclusionsInput",driver).findElements(By.xpath(".//ancestor::div[@class='row']/parent::div//span[contains(@class,'email')][text()='"+duplicateAdvertiser.toLowerCase()+"']"));
					Assert.assertTrue(deletedAdvertiser.isEmpty(), "This is to verify that the count for deleted advertiser is 0");
					//Summary verification for delete
					softAssert.assertTrue(driver.findElements(By.xpath("//span[contains(@class,'previewLabel')][text()='Ad Exclusions:']/parent::li/div[contains(@class,'emailRecord')][text()='"+duplicateAdvertiser.toLowerCase()+"']")).isEmpty());
					//adding the url again
					getWebElement(dp.or, "advertiserExclusionsInput",driver).clear();
					getWebElement(dp.or, "advertiserExclusionsInput",driver).sendKeys(duplicateAdvertiser);
					getWebElement(dp.or, "advertiserExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[contains(@class,'col')]/following-sibling::div/button[text()='Add']")).click();
					messageToast = verifyGetToastMessage(dp, driver);
					Assert.assertEquals(messageToast, "exclusion added successfully");
					Assert.assertTrue(getWebElement(dp.or, "advertiserExclusionsInput",driver).findElement(By.xpath(".//ancestor::div[@class='row']/parent::div//span[contains(@class,'email')][text()='"+duplicateAdvertiser.toLowerCase()+"']")).isDisplayed());
					//Summary verification for re-adding
					softAssert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][text()='Ad Exclusions:']/parent::li/div[contains(@class,'emailRecord')][text()='"+duplicateAdvertiser.toLowerCase()+"']")).isDisplayed());
				}
				
				scrollDown(driver);
				
			} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}		

		}
		
		public void setMultipleBrands(SubscriptionsDP dp, WebDriver driver, String brandName)
		{		
			try
			{		
				Reporter.log("Selecting Target Setting", true);
				enterText(getWebElement(dp.or, "targetSettingBrand",driver),brandName);
				Thread.sleep(1000);
				driver.findElement(By.xpath("//strong[text()='"+brandName+"']")).click();
				//String selectedValue = getWebElement(dp.or, "targetSettingInput",driver).getText();
				//Assert.assertEquals(selectedValue,advertisertName);
				//softAssert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li/div[contains(text(),'"+targetName+"')]")).isDisplayed());
				Reporter.log("Selected Target Setting", true);
				scrollDown(driver);
							
			} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}		

		}
		
		
		
		
		/*This method is to verify that toast appears and get the toast message*/
		public String verifyGetToastMessage(SubscriptionsDP dp, WebDriver driver)
		{
			
			try {
				
				WebElement toastNotification = getWebElement(dp.or, "toastNotification",driver);
				waitForElementPresent(toastNotification,TIMEOUT, PULSE);
				Thread.sleep(1500);
				String toastMessage = toastNotification.getText().trim();
				Thread.sleep(2000);
				return toastMessage;
			} 
			catch(org.openqa.selenium.StaleElementReferenceException ex)
			{
				WebElement toastNotification;
				try {
					toastNotification = getWebElement(dp.or, "toastNotification",driver);
					String toastMessage = toastNotification.getText().trim();
					return toastMessage;
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}
				
			}
			
			catch (Exception e) {
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
				return null;
			}
		}
		
		public void selectSubscriptionDateRangeIndustry(SubscriptionsDP dp, WebDriver driver,String dateNumber) {
			try {
				Reporter.log("Selecting Date Range for Industry", true);
				selectByVisibleText(
						getWebElement(dp.or, "chooseLastDaysSelect", driver),
						dateNumber);
					Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'Last "+dateNumber+" months')]")).isDisplayed());
				Reporter.log("Selected Date Range", true);
				scrollDown(driver);

			} catch (Exception e) {

				Assert.assertTrue(false,
						"Test case failed due to exception " + e.getMessage());
			}

		}
		
		public void selectSecondAdvertisersEngineRegionDaterange(SubscriptionsDP dp, WebDriver driver, String Advertiser2, String engineName,String regionName,String dateNumber,String BugID) {
			try
			{		
				Reporter.log("Selecting second Advertiser", true);
				driver.findElement(By.xpath("//label[@for='root_advertiser2']/following-sibling::div//input")).sendKeys(Advertiser2);
				Thread.sleep(2000);
				driver.findElement(By.xpath("//label[@for='root_advertiser2']/following-sibling::div//div[@role='listbox']//div[@role='option' and text()='"+Advertiser2+"']")).click();
				String selectedValue2 = driver.findElement(By.xpath("//label[@for='root_advertiser2']/following-sibling::div//child::span[@class='Select-value-label']")).getText().trim();
				Assert.assertEquals(selectedValue2,Advertiser2);
				scrollDown(driver);
				Reporter.log("Selected second Advertiser", true);
				Reporter.log("Selecting Engine & Region", true);
				selectByVisibleText(driver.findElement(By.xpath("//label[@for='root_advertiser2']/ancestor::div/following-sibling::div//label[text()='Engine']/following-sibling::select")),
						engineName);
				Thread.sleep(1000);
				selectByVisibleText(driver.findElement(By.xpath("//label[@for='root_advertiser2']/ancestor::div/following-sibling::div//label[text()='Region']/following-sibling::select")),
						regionName);
				Thread.sleep(1000);
				Reporter.log("Selecting Engine & Region", true);
				//Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'"+engineName+"')]")).isDisplayed());
				//Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'"+regionName+"')]")).isDisplayed());
				scrollDown(driver);
				Reporter.log("Selecting Date Range for Industry", true);
				selectByVisibleText(
						driver.findElement(By.xpath("//label[@for='root_industryDateRange2']/following-sibling::div/select")),
						dateNumber);
					//Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'Last "+dateNumber+" months')]")).isDisplayed());
				Reporter.log("Selected Date Range", true);
				scrollDown(driver);
} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}try{	
				softAssert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'"+Advertiser2.toLowerCase()+"')]")).isDisplayed());
			} catch(NoSuchElementException e)
			{
				if (driver.findElements(By.xpath("//div[contains(@class,'core__previewWrapper')]/div/ul//li[contains(text(),'"+Advertiser2.toLowerCase()+"')]")).size() != 1 )
					Reporter.getCurrentTestResult().setAttribute("warn", "Bug ID: "+BugID+"");
				
			}
				
				
	} 
		public void selectIndustryKeyword(SubscriptionsDP dp, WebDriver driver, String keyword) {
			try
			{		
				Reporter.log("Selecting Keyword", true);
				//getWebElement(dp.or, "selectAdvertiserClearButton",driver).click();
				enterText(getWebElement(dp.or, "selectIndustryKeywordInput",driver),keyword);
				Thread.sleep(2000);
				getWebElement(dp.or, "selectIndustryKeywordList",driver).findElement(By.xpath("//div[@role='option' and text()='"+keyword+"']")).click();
				String selectedValue = getWebElement(dp.or, "selectIndustryKeywordText",driver).getText().trim();
				Assert.assertEquals(selectedValue,keyword);
				Reporter.log("Selected Keyword", true);
				
				softAssert.assertTrue(driver.findElement(By.xpath("//span[contains(@class,'previewLabel')][text()='Keyword:']/parent::li[text()='"+keyword+"']")).isDisplayed());
				scrollDown(driver);
							
			} catch (Exception e) {
				
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
			}

		}
		
}
