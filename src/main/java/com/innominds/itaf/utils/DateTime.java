package com.innominds.itaf.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTime {

	// Get the current system date

	public static String getDate() {

		Date currentDateTime = new Date();

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
		simpleDateFormat.applyPattern("MM-dd-yyyy");
		String date = simpleDateFormat.format(currentDateTime);

		return date;
	}

	
	// Get the current system date and time in customized format.

	public static String getSystemDateTime() {

		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy hh-mm");
		Date date = new Date();
		return dateFormat.format(date);

	}

	

}
