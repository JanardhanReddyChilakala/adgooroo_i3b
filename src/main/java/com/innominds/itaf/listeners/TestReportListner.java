package com.innominds.itaf.listeners;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.h2.jdbcx.JdbcConnectionPool;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;

import com.innominds.itaf.driverinit.DriverManager;
import com.innominds.itaf.frameworkengine.CommonUtils;
import com.innominds.itaf.frameworkengine.Constants;
import com.innominds.itaf.utils.PropertyFileUtils;
import com.innominds.itaf.utils.SimpleExcelReaderExample;
import com.innominds.itaf.utils.WriteLogsInXLFile;
import com.innominds.itaf.utils.WriteReportToCSV;

public class TestReportListner extends DriverManager implements ITestListener {

	static int passcount = 0;
	static int failcount = 0;
	static int skippedcount = 0;
	static int totalcount = 0;
	static int th_count = 0;
	String startTime = null;
	String EndTime = null;
	String osName = null;
	String browserName = null;
	String testName = null;
	String methodName = null;
	String senarioName = null;
	String screenShotName = null;
	String[] classNameExtractorFromPack = null;
	String groupName = null;
	Boolean isRecordingEnabled = false;
	String warningMessage = null;

	public WebDriver driver1;

	@Override
	public void onTestStart(ITestResult arg0) {
		driver1 = getCurrentDriver();
		
	}

	@Override
	public void onTestFailure(ITestResult arg0) {
		failcount++;
		totalcount++;
		List group = arg0.getTestContext().getCurrentXmlTest().getIncludedGroups();
		groupName = group.get(0).toString();
		browserName = arg0.getTestContext().getCurrentXmlTest().getParameter("browser");
		osName = arg0.getTestContext().getCurrentXmlTest().getParameter("os");
		testName = arg0.getTestContext().getCurrentXmlTest().getParameter("testName");
		methodName = arg0.getMethod().getMethodName();
		senarioName = arg0.getInstanceName();
		classNameExtractorFromPack = senarioName.split("\\.");
		for (String className : classNameExtractorFromPack) {
			senarioName = className;
		}

		String userDir = System.getProperty("user.dir").toString().replace('\\', '/');
		screenShotName = "file://" + userDir + "/ScreenShots/" + senarioName + "_" + testName + "_" + methodName + "_" + osName + "_"
				+ browserName + ".png";
		String screenshotfilename = senarioName + "_" + testName + "_" + methodName + "_" + osName + "_" + browserName + ".png";
		
		String testDataRow;
		if (arg0.getTestContext().getAttribute("dpName")==null)
			testDataRow = " ";
		else
			testDataRow = arg0.getTestContext().getAttribute("dpName").toString();

		WriteLogsInXLFile.writeLogs(senarioName, testName, methodName, browserName, osName, "Fail", testDataRow, screenShotName,
				arg0.getThrowable().toString(), groupName);
		WriteReportToCSV.createCSVReport(senarioName, testName, methodName, browserName, osName, "Fail", testDataRow,
				screenShotName, arg0.getThrowable().toString(), groupName);
		PropertyFileUtils props = new PropertyFileUtils(
				CommonUtils.getFilePath(Constants.ENVIRONMENT_PROPERTIES_PATH, Constants.FEATURES_PROPERTIES_FILE));
		String isJIRA = props.getDataFromPropertyFile("JIRAIntegration");
		if (Boolean.parseBoolean(isJIRA)) {
			//Currently supports without screenshot
			screenshotfilename = "";
			
		}
		

	}

	@Override
	public void onTestSkipped(ITestResult arg0) {
		skippedcount++;
		totalcount++;

		List group = arg0.getTestContext().getCurrentXmlTest().getIncludedGroups();
		groupName = group.get(0).toString();
		browserName = arg0.getTestContext().getCurrentXmlTest().getParameter("browser");
		osName = arg0.getTestContext().getCurrentXmlTest().getParameter("os");
		testName = arg0.getTestContext().getCurrentXmlTest().getParameter("testName");
		methodName = arg0.getMethod().getMethodName();
		senarioName = arg0.getInstanceName();
		classNameExtractorFromPack = senarioName.split("\\.");
		for (String className : classNameExtractorFromPack) {
			senarioName = className;
		}

		String testDataRow;
		if (arg0.getTestContext().getAttribute("dpName")==null)
			testDataRow = " ";
		else
			testDataRow = arg0.getTestContext().getAttribute("dpName").toString();
		WriteLogsInXLFile.writeLogs(senarioName, testName, methodName, browserName, osName, "Skipped", testDataRow, "", "",
				groupName);
		WriteReportToCSV.createCSVReport(senarioName, testName, methodName, browserName, osName, "Skipped", testDataRow, "", "",
				groupName);
	}

	@Override
	public void onTestSuccess(ITestResult arg0) {
		passcount++;
		totalcount++;
		List group = arg0.getTestContext().getCurrentXmlTest().getIncludedGroups();
		groupName = group.get(0).toString();
		browserName = arg0.getTestContext().getCurrentXmlTest().getParameter("browser");
		osName = arg0.getTestContext().getCurrentXmlTest().getParameter("os");
		testName = arg0.getTestContext().getCurrentXmlTest().getParameter("testName");
		methodName = arg0.getMethod().getMethodName();
		senarioName = arg0.getInstanceName();
		Object value = arg0.getAttribute("key");
		arg0.setAttribute("key", value);
		classNameExtractorFromPack = senarioName.split("\\.");
		for (String className : classNameExtractorFromPack) {
			senarioName = className;
		}
		String testDataRow;
		if (arg0.getTestContext().getAttribute("dpName")==null)
			testDataRow = " ";
		else
			testDataRow = arg0.getTestContext().getAttribute("dpName").toString();
		
		if (arg0.getAttribute("warn") == null)
		{
		WriteLogsInXLFile.writeLogs(senarioName, testName, methodName, browserName, osName, "Pass", testDataRow, "", "", groupName);
		}else{
		
		WriteLogsInXLFile.writeLogs(senarioName, testName, methodName, browserName, osName, "Pass", testDataRow, "", (String) arg0.getAttribute("warn") , groupName);
	}
		WriteReportToCSV.createCSVReport(senarioName, testName, methodName, browserName, osName, "Pass", testDataRow, "", "",
				groupName);
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
	}

	@Override
	public void onStart(ITestContext arg0) {
		startTime = CommonUtils.getCurrentDateInRequiredDateFormat("dd-MMM-yyyy hh:mm aaa");
		//Execution Video Recording Start...
		PropertyFileUtils props = new PropertyFileUtils(
				CommonUtils.getFilePath(Constants.ENVIRONMENT_PROPERTIES_PATH, Constants.FEATURES_PROPERTIES_FILE));
		

	}

	static String finalhtmlreport;
	static boolean multithread = false;

	@AfterClass
	public void sendReport() {

	}

	@Override
	public void onFinish(ITestContext arg0) {

			try {
					createFinalReports();					
					} catch (IOException e) {

					e.printStackTrace();
				}
	}


	private void createFinalReports() throws IOException {
		EndTime = CommonUtils.getCurrentDateInRequiredDateFormat("dd-MMM-yyyy hh:mm aaa");

		float passPercentage = (100 * passcount) / totalcount;
		float failPercentage = (100 * failcount) / totalcount;
		String infile = System.getProperty("ResultPath");
		String finalReportFileName = "FinalReport" + System.currentTimeMillis();
		SimpleExcelReaderExample.convertResultReportToHtml(infile, finalReportFileName, startTime, EndTime,
				passPercentage, failPercentage);
		
	}

	@Override
	public String getCurrentProjectDirecotry() throws IOException {
		return new java.io.File(".").getCanonicalPath();
	}

}
