package com.innominds.itaf.driverinit;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

import com.innominds.itaf.frameworkengine.CommonUtils;
import com.innominds.itaf.frameworkengine.Constants;
import com.innominds.itaf.utils.PropertyFileUtils;

/**
 * The Class Driver Manager.
 * 
 * @author Innominds
 */
public class DriverManager {

	protected String browserName = null;
	protected ThreadLocal<RemoteWebDriver> threadDriver = null;
	public static final Logger logger = LogManager.getLogger(DriverManager.class.getName());
	private String browser = null;
	protected String os = null;
	
	private DesiredCapabilities dc;
	private WebDriver driver;
	public static int thr_count = 0;

	private WebDriver rwd = driver;

	public WebDriver getCurrentDriver() {
		return rwd;
	}	
	
	public WebDriver getDriver(String executionType) throws Exception {
		
			setDC_Capabilities(browserName, os, true);			
			rwd = driver;
			return driver;
	}
	
	public void setDC_Capabilities(String browser, String OSname, boolean isLocalDriver) throws IOException {
		dc = new DesiredCapabilities();
		
		dc.setCapability("ignoreZoomSetting", true);
		dc.setCapability("ignoreProtectedModeSettings", true);
		
		switch(browser.toLowerCase()) {
		
			case "firefox": dc = DesiredCapabilities.firefox();
							
							if(isLocalDriver) {								
								System.setProperty("webdriver.gecko.driver", getCurrentProjectDirecotry()
										+ "/src/test/resources/Drivers/firefox/firefox_64/geckodriver.exe");
								driver = new FirefoxDriver(dc);
							}
							else
								dc.setPlatform(Platform.extractFromSysProperty(OSname)); 
							
							break;
			case "chrome": dc = DesiredCapabilities.chrome();
							
							if(isLocalDriver) {
								System.setProperty("webdriver.chrome.driver", getCurrentProjectDirecotry()
										+ "/src/test/resources/Drivers/chrome/chromedriver_win32/chromedriver.exe");			
										dc.setBrowserName(DesiredCapabilities.chrome().getBrowserName());
								driver = new ChromeDriver(dc);								
							}
							else
								dc.setPlatform(Platform.extractFromSysProperty(OSname)); 
							break;	
			
		}
		
	}


	/**
	 * Load url.
	 * 
	 * @param url
	 *            the url
	 * @throws Exception
	 *             the exception
	 */
	public void loadURL(String url, WebDriver driver, String browser) throws Exception {
		this.driver = new Augmenter().augment(driver);
		driver.get(url);
		if ("firefox".equalsIgnoreCase(browser)) {

		} else {
			driver.manage().window().maximize();
		}

		driver.manage().timeouts().implicitlyWait(Constants.IMPLICIT_TIMEOUT, TimeUnit.SECONDS);
		rwd = driver;
	}

	/**
	 * Quit browser.
	 *
	 * @return the string
	 */
	public boolean quitBrowser(WebDriver driver) {
		boolean isQuit = false;
		try {
			driver.quit();
			isQuit = true;

		} catch (Exception e) {
			throw new RuntimeException("Failed: to quit the browser " + e.getMessage());
		}
		return isQuit;

	}

	/**
	 * Close browser.
	 *
	 * @return true, if successful
	 */
	public boolean closeBrowser(WebDriver driver) {
		boolean isClose = false;
		try {
			driver.close();
			isClose = true;
		} catch (Exception e) {
			throw new RuntimeException("Failed: to close the browser " + e.getMessage());
		}
		return isClose;

	}

	public String captureScreeshot(String fileName, WebDriver driver) throws Exception {

		String dest = System.getProperty("user.dir") + "/ScreenShots/" + fileName + ".png";

		File destination = new File(dest);
		File source = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(source, destination);

		return dest;

	}

	public String getCurrentProjectDirecotry() throws IOException {
		return new java.io.File(".").getCanonicalPath();
	}

	@AfterMethod(groups = { "Regression", "Smoke", "Sanity" })
	public void takeScreenShot(ITestResult result) {

		if (result.getStatus() == ITestResult.FAILURE) {
			String browserNam = result.getTestContext().getCurrentXmlTest().getParameter("browser");
			String osName = result.getTestContext().getCurrentXmlTest().getParameter("os");

			String testName = result.getMethod().getMethodName();
			String senarioName = result.getInstanceName();
			String[] classNameExtractorFromPack = senarioName.split("\\.");
			for (String className : classNameExtractorFromPack) {
				senarioName = className;
			}

			try {
				captureScreeshot(senarioName + "_" + testName + "_" + osName + "_" + browserNam, driver);
			} catch (Exception e) {
				Assert.assertTrue(false, "Test case failed due to exception "+e.getMessage());
				
			}
		}
	}

}