package com.innominds.itaf.frameworkengine;

import java.io.File;

/**
 * The Class Constants.
 *
 * @author Chaya Venkateswarlu, Kiran Kumar Cherukuri
 */

public class Constants {

	/*
	 * ===================== Project Resources folder
	 * path=======================
	 */

	public static final String PROJ_RESOURCE_PATH = System.getProperty("user.dir") + File.separator + "src"
			+ File.separator + "test" + File.separator + "resources" + File.separator;
	public static final String ENVIRONMENT_PROPERTIES_PATH = PROJ_RESOURCE_PATH + "Properties" + File.separator
			+ "Environments" + File.separator;
	public static final String PROPERTIES_PATH = PROJ_RESOURCE_PATH + "Properties" + File.separator;

	/*
	 * ===================== Implicit & Explicit timeout=======================
	 */

	// Seconds
	public static final int IMPLICIT_TIMEOUT = 30;
	public static final int EXPLICIT_TIMEOUT = 60;
	public static final int THREAD_SLEEP = 500;

	/* =====================For Web========================== */
	// Test Suite File Path
	public static final String TESTSUITE_FILE_PATH = PROJ_RESOURCE_PATH + "TestSuites" + File.separator
			+ "WebSuiteRunner.xls";
	public static final String TESTSUITE_FILE_SHEETNAME = "TestSuites";
	public static final String SCREENSHOT_FOLDER_PATH = PROJ_RESOURCE_PATH + "TestResults" + File.separator
			+ "Screenshots";
	public static final String REPORT_FOLDER_PATH = System.getProperty("user.dir") + File.separator + "Reports";
	public static final String TESTDATA_FILE_PATH = PROJ_RESOURCE_PATH + "TestData" + File.separator;
	public static final String WEB_PROPERTIES_FILE = ENVIRONMENT_PROPERTIES_PATH + "web.properties";

	
	/* ===================== Drivers file path========================== */

	public static final String CHROME_DRIVER_WIN_32_EXE_PATH = PROJ_RESOURCE_PATH + "Drivers" + File.separator
			+ "chrome" + File.separator + "chromedriver_win32" + File.separator + "chromedriver.exe";
	public static final String FIREFOX_DRIVER_32_EXE_PATH = PROJ_RESOURCE_PATH + "Drivers" + File.separator + "firefox"
			+ File.separator + "firefox_32" + File.separator + "geckodriver.exe";
	public static final String FIREFOX_DRIVER_64_EXE_PATH = PROJ_RESOURCE_PATH + "Drivers" + File.separator + "firefox"
			+ File.separator + "firefox_64" + File.separator + "geckodriver.exe";


	/* ===================== Repository paths========================== */

	public static final String WEB_OBJECT_REPO_FILE_PATH = PROJ_RESOURCE_PATH + "ObjectRepo" + File.separator + "WebOR"
			+ File.separator;
	public static final String EXCEL_REPORT_FILE_PATH = PROJ_RESOURCE_PATH + "TestSuites";

	
	/* =====================Features Properties File========================== */

	public static final String FEATURES_PROPERTIES_FILE = "Features.properties";


	
	
}
