/**
 * 
 */
package com.innominds.itaf.frameworkengine;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import com.innominds.itaf.utils.PropertyFileUtils;



/**
 * The Class with wrapper function for webdriver api's.
 *
 * @author paggrawal, Chaya Venkateswarlu, Praveen Gaddam and Kiran Kumar
 *         Cherukuri
 */
public class PageActionUtils {

	private By by;
	public WebElement element;
	public List<WebElement> elements;

	public WebDriverWait wait;
	public static Logger logger = LogManager.getLogger(PageActionUtils.class.getName());
	

	/**
	 * The Enum AttributeName.
	 */
	private enum AttributeName {

		unKnown, name, id, xpath, css, classname, linktext, partiallinkText, tagname, empty
	}

	/**
	 * Gets getBy.
	 *
	 * @param map2
	 *            the map 2
	 * @param keyWord
	 *            the key word
	 * @return By
	 */
	protected By getBy(Map<String, Map<String, String>> map2, String keyWord, WebDriver driver) {
		Map<String, String> a = map2.get(keyWord);
		try {
			wait = new WebDriverWait(driver, Constants.EXPLICIT_TIMEOUT);

			Map.Entry<String, String> entry = a.entrySet().iterator().next();
			String propType = entry.getKey().toLowerCase().trim();
			String propValue = entry.getValue().trim();

			AttributeName attributeName = AttributeName.unKnown;
			attributeName = AttributeName.valueOf(propType.trim());

			if (propType != null && propValue != null) {
				switch (attributeName) {
				case id: 
					by = By.id(propValue);
					break;
				
				case name: 
					by = By.name(propValue);
					break;
				
				case xpath: 
					by = By.xpath(propValue);
					break;
				
				case classname: 
					by = By.className(propValue);
					break;
				
				case css: 
					by = By.cssSelector(propValue);
					break;
				
				case linktext: 
					by = By.linkText(propValue);
					break;
				
				case partiallinkText: 
					by = By.partialLinkText(propValue);
					break;
				
				case tagname: 
					by = By.tagName(propValue);
					break;
				
				default:
					Reporter.log("No locator type found....");
					break;

				}
			} else {
				logger.info("Property type is empty");
			}
		} catch (Exception e) {
			logger.error(e);
			logger.error("Property type is empty or some other error");
		}
		return by;
	}

	/**
	 * Gets the web element.
	 *
	 * @param map2
	 *            the map 2
	 * @param keyWord
	 *            the key word
	 * @return the web element
	 * @throws Exception
	 */

	public WebElement getWebElement(Map<String, Map<String, String>> map2, String keyWord, WebDriver driver)
			throws Exception {
		try {
			by = getBy(map2, keyWord, driver);

			if (by != null) {

				// if
				// (wait.until(ExpectedConditions.presenceOfElementLocated(by))
				// != null)
				element = driver.findElement(by);
			} else {
				System.out.println("Properties not found for element locator");
			}

		} catch (Exception e) {

			logger.error(e);
			logger.error("Property type is empty or some other error");
			throw new Exception(e.getMessage());
		}
		return element;
	}
	
	public void waitForElementPresent(WebElement ele, long timeout, int pulse) throws InterruptedException {
		long rpt = timeout/pulse;
		for (int i=0; i<rpt; i++) {
			if (!ele.isDisplayed() && !ele.isEnabled()) {
				Thread.sleep(pulse);
			} else
				break;
		}
	}

	/**
	 * Click.
	 *
	 * @param element
	 *            the element
	 * @throws IllegalArgumentException
	 *             the illegal argument exception
	 */
	public void click(WebElement element) throws IllegalArgumentException {
		try {
			element.click();
		} catch (Exception e) {
			throw new RuntimeException("Failed: to click WebElement " + e.getMessage());
		}

	}

	/**
	 * Enter text.
	 *
	 * @param element
	 *            the element
	 * @param text
	 *            the text
	 */
	public void enterText(WebElement element, String text) {
		try {
			 wait.until(ExpectedConditions.visibilityOf(element));
			element.sendKeys(text.trim());
		} catch (Exception e) {
			throw new RuntimeException("Failed: to enter input text " + e.getMessage());
		}

	}

	/**
	 * Clear text.
	 *
	 * @param element
	 *            the element
	 */
	public void clearText(WebElement element) {
		try {
			element.clear();
		} catch (Exception e) {
			throw new RuntimeException("Failed: to enter input text " + e.getMessage());
		}

	}

	/**
	 * Returns true if element is visible, else false.
	 *
	 * @param element
	 *            the element
	 * @return true, if is element displayed
	 * @throws ElementNotVisibleException
	 *             the element not visible exception
	 */

	public boolean isElementDisplayed(WebElement element) {
		boolean isVis = false;
		try {
			isVis = element.isDisplayed();
		} catch (Exception e) {
			System.out.println("Failed: to retrive visibility of element " + e.getMessage());
			
		}
		return isVis;
	}

	

	/**
	 * Js click.
	 *
	 * @param myWebElement
	 *            the my web element
	 */
	public void jsClick(WebElement myWebElement, WebDriver driver) {
		try {

			((JavascriptExecutor) driver).executeScript("arguments[0].click();", myWebElement);
		} catch (Exception e) {
			throw new RuntimeException("Failed: to click the webelement " + e.getMessage());
		}
	}

	

	/**
	 * Checks if is displayed.
	 *
	 * @param element
	 *            the element
	 * @param UserNameID
	 *            the user name ID
	 * @return true, if is displayed
	 */
	public boolean isDisplayed(WebElement element, String UserNameID) {
		try {
			boolean status = element.isDisplayed();
			return status;
		} catch (Exception e) {
			throw new RuntimeException("Failed: to check if the element is displayed or not" + e.getMessage());
		}
	}

	/**
	 * Checks if is selected.
	 *
	 * @param element
	 *            the element
	 * @return true, if is selected
	 */
	public boolean isSelected(WebElement element) {
		boolean status = false;
		try {
			status = element.isSelected();

		} catch (Exception e) {
			throw new RuntimeException("Failed: to check if the element is selected or not" + e.getMessage());
		}
		return status;
	}

	/**
	 * Gets the text.
	 *
	 * @param element
	 *            the element
	 * @return the text
	 */
	public String getText(WebElement element) {
		try {
			String linkText = element.getText();
			return linkText;
		} catch (Exception e) {
			throw new RuntimeException("Failed: to get inner text of the page" + e.getMessage());
		}
	}

	/**
	 * Select by visible text.
	 *
	 * @param element
	 *            the element
	 * @param text
	 *            the text
	 */
	public void selectByVisibleText(WebElement element, String text) {
		Select oSelect = null;
		try {
			oSelect = new Select(element);
			wait.until(ExpectedConditions.visibilityOf(element));
			Thread.sleep(Constants.THREAD_SLEEP);
			oSelect.selectByVisibleText(text.trim());
		} catch (Exception e) {
			throw new RuntimeException("Failed: to select the element by visible text" + e.getMessage());
		}
	}

	

	HttpURLConnection con = null;

	public boolean browserResponse(WebDriver driver) {
		try {
			HttpURLConnection.setFollowRedirects(true);

			con = (HttpURLConnection) new URL(driver.getCurrentUrl()).openConnection();
			con.setRequestMethod("HEAD");

			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		} catch (Exception e) {
			// e.printStackTrace();
			return false;
		}
	}

	public ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
		@Override
		public Boolean apply(WebDriver driver) {

			return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
					.equals("complete");
		}
	};
	public ExpectedCondition<Boolean> xhrLoad = new ExpectedCondition<Boolean>() {
		@Override
		public Boolean apply(WebDriver driver) {
			return ((JavascriptExecutor) driver).executeScript("return XMLHttpRequest.DONE").toString().equals("4");
		}
	};

	public ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
		@Override
		public Boolean apply(WebDriver driver) {
			try {
				return (Boolean) ((JavascriptExecutor) driver)
						.executeScript("return window.jQuery != undefined || jQuery.active == 0");
			} catch (Exception e) {
				return true;
			}
		}
	};

	public void waitForPageLoaded(WebDriver driver) {
		boolean flag = true;
		try {
			//0.5 second delay beyond which we could say slow performance
			Thread.sleep(500);
			WebDriverWait wait = new WebDriverWait(driver, Constants.EXPLICIT_TIMEOUT);
			flag = wait.until(jQueryLoad) && wait.until(jsLoad) && wait.until(xhrLoad);
			browserResponse(driver);

		} catch (Exception e) {
			System.out.println("wait for page to load exception");
			browserResponse(driver);
		}
	}	
}