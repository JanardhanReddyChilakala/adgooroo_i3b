package com.innominds.itaf.frameworkengine;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * CommonUtils class holds wrapper api build using java API's which are commonly
 * used for.
 *
 * @author paggrawal, Chaya Venkateswarlu
 */

@SuppressWarnings("deprecation")
public class CommonUtils {

	public static Logger logger = LogManager.getLogger(CommonUtils.class.getName());

	/**
	 * Split the string.
	 *
	 * @param stringToSplit
	 *            the string to split
	 * @param delimiter
	 *            the delimiter
	 * @return the string[]
	 */
	public static String[] splitString(String stringToSplit, String delimiter) {
		String[] flag = null;

		try {
			String[] result = stringToSplit.split(delimiter);
			if (result.length < 2) {
				throw new IllegalArgumentException("String not in correct format");
			} else {
				flag = result;
			}
		} catch (Throwable t) {
			logger.error("Unable to split the string-" + stringToSplit + " due to erro-", t);
		}
		return flag;
	}

	/**
	 * Gets the current date in required date format.
	 *
	 * @param dateFormat
	 *            the date format
	 * @return the current date in required date format
	 */
	public static String getCurrentDateInRequiredDateFormat(String dateFormat) {
		String timeStamp = null;
		try {
			timeStamp = new SimpleDateFormat(dateFormat).format(Calendar.getInstance().getTime());
		} catch (Throwable t) {
			logger.error("unable to format the given date format:" + dateFormat + " of the current date due to error",
					t);
		}

		return timeStamp;
	}

	/*
	 * Get folder path
	 */

	/**
	 * Gets the file path.
	 *
	 * @param folderName
	 *            the folder name
	 * @param fileName
	 *            the file name
	 * @return the file path
	 */
	public static String getFilePath(String folderName, String fileName) {
		String path = null;
		try {

			path = new File(folderName + fileName).getAbsolutePath();

		} catch (Exception e) {
			throw new RuntimeException("Failed : to get file path from Resource source folder" + e.getMessage());
		}
		return path;
	}
	/*
	 * Returns file extension
	 * 
	 * @param filename with path
	 */

	/**
	 * Gets the file extn.
	 *
	 * @param fileName
	 *            the file name
	 * @return the file extn
	 */
	public static String getFileExtn(String fileName) {
		String fileExtension = "";
		try {
			String fn = new File(fileName).getName();
			if (fn.contains(".") && fn.lastIndexOf(".") != 0) {
				fileExtension = fn.substring(fn.lastIndexOf(".") + 1);
			}
		} catch (Exception e) {
			throw new RuntimeException("Failed: to get Excel File Name extension " + e.getMessage());
		}
		return fileExtension;
	}

	/*
	 * Store properties file data into Hashmap
	 * 
	 * @param - path of file
	 * 
	 */

	/**
	 * Store prop into map.
	 *
	 * @param filePath
	 *            the file path
	 * @return the hash map
	 */
	public static HashMap<String, String> storePropIntoMap(String filePath) {
		HashMap<String, String> map = new HashMap<String, String>();
		try {
			Properties prop = new Properties();
			FileInputStream ip = new FileInputStream(new File(filePath));
			prop.load(ip);
			Set<Object> keys = prop.keySet();
			for (Object k : keys) {
				map.put((String) k, prop.getProperty((String) k));
			}
		} catch (Exception e) {
			throw new RuntimeException("Failed: to get Excel File Name extension " + e.getMessage());
		}
		return map;
	}
	
	public static String getCurrentTimeStamp() {
		java.util.Calendar cal = java.util.Calendar.getInstance();
		cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
		String date = sdf.format(cal.getTime());
		return date;
	}

}
