# Harmony 
## Core Test Automation Framework by Innominds Team

### Key Notes:
* All passwords to be used using encryption (please discuss with ***us(automationcoe@innominds.com)*** so that we could guide you how to get encrypted password). (E-mail reporting, etc)


### Features include:

* Cross-Browser(IE, Edge, Chrome, Firefox, Safari) and Cross-Platform (Windows, Unix,Linux) Testing.
* Parallel Testing (TestNG Integration).
* CI/CD Integration with Maven and Jenkins.